package com.justmobile.ctos.profiletab;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.R;

public class Profile extends SherlockFragment implements OnClickListener {

	static Button btRegister, btUpdate;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater
				.inflate(R.layout.profiletab_firstin, container, false);
		//setHasOptionsMenu(true);
//		ActionBar actionBar = getSherlockActivity().getSupportActionBar();
//		actionBar.setBackgroundDrawable(getResources().getDrawable(
//				R.drawable.topbar01));
		
		btRegister = (Button) v.findViewById(R.id.btRegister);
		btUpdate = (Button) v.findViewById(R.id.btUpdate);

		btRegister.setOnClickListener(this);
		btUpdate.setOnClickListener(this);
		return v;
	}

	@Override
	public void onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		MenuItem login = menu.add(Menu.NONE, 0, 0, "").setIcon(R.drawable.login);
		login.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS );
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			this.startActivity(intent);
			getSherlockActivity().finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.btRegister) {
			Intent intent = new Intent(getSherlockActivity(),
					RegisterFirstPage.class);
			getSherlockActivity().startActivity(intent);
		}else if(view.getId() == R.id.btUpdate){
			Alert("Please log in to view/edit your profile");
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(getActivity()).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getSherlockActivity(),
								CTOSLoginPage.class);
						startActivity(intent);
						getSherlockActivity().finish();
						dialog.dismiss();
					}
				}).show();

	}
}