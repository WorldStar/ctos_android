package com.justmobile.ctos.profiletab;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.infotab.Contact;
import com.justmobile.ctos.library.ConvertImage;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.sms.SmsReceiver;
import com.justmobile.ctos.webservice.MemberWebService;

public class RegisterSeventhPage extends Activity implements
		OnClickListener {
	Dialog dialog;
	HashMap<String, String> userInfo = new HashMap<String, String>();
	WSMemberRegistration wsMemberRegistration;
	Bitmap frontIC, backIC;
	byte[] front, back;
	TextView tvSerial, tvCac;
	String ic="";
	String icnew="";
	char[] charArrays=new char[12];
	int y=0;
	SharedPreferences settings;
    Typeface font1, font2;
    Button cancel, agree;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_confirm);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		TextView etpnum = (TextView)findViewById(R.id.etpNum);
		etpnum.setVisibility(View.VISIBLE);
		etpnum.setText("Page(5/5)");
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);

		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		Log.d("userInfo @ intent 7th page", userInfo.toString());
		settings = this.getSharedPreferences("REG", Context.MODE_PRIVATE);
		
		MemberWebService cacWs = new MemberWebService();
		
		TextView tvName = (TextView) findViewById(R.id.tvName);
		tvSerial = (TextView) findViewById(R.id.tvSerial);
		tvCac = (TextView) findViewById(R.id.tvCac);
		tvCac.setVisibility(View.GONE);
		tvSerial.setVisibility(View.GONE);
		TextView tvIC = (TextView) findViewById(R.id.tvIC);
		TextView tvOldIC = (TextView) findViewById(R.id.tvOldIC);
		TextView tvOldtitle = (TextView) findViewById(R.id.tvoldtitle);
		TextView tvGender = (TextView) findViewById(R.id.tvGender);
		TextView tvMyKadAddress = (TextView) findViewById(R.id.tvMyKadAddress);
		TextView tvCorresponseAddress = (TextView) findViewById(R.id.tvCorresponseAddress);
		TextView tvCitizenship = (TextView) findViewById(R.id.tvCitizenship);
		TextView tvMobile = (TextView) findViewById(R.id.tvMobile);
		TextView tvEmail = (TextView) findViewById(R.id.tvEmail);
		TextView tvSecurityQs = (TextView) findViewById(R.id.tvSecurityQs);
		
		ImageView ivFrontIC = (ImageView) findViewById(R.id.ivFrontIC);
		
		ImageView ivBackIC = (ImageView) findViewById(R.id.ivBackIC);
		String oldic = userInfo.get("ic");
		if(oldic != null && !oldic.equals("")){
			tvOldIC.setText(oldic);
		}else{
			tvOldIC.setVisibility(View.GONE);
			tvOldtitle.setVisibility(View.GONE);
		}
		ic=userInfo.get("nic");
		if(!userInfo.get("citizenship").equals("FOREIGNER")){
			for(int x=0;x<ic.length();x++)
			{
				if(ic.charAt(x)!='-')
				{
					charArrays[y] = ic.charAt(x);
					y++;
				}
			}
			icnew = String.valueOf(charArrays);
		}else{
			icnew = ic;
		}
		Log.d("IC NO:", icnew);

		//new SMSTask().execute();
		tvName.setText(userInfo.get("name"));
		tvSerial.setText(userInfo.get("serial"));
		tvCac.setText(userInfo.get("cac"));
		tvIC.setText(userInfo.get("nic"));
		tvGender.setText(userInfo.get("genderType"));
		tvMyKadAddress.setText(userInfo.get("address1") + " "
				+ userInfo.get("address2") + " " + userInfo.get("address3")
				+ " " + userInfo.get("postcode") + " " + userInfo.get("city")
				+ " " + userInfo.get("state") + " "
				+ userInfo.get("nationality"));
		if(userInfo.get("corresflg") != null && userInfo.get("corresflg").equals("1")){
			tvCorresponseAddress.setText(userInfo.get("corraddress1") + " "
					+ userInfo.get("corraddress2") + " "
					+ userInfo.get("corraddress3") + " "
				+ userInfo.get("corrpostcode") + " " + userInfo.get("corrcity")
				+ " " + userInfo.get("corrstate") + " "
				+ userInfo.get("sCcountry"));
		}else{
			tvCorresponseAddress.setText("");
		}
		tvCitizenship.setText(userInfo.get("citizenship"));
		tvMobile.setText(userInfo.get("mobileNumberPrefix") + "-"
				+ userInfo.get("mobileNumberSuffix"));
		tvEmail.setText(userInfo.get("email"));
		if (userInfo.get("standardSecurityQuestion").equals("0")) {
			tvSecurityQs.setText(userInfo.get("customSecurityQuestion"));
		} else {
			tvSecurityQs.setText(userInfo.get("secQuest"));
		}
		
		front = Base64.decode(settings.getString("front", null), Base64.DEFAULT);
		frontIC = BitmapFactory.decodeByteArray(front, 0, front.length);
		int width = frontIC.getWidth();
		int height = frontIC.getHeight();
		float ratio = (float)200 / (float)width;
		//height = (int)((float)height * (float)ratio);
		ivFrontIC.setImageBitmap(Bitmap.createScaledBitmap(frontIC, width, height,
				false));

		back = Base64.decode(settings.getString("back", null), Base64.DEFAULT);
		backIC = BitmapFactory.decodeByteArray(back, 0, back.length);
		width = backIC.getWidth();
		height = backIC.getHeight();
		ratio = (float)200 / (float)width;
		//height = (int)((float)height * (float)ratio);
		ivBackIC.setImageBitmap(Bitmap.createScaledBitmap(backIC, width, height,
				false));
		//
		// // Declare display dialog after user register
		dialog = new Dialog(this);
		dialog.setContentView(R.layout.profiletab_register_dialog);
		dialog.setTitle("Registration Success");
		dialog.setCancelable(false);
		Button dialogContactUs = (Button) dialog
				.findViewById(R.id.dialogContactUs);
		Button dialogBacktoMenu = (Button) dialog
				.findViewById(R.id.dialogBacktoMenu);
		dialogContactUs.setOnClickListener(this);
		dialogBacktoMenu.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view.getId() == R.id.btCancel){
			Intent intent1 = new Intent(this, RegisterSixthPage.class);
			intent1.putExtra("userInfo", userInfo);
			this.startActivity(intent1);
			this.finish();
		}else if(view.getId() == R.id.btAgree){
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				wsMemberRegistration = new WSMemberRegistration();
				wsMemberRegistration.execute();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}else if (view.getId() == R.id.dialogContactUs) {
			dialog.dismiss();
			Intent intent = new Intent(this, Contact.class);
			//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			this.startActivity(intent);
			finish();
		}else if(view.getId() == R.id.dialogBacktoMenu){
			dialog.dismiss();
			Intent intent = new Intent(this, CTOSMainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			this.startActivity(intent);
			finish();
		}
	}

	class WSMemberRegistration extends AsyncTask<String, String, String> {

		ProgressDialog MyDialog;
		protected String doInBackground(String... arg0) {
			String frontICBase64 = "", backICBase64 = "";

			frontICBase64 = ConvertImage.bitmapToBase64(frontIC);
			backICBase64 = ConvertImage.bitmapToBase64(backIC);

			MemberWebService memberWS = new MemberWebService();
			/*List<HashMap<String, String>> temp1 = memberWS
					.PmCac(	userInfo.get("nic"),
							userInfo.get("passport"),
							userInfo.get("country"),
							userInfo.get("mobileNumberPrefix"),
							userInfo.get("mobileNumberSuffix"));
			if(temp1.get(0).get("status").equals("0")){
				return temp1.get(0).get("errorMessage");
			}else{*/
				//String cacno = temp1.get(0).get("cacSerialNo").substring(2, temp1.get(0).get("cacSerialNo").length());
			
			
			
			
				List<HashMap<String, String>> temp = memberWS
						.PmCreate(userInfo.get("name"), userInfo.get("password"),
								userInfo.get("confirmPassword"),
								icnew, userInfo.get("ic"),
								userInfo.get("passport"), userInfo.get("gender"),
								userInfo.get("address1"), userInfo.get("address2"),
								userInfo.get("address3"), userInfo.get("postcode"),
								userInfo.get("city"), userInfo.get("state"),
								userInfo.get("country"),
								userInfo.get("citizenship"),
								userInfo.get("nationality"),
								userInfo.get("corraddress1"),
								userInfo.get("corraddress2"),
								userInfo.get("corraddress3"),
								userInfo.get("corrpostcode"),
								userInfo.get("corrcity"),
								userInfo.get("corrstate"),
								userInfo.get("corrcountry"), userInfo.get("email"),
								userInfo.get("mobileNumberPrefix"),
								userInfo.get("mobileNumberSuffix"),
								userInfo.get("standardSecurityQuestion"),
								userInfo.get("customSecurityQuestion"),
								userInfo.get("securityAnswer"), frontICBase64,
								backICBase64, userInfo.get("cac"));//
				if(temp.size() > 0){
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("errorMessage");
					else
						return temp.get(0).get("status");
				}else{
					return "Server Error.";
				}
			//}
		}

		protected void onPostExecute(String result) {

			//if (MyDialog.isShowing())
				MyDialog.dismiss();

			if (result.equals("1")){
				dialog.show();
		    }else {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RegisterSeventhPage.this);
				builder.setMessage("" + result)
						.setTitle("CTOS")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
			//MyDialog = ProgressDialog.show(RegisterSeventhPage.this, "",
					//"Registering...", true);

			MyDialog = new ProgressDialog(RegisterSeventhPage.this);
			MyDialog.setMessage("Registering...");
			MyDialog.setIndeterminate(false);
			MyDialog.setCancelable(false);
			MyDialog.show();
			/*MyDialog.setCancelable(true);
			MyDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface arg0) {
					// TODO Auto-generated method stub
					wsMemberRegistration.cancel(true);
					ReturnMessage.showAlertDialog(RegisterSeventhPage.this,
							"Registration cancelled.");
				}
			});*/
		}

	}
	/*private class WSMemberRegistration extends AsyncTask<Void, Void, String> {
		ProgressDialog MyDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			MyDialog = ProgressDialog.show(RegisterSeventhPage.this, "",
					"Registering...", true);

			MyDialog.setCancelable(true);
			MyDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface arg0) {
					// TODO Auto-generated method stub
					wsMemberRegistration.cancel(true);
					ReturnMessage.showAlertDialog(RegisterSeventhPage.this,
							"Registration cancelled.");
				}
			});

		}

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String frontICBase64 = "", backICBase64 = "";

			frontICBase64 = ConvertImage.bitmapToBase64(frontIC);
			backICBase64 = ConvertImage.bitmapToBase64(backIC);

			MemberWebService memberWS = new MemberWebService();
			/*List<HashMap<String, String>> temp1 = memberWS
					.PmCac(	userInfo.get("nic"),
							userInfo.get("passport"),
							userInfo.get("country"),
							userInfo.get("mobileNumberPrefix"),
							userInfo.get("mobileNumberSuffix"));
			if(temp1.get(0).get("status").equals("0")){
				return temp1.get(0).get("errorMessage");
			}else{*/
				//String cacno = temp1.get(0).get("cacSerialNo").substring(2, temp1.get(0).get("cacSerialNo").length());
			
			
			
			
	/*			List<HashMap<String, String>> temp = memberWS
						.PmCreate(userInfo.get("name"), userInfo.get("password"),
								userInfo.get("confirmPassword"),
								icnew, userInfo.get("ic"),
								userInfo.get("passport"), userInfo.get("gender"),
								userInfo.get("address1"), userInfo.get("address2"),
								userInfo.get("address3"), userInfo.get("postcode"),
								userInfo.get("city"), userInfo.get("state"),
								userInfo.get("country"),
								userInfo.get("citizenship"),
								userInfo.get("nationality"),
								userInfo.get("corraddress1"),
								userInfo.get("corraddress2"),
								userInfo.get("corraddress3"),
								userInfo.get("corrpostcode"),
								userInfo.get("corrcity"),
								userInfo.get("corrstate"),
								userInfo.get("corrcountry"), userInfo.get("email"),
								userInfo.get("mobileNumberPrefix"),
								userInfo.get("mobileNumberSuffix"),
								userInfo.get("standardSecurityQuestion"),
								userInfo.get("customSecurityQuestion"),
								userInfo.get("securityAnswer"), frontICBase64,
								backICBase64, userInfo.get("cac"));//

				if (temp.get(0).get("status").equals("0"))
					return temp.get(0).get("errorMessage");
				else
					return temp.get(0).get("status");
			//}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			if (MyDialog.isShowing())
				MyDialog.dismiss();

			if (result.equals("1"))
				dialog.show();
			else {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RegisterSeventhPage.this);
				builder.setMessage("Error!: " + result)
						.setTitle("CAC verification failed.")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}

	}
	*/
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterSixthPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}
	/*class SMSTask extends AsyncTask<String, String, String> {

		private Exception exception;
		ProgressDialog MyDialog;
		protected String doInBackground(String... arg0) {
			try {
				MemberWebService cacWs = new MemberWebService();
				List<HashMap<String, String>> temp1 = cacWs
						.PmCac(	userInfo.get("nic"),
								userInfo.get("passport"),
								userInfo.get("country"),
								userInfo.get("mobileNumberPrefix"),
								userInfo.get("mobileNumberSuffix"));
				if(temp1.get(0).get("status").equals("0")){
					AlertDialog.Builder builder = new AlertDialog.Builder(
							RegisterSeventhPage.this);
					builder.setMessage("Error!: " +  temp1.get(0).get("errorMessage"))
							.setTitle("Confirm Cac code")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int id) {
											dialog.cancel();
										}
									});
					AlertDialog alert = builder.create();
					alert.show();
					userInfo.put("cac", "");
					userInfo.put("serial", "");
					return "";
				}else{
					ContentResolver contentResolver = getContentResolver();
					Cursor cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
	
					int indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
					int indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
					int indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
					
					if ( indexBody < 0 || !cursor.moveToFirst() ) return "success";
					
					boolean flg = true;
					Calendar c = Calendar.getInstance();
					int lasttime = c.get(Calendar.HOUR) * 60 + c.get(Calendar.MINUTE);
					String serial1 = temp1.get(0).get("cacSerialNo");
					cursor.moveToNext();
					int i = 0;
					while(flg){
						i++;
						String str = cursor.getString(indexBody);
						String cac = "";
						String serial = "";
						int index = 0;
						if((index = str.indexOf("CAC Code:")) != -1){
							cac = str.substring(index+10, index+16);
							index = str.indexOf("Serial No:");
							serial = str.substring(index+11, index+15);
						}
						if(serial1.equals(serial)){
							flg = false;
							userInfo.put("cac", cac);
							userInfo.put("serial", serial);
							serialno = serial;
							tvSerial.setText(serial);
						}else{
						}
						if(i > 2){
							cursor.close();
							contentResolver = getContentResolver();
							cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
							
							indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
							indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
							indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
							cursor.moveToNext();
							i = 0;
						}else{
							cursor.moveToNext();
						}
						int curtime = c.get(Calendar.HOUR) * 60 + c.get(Calendar.MINUTE);
						if(Math.abs(lasttime - curtime) > 1){
							flg = false;
							userInfo.put("cac", "");
							userInfo.put("serial", "");
						}
						try{
							Thread.sleep(200);
						}catch(Exception e){}
					}
					//do
					//{
						//String str = "Sender: " + cursor.getString( indexAddr ) + "\n" + cursor.getString( indexBody );
						//smsList.add( str );
					//}
					//while( cursor.moveToNext() );
					return "success";
				}
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}
	
		protected void onPostExecute(String feed) {
			if (MyDialog.isShowing())
				MyDialog.dismiss();

			if (feed.equals("success")) {
				Log.d("success", "success");
			}
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MyDialog = ProgressDialog.show(RegisterSeventhPage.this, "",
					"Reading Cac Code...", true);

			MyDialog.setCancelable(true);
			MyDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface arg0) {
					// TODO Auto-generated method stub
					wsMemberRegistration.cancel(true);
					ReturnMessage.showAlertDialog(RegisterSeventhPage.this,
							"Registration cancelled.");
				}
			});
		}

	}
	*/
}