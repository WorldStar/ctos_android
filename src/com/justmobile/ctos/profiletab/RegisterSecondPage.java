
package com.justmobile.ctos.profiletab;

import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.profiletab.RegisterFirstPage.sendingData;
import com.justmobile.ctos.webservice.MemberWebService;

public class RegisterSecondPage extends Activity implements OnClickListener {
	Button translateEn;
	Button translateMy;
	int tr = 0;
	WebView webview, webview2;
	static String bm = "", en = "";
	String initial = "";
	private ProgressDialog pDialog;
	Iterator itr;
	String name, namemalaysian, warganegara, nonmalaysian, foreigner;
	String sNationality, sNationality1;
	HashMap<String, String> userInfo = new HashMap<String, String>();
	Typeface font1,font2;
	float initscale = 0f;
	float scale = 0f;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_tc2);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setText("I Agree");
		btlog.setOnClickListener(this);
		
		Nation nations = new Nation();
		nations.start();
		
		
		TextView title = (TextView) findViewById(R.id.registerTitle);
		translateEn = (Button) findViewById(R.id.btEng);
		translateEn.setBackgroundResource(R.drawable.agree2);
		translateMy = (Button) findViewById(R.id.btMy);
		title.setText("Terms & Conditions : Consent for Disclosure Of CTOS Report");
		title.setVisibility(View.GONE); 
		webview = (WebView) findViewById(R.id.registerWebview);
		webview2 = (WebView) findViewById(R.id.registerWebview2);
		//webview.getSettings().setSupportZoom(true);
		//webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		//webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		//webview.getSettings().setLoadWithOverviewMode(true);
		//webview.getSettings().setUseWideViewPort(true);
		/*initscale =  1.6685696f;//webview.getScale();
		//float scale = 100 * initscale;
		//webview.setInitialScale((int) scale);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setSupportZoom(true);
		//webview.getSettings().setUseWideViewPort(true);
		webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		*/
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setBuiltInZoomControls(true);
		new sendingData()
		.execute(
				"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4",
				"1");
		//scale = 100 * webview.getScale();
		//webview.setInitialScale((int) scale);
		//webview.loadUrl("file:///android_asset/tc_en.html");
		webview2.getSettings().setSupportZoom(true);
		webview2.getSettings().setBuiltInZoomControls(true);
		//scale = 100 * webview2.getScale();
		//webview2.setInitialScale((int) scale);
		//webview2.loadUrl("file:///android_asset/tc_bm.html");
		webview2.setVisibility(View.GONE);
		
		translateEn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {				
					boolean connection = new ConnectionDetector(RegisterSecondPage.this)
					.isConnectStatus();
					if (connection) {
						new sendingData()
									.execute(
											"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4",
											"1");
						translateEn.setBackgroundResource(R.drawable.agree2);
						translateMy.setBackgroundResource(R.drawable.agree1);
							//webview.loadUrl("file:///android_asset/tc_en.html");
						webview.setVisibility(View.VISIBLE);
						webview2.setVisibility(View.GONE);
					}else{
						new ConnectionDetector(RegisterSecondPage.this).Alert("No connection available.");
					}
			}
		});
		translateMy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				

					boolean connection = new ConnectionDetector(RegisterSecondPage.this)
					.isConnectStatus();
					if (connection) {
							//webview.loadUrl("file:///android_asset/tc_bm.html");
						new sendingData()
						.execute(
								"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/c1dfd96eea8cc2b62785275bca38ac261256e278",
								"0");// bm
						webview.setVisibility(View.GONE);
						webview2.setVisibility(View.VISIBLE);
						translateEn.setBackgroundResource(R.drawable.agree1);
						translateMy.setBackgroundResource(R.drawable.agree2);
					}else{
						new ConnectionDetector(RegisterSecondPage.this).Alert("No connection available.");
					}
			}
		});
	}

	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;
		String urlString = "";
		String po = "";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RegisterSecondPage.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				Post post = new Post();
				po = "";
				po = post.CallWebService(urls[0], "1", "");
				urlString = urls[0];
				if (urls[1].equals("1"))
					initial = "1";
				else
					initial = "0";

				return "1";
			} catch (Exception e) {
				this.exception = e;
				return "0";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if(feed.equals("1")){
				try{
					String finalString="";
					for(int i=0;i<po.length();i++)
					{
						if(po.charAt(i)!='\\')
								finalString+=po.charAt(i);
						
					}
					String sXML = "";
					sXML = finalString.replaceAll("rn", "");
					int index = 0;
					if((index = sXML.indexOf("content"))!=-1){
						sXML = sXML.substring(index+10, sXML.length()-2);
					}
					//Spanned span = Html.fromHtml(sXML);
					//sXML = span.toString();
					//sXML = TextUtils.htmlEncode(sXML);
					
					
					if(initial.equals("1")){
						webview.loadData(sXML, "text/html", "utf-8");
					}else{
						webview2.loadData(sXML, "text/html", "utf-8");
					}
					
					
				}catch(Exception e){
					ReturnMessage.showAlertDialog(RegisterSecondPage.this,
							"Internet is invalid.");
				}
			}else{
				ReturnMessage.showAlertDialog(RegisterSecondPage.this,
						"Internet is invalid.");
			}
		}
	}
	public void onClick(View v){
		if(v.getId() == R.id.btLog){
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				SharedPreferences settings = this.getSharedPreferences("REG", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("front", "");
				editor.putString("back", "");
				editor.commit();
				Intent intent = new Intent(this, RegisterIntroPage.class);
				intent.putExtra("userInfo", userInfo);
				startActivity(intent);
				finish();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterFirstPage.class);
		this.startActivity(intent);
		this.finish();
	}

	private class Nation extends Thread {
		@Override
		public void run() {
			webservice();
		}
	}

	public void webservice() {
		boolean connection = new ConnectionDetector(this).isConnectStatus();
		if (connection) {
			MemberWebService nation = new MemberWebService();
			nation.getLsNationality();
			nation.result.toString();
			itr = nation.result.iterator();
			Log.d("Nation", nation.result.toString());
			Looper.prepare();
			Log.d("Test", "test1");
			if (nation.result.get(0).containsKey("name")) {
				Log.d("Test", "test2");
				while (itr.hasNext()) {
					HashMap<String, String> map = (HashMap<String, String>) itr
							.next();
					name = map.get("name");
					sNationality1 = map.get("code");
					Log.d("Name", name);
					Log.d("nationality", sNationality1);
					if (name.contains("Malaysian")
							&& sNationality1.contains("WARGANEGARA")) {
						namemalaysian = name;
						warganegara = sNationality1;
						userInfo.put("Malaysian", namemalaysian);
						userInfo.put("WARGANEGARA", warganegara);
						Log.d("namemalaysian", namemalaysian);
						Log.d("warganegara", warganegara);
	
					} else {
						nonmalaysian = name;
						foreigner = sNationality1;
						userInfo.put("NonMalaysian", nonmalaysian);
						userInfo.put("FOREIGNER", foreigner);
						Log.d("nonmalaysian", nonmalaysian);
						Log.d("foreigner", foreigner);
					}
	
				}
	
				Looper.loop();
			}
		}else{
			RegisterSecondPage.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					new ConnectionDetector(RegisterSecondPage.this).Alert("No connection available.");
					finish();
				}
			});
		}
	}
	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}