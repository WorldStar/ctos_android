package com.justmobile.ctos.profiletab;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.content.ContentResolver;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.profiletab.RegisterFifthPage.SMSTask;
import com.justmobile.ctos.profiletab.RegisterThirdPage.RetreiveFeedTask;
import com.justmobile.ctos.sms.SmsReceiver;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.webservice.MemberWebService;
import com.justmobile.ctos.webservice.WebServiceLink;

public class RegisterFourthPage extends Activity implements
		OnClickListener, OnEditorActionListener {

	CheckBox cbCorresponseAddress;
	EditText etCorrAddress1, etCorrAddress2, etCorrAddress3, etCorrCity,
			etCorrPostcode, etCorrState, etMobileSuffix, etEmail;
	Button btCorrCountry, btMobilePrefix, btCorrState;
	TextView tvCTOSIDEmail;
	String selectionList[], selectionListCode[], selectionListCode2[];
	String selectionList1 [];
	String selectionList2 [];
	String sCorrCountry = "", sCorrState = "";
	String sCcountry ="", sCstate = "";
	String sMobilePrefix = "";
	private ProgressDialog pDialog;
	Boolean isLocal;
	boolean cacflg = false; 
	HashMap<String, String> userInfo = new HashMap<String, String>();
	Typeface font1,font2;
	Button cancel, agree;
	boolean emailflg = false;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_user2);
		emailflg = false;
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		TextView etpnum = (TextView)findViewById(R.id.etpNum);
		etpnum.setVisibility(View.VISIBLE);
		etpnum.setText("Page(2/5)");
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		new RetreiveFeedTask().execute();
		//new RetreiveFeedTask().execute();
		isLocal = getIntent().getBooleanExtra("isLocal", true);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		Log.d("userInfo @ intent 4th page", userInfo.toString());

		cbCorresponseAddress = (CheckBox) findViewById(R.id.cbCorresponseAddress);

		etCorrAddress1 = (EditText) findViewById(R.id.etCorrAddress1);
		etCorrAddress1.setSingleLine();
		etCorrAddress2 = (EditText) findViewById(R.id.etCorrAddress2);
		etCorrAddress2.setSingleLine();
		etCorrAddress3 = (EditText) findViewById(R.id.etCorrAddress3);
		etCorrAddress3.setSingleLine();
		etCorrCity = (EditText) findViewById(R.id.etCorrCity);
		etCorrPostcode = (EditText) findViewById(R.id.etCorrPostcode);
		etCorrState = (EditText) findViewById(R.id.etCorrState);

		etMobileSuffix = (EditText) findViewById(R.id.etMobileSuffix);
		etMobileSuffix.setSingleLine();
		etEmail = (EditText) findViewById(R.id.etEmail);
		etEmail.setOnEditorActionListener(this);
		btCorrCountry = (Button) findViewById(R.id.btCorrCountry);
		btCorrCountry.setOnClickListener(this);
		btCorrState = (Button) findViewById(R.id.btCorrState);
		btCorrState.setOnClickListener(this);

		btCorrState.setVisibility(View.GONE);
		etCorrState.setVisibility(View.VISIBLE);

		btMobilePrefix = (Button) findViewById(R.id.btMobilePrefix);
		btMobilePrefix.setSingleLine();
		btMobilePrefix.setOnClickListener(this);
		if(userInfo.get("corresflg") != null){
			if(userInfo.get("corresflg").equals("1")){
				cbCorresponseAddress.setChecked(true);
			}else{
				cbCorresponseAddress.setChecked(false);
			}
			etCorrAddress1.setText(userInfo.get("corraddress1"));
			etCorrAddress2.setText(userInfo.get("corraddress2"));
			etCorrAddress3.setText(userInfo.get("corraddress3"));
			etCorrCity.setText(userInfo.get("corrcity"));
			etCorrPostcode.setText(userInfo.get("corrpostcode"));
			sCcountry=userInfo.get("nationality");
			sCorrCountry = userInfo.get("corrcountry");
			if(sCorrCountry.equals("MY")){
				etCorrState.setVisibility(View.GONE);
				btCorrState.setVisibility(View.VISIBLE);
				btCorrState.setText(userInfo.get("corrstate"));
			}else{
				etCorrState.setVisibility(View.VISIBLE);
				btCorrState.setVisibility(View.GONE);
				etCorrState.setText(userInfo.get("corrstate"));
			}
			sCstate = userInfo.get("corrstate");
			sCorrState = userInfo.get("scorrstate");
			btCorrCountry.setText(userInfo.get("sCcountry"));
			btMobilePrefix.setText(userInfo.get("mobileNumberPrefix"));
			etMobileSuffix.setText(userInfo.get("mobileNumberSuffix"));
			etEmail.setText(userInfo.get("email"));
			sMobilePrefix = userInfo.get("mobileNumberPrefix");			

		}
		//tvCTOSIDEmail = (TextView) findViewById(R.id.tvCTOSIDEmail);
		cacflg = false;
		cbCorresponseAddress
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton button,
							boolean checked) {
						// TODO Auto-generated method stub
						if (button.isChecked()) {
							etCorrAddress1.setText(userInfo.get("address1"));
							etCorrAddress2.setText(userInfo.get("address2"));
							etCorrAddress3.setText(userInfo.get("address3"));
							etCorrCity.setText(userInfo.get("city"));
							etCorrPostcode.setText(userInfo.get("postcode"));
							btCorrCountry.setText(userInfo.get("nationality"));
							sCcountry=userInfo.get("nationality");
							sCorrCountry = userInfo.get("country");
							if(sCorrCountry.equals("MY")){
								btCorrState.setText(userInfo.get("state"));
								sCstate = userInfo.get("state");
								sCorrState = userInfo.get("sState");
								etCorrState.setVisibility(View.GONE);
								btCorrState.setVisibility(View.VISIBLE);
							}else{
								etCorrState.setText(userInfo.get("state"));
								etCorrState.setVisibility(View.VISIBLE);
								btCorrState.setVisibility(View.GONE);
							}
						} else {
							if(userInfo.get("corresflg") != null){
								etCorrAddress1.setText(userInfo.get("corraddress1"));
								etCorrAddress2.setText(userInfo.get("corraddress2"));
								etCorrAddress3.setText(userInfo.get("corraddress3"));
								etCorrCity.setText(userInfo.get("corrcity"));
								etCorrPostcode.setText(userInfo.get("corrpostcode"));
								btCorrCountry.setText(userInfo.get("sCcountry"));
								sCcountry=userInfo.get("sCcountry");
								sCorrCountry = userInfo.get("corrcountry");

								if(sCorrCountry.equals("MY")){
									btCorrState.setText(userInfo.get("state"));
									sCstate = userInfo.get("corrstate");
									sCorrState = userInfo.get("scorrstate");
									etCorrState.setVisibility(View.GONE);
									btCorrState.setVisibility(View.VISIBLE);
								}else{
									etCorrState.setVisibility(View.VISIBLE);
									btCorrState.setVisibility(View.GONE);
									etCorrState.setText(userInfo.get("corrstate"));
								}
								btMobilePrefix.setText(userInfo.get("mobileNumberPrefix"));
								etMobileSuffix.setText(userInfo.get("mobileNumberSuffix"));
								etEmail.setText(userInfo.get("email"));
								sMobilePrefix = userInfo.get("mobileNumberPrefix");

								etCorrAddress1.setText("");
								etCorrAddress2.setText("");
								etCorrAddress3.setText("");
								etCorrCity.setText("");
								etCorrPostcode.setText("");
								etCorrState.setText("");
								btCorrCountry.setText("");
								sCorrCountry = "";
								sCorrState = "";
								sCstate = "";
								sCcountry="";
							}else{
								etCorrAddress1.setText("");
								etCorrAddress2.setText("");
								etCorrAddress3.setText("");
								etCorrCity.setText("");
								etCorrPostcode.setText("");
								etCorrState.setText("");
								btCorrCountry.setText("");
								sCcountry="";
								sCorrCountry = "";
								sMobilePrefix = "";
								sCorrCountry = "";
								sCorrState = "";
								sCstate = "";
								etCorrState.setVisibility(View.VISIBLE);
								btCorrState.setVisibility(View.GONE);
							}
						}
					}
				});
	}


	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	    if((actionId == EditorInfo.IME_ACTION_DONE) ||
	            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
	        // TODO Auto-generated method stub
	    	//if(etState.length() > 0){	    	
	    	onAgree();
	    }
	    return false;
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(RegisterFourthPage.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						sendPage();
					}
				}).show();
	}
	public void sendPage(){
		Log.d("userInfo @ register 4th page", userInfo.toString());
		Intent intent = new Intent(this, RegisterFifthPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btCancel){
			Intent intent1 = new Intent(this, RegisterThirdPage.class);
			intent1.putExtra("userInfo", userInfo);
			this.startActivity(intent1);
			this.finish();
		}else if(v.getId() == R.id.btAgree){
			onAgree();
		}else if (v.getId() == R.id.btMobilePrefix) {		

			AlertDialog.Builder prefixBuilder = new AlertDialog.Builder(this);
			prefixBuilder.setTitle("Pick a Mobile Prefix / Kod Telefon:");
			prefixBuilder.setItems(selectionList1,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btMobilePrefix.setText(selectionList1[item]);
							sMobilePrefix = selectionList1[item];
							Log.d("prefix selected", sMobilePrefix);
							dialog.dismiss();
						}
					});
			AlertDialog prefixAlert = prefixBuilder.create();
			prefixAlert.show();
		}else if(v.getId() == R.id.btCorrCountry){

			AlertDialog.Builder countryBuilder = new AlertDialog.Builder(this);
			countryBuilder.setTitle("Pick a country:");
			countryBuilder.setItems(selectionList,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btCorrCountry.setText(selectionList[item]);
							sCorrCountry = selectionListCode[item];
							sCcountry = selectionList[item];
							if(sCorrCountry.equals("MY")){
								etCorrState.setVisibility(View.GONE);
								btCorrState.setVisibility(View.VISIBLE);
								sCorrState = "KL";
								sCstate = "W.PERSEKUTUAN(KL)";
								btCorrState.setText(sCstate);
							}else{
								etCorrState.setVisibility(View.VISIBLE);
								btCorrState.setVisibility(View.GONE);
								etCorrState.setText("");
							}
							Log.d("country selected", sCorrCountry);
							dialog.dismiss();
						}
					});
			AlertDialog countryAlert = countryBuilder.create();
			countryAlert.show();
		}else if(v.getId() == R.id.btCorrState){

			AlertDialog.Builder countryBuilder = new AlertDialog.Builder(this);
			countryBuilder.setTitle("Pick a state:");
			countryBuilder.setItems(selectionList2,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btCorrState.setText(selectionList2[item]);
							sCorrState = selectionListCode2[item];
							sCstate = selectionList2[item];
							dialog.dismiss();
						}
					});
			AlertDialog countryAlert = countryBuilder.create();
			countryAlert.show();
		}
	}
	class RetreiveFeedTask extends AsyncTask<String, String, String> {

		private Exception exception;

		protected String doInBackground(String... urls) {
			try {
				MemberWebService nation = new MemberWebService();
				List<HashMap<String, String>> temp = nation.getLsCountry();
				int sizeTemp = temp.size();
				selectionList = new String[sizeTemp];
				selectionListCode = new String[sizeTemp];
				for (int i = 0; i < sizeTemp; i++) {
					selectionList[i] = temp.get(i).get("name");
					selectionListCode[i] = temp.get(i).get("code");
				}
				MemberWebService nation1 = new MemberWebService();
				List<HashMap<String, String>> temp1 = nation1.getLsMobilePrefix();
				int sizeTemp1 = temp1.size();
				selectionList1 = new String[sizeTemp1];
				for (int i = 0; i < sizeTemp1; i++) {
					Log.d(""+i,temp1.get(i).get("code") );
					selectionList1[i] = temp1.get(i).get("code");
				}
				MemberWebService nation2 = new MemberWebService();
				List<HashMap<String, String>> temp2 = nation2.getLsState();
				int sizeTemp2 = temp2.size();
				selectionList2 = new String[sizeTemp2];
				selectionListCode2 = new String[sizeTemp2];
				for (int i = 0; i < sizeTemp2; i++) {
					selectionList2[i] = temp2.get(i).get("name");
					selectionListCode2[i] = temp2.get(i).get("code");
				}

				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (feed.equals("success")) {
				Log.d("success", "success");
			}
		}
	}
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterThirdPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}

	class MobileCheckTask extends AsyncTask<String, String, String> {

		private Exception exception;
		ProgressDialog MyDialog;
		protected String doInBackground(String... urls) {
			try {
				MemberWebService mobile = new MemberWebService();
				List<HashMap<String, String>> temp = mobile.getCheckMobile(sMobilePrefix, etMobileSuffix.getText().toString());
				if (temp.get(0).get("status").equals("1"))
					return "Success";
				else
					return temp.get(0).get("errorMessage");
			} catch (Exception e) {
				this.exception = e;
				return "Internet is invalid.";
			}
		}
		protected void onPreExecute() {
			super.onPreExecute();	
			//MyDialog = ProgressDialog.show(RegisterSeventhPage.this, "",
					//"Registering...", true);

			MyDialog = new ProgressDialog(RegisterFourthPage.this);
			MyDialog.setMessage("Mobile number checking...");
			MyDialog.setIndeterminate(false);
			MyDialog.setCancelable(false);
			MyDialog.show();
		}
		protected void onPostExecute(String feed) {
			MyDialog.dismiss();
			if (feed.equals("Success")) {
				Log.d("success", "Success");
				Alert("Your TAC number will be sent via SMS to your registered mobile phone number " + userInfo.get("mobileNumberPrefix") + "-" + userInfo.get("mobileNumberSuffix"));
			}else{
				sMobilePrefix = "";
				btMobilePrefix.setText("");
				etMobileSuffix.setText("");
				userInfo.put("mobileNumberPrefix", sMobilePrefix);
				userInfo.put("mobileNumberSuffix", etMobileSuffix.getText().toString());
				
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RegisterFourthPage.this);
				builder.setMessage("" + feed)
						.setTitle("CTOS")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();										
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}
	public void onAgree(){

		try{
		if (sMobilePrefix.equals("")) {
			ReturnMessage.showAlertDialog(this,
					"Please select Mobile Prefix");
		} else if (etMobileSuffix.length() == 0) {
			ReturnMessage.showAlertDialog(this, "Mobile Number incorrect.");
			etMobileSuffix.requestFocus();
		}else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches() && etEmail.length() != 0) {
				ReturnMessage.showAlertDialog(this, "Email incorrect.");
				etEmail.requestFocus();
		}else if(etEmail.length() == 0){
			String msg = "The personal email field is empty. We strongly recommend you to submit your email address. This is enable us to communicate easily and directly with you at anytime. To continue insert personal email, please click �OK� or click �Cancel� to continue your registration.";

			new AlertDialog.Builder(RegisterFourthPage.this)
			.setTitle("CTOS")
			.setMessage(msg)

			.setPositiveButton("OK", new DialogInterface.OnClickListener() {

	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing but close the dialog
	        	etEmail.requestFocus();
	            dialog.dismiss();
	        }
	    	
		    })		
		    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing
		        	nextProcess();
		            dialog.dismiss();
		        }
		    })
		    .show();
		}else{
			nextProcess();
		}
		}catch(Exception e){
			ReturnMessage.showAlertDialog(this, "Please fill in the details.");
		}
	}
	public void nextProcess(){
		if(cbCorresponseAddress.isChecked()){
			userInfo.put("corresflg", "1");
		}else{
			userInfo.put("corresflg", "0");
		}
		userInfo.put("corraddress1", etCorrAddress1.getText()
				.toString());
		userInfo.put("corraddress2", etCorrAddress2.getText()
				.toString());
		userInfo.put("corraddress3", etCorrAddress3.getText()
				.toString());
		userInfo.put("corrpostcode", etCorrPostcode.getText()
				.toString());
		userInfo.put("corrcity", etCorrCity.getText().toString());
		
		if(sCorrCountry.equals("MY")){
			userInfo.put("corrstate", sCstate);
			userInfo.put("scorrstate", sCorrState);
		}else{
			userInfo.put("corrstate", etCorrState.getText().toString());
			sCstate = etCorrState.getText().toString();
			sCorrState = "";
		}
		userInfo.put("corrcountry", sCorrCountry);
		userInfo.put("sCcountry", sCcountry);
		userInfo.put("corrstate", sCstate);
		userInfo.put("scorrstate", sCorrState);
		userInfo.put("email", etEmail.getText().toString());
		WebServiceLink.cacflg = true;

		boolean connection = new ConnectionDetector(this)
		.isConnectStatus();
		if (connection) {
			String sp = userInfo.get("mobileNumberPrefix");
			String up = userInfo.get("mobileNumberSuffix");
			if(sp == null)sp  ="";
			if(up == null)up = "";
			sp = sp+up;
			String tp = sMobilePrefix + etMobileSuffix.getText().toString();
			if(sp.equals(tp)){
				userInfo.put("mobileNumberPrefix", sMobilePrefix);
				userInfo.put("mobileNumberSuffix", etMobileSuffix.getText().toString());
				userInfo.put("tacflg", "1");
				sendPage();
			}else{
				userInfo.put("mobileNumberPrefix", sMobilePrefix);
				userInfo.put("mobileNumberSuffix", etMobileSuffix.getText().toString());
				userInfo.put("tacflg", "0");
				userInfo.put("cac", "");
				new MobileCheckTask().execute();
			}
		}else{
			new ConnectionDetector(this).Alert("No connection available.");
		}
		//sendPage();
		//new SMSTask().execute();		
	}
}