package com.justmobile.ctos.profiletab;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.profiletab.ProfileAfter.reqSMS;
import com.justmobile.ctos.profiletab.ProfileAfter.retreiveData;
import com.justmobile.ctos.profiletab.ProfileAfter.sendingData;
import com.justmobile.ctos.servicestab.SelfCheck1;
import com.justmobile.ctos.sms.SmsReceiver;
import com.justmobile.ctos.webservice.MemberWebService;

public class ProfileAfterUpdate extends Activity {
	TextView tvName, tvNational, tvIc, tvEmail, tvMobile, tvOld, tvGender, tvAddress;
	SharedPreferences settings;
	HashMap<String, String> userInfo = new HashMap<String, String>();
	LinearLayout lay;
	ScrollView scrollview;
	Button more_btn,cacButton;
	RelativeLayout mylayout;
	ImageView imageViewprof;
	String CID, Key;
	boolean cacflg = false; 
	String selectionList[], selectionListCode[],selectionList2[], selectionListCode2[], selectionList1[];
	private ProgressDialog pDialog;
	String np = "";
	String npc = "";
	String op = "";
	String a1 = "";
	String a2 = "";
	String a3 = "";
	String ac = "";
	String acity = "";
	String astate = "";
	String acac = "";
	String anum = "";
	String aemail = "";
	String name = "", nat = "", newIc = "", oldIc = "", gender = "",
			address1 = "", address2 = "", address3 = "", city = "", state = "",
			pcode = "", country = "", cardphoto = "";
	String cstate1 = "", caddress1 = "", caddress2 = "", caddress3 = "",
			cpcode1 = "", csstate = "", ccountry = "", ccity1 = "", cmbs = "", cmnp = "",
			email = "";
	String oldPass="";
	String cac="";
	String city12 = "";
	String state12 = "";
	String pcode12 = "";
	String prefixnumber = "";
	String oldnumber = "";
	boolean c1 = false, c2 = false, c3 = false;
	LinearLayout updatescroll;
	Button btUpdate,  btChange, btcancel, btsubmit, btcancel1, btsubmit1;
	EditText add1, add2, add3, ecpcode, eccity, ecstate, cnational, cemail,
			cprefix, ccac,btstate, cnumber, oldpass, newpass, newpassconfirm;

	PopupWindow pw, pwchange;
	Dialog cDialog;
	public boolean flg = false;
	boolean mobileflg = false;
	Typeface font1, font2;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profileupdate);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Profile Update");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		mobileflg = false;
		try {
			email = settings.getString("email", null);
			CID= settings.getString("CID", null);
			Key = settings.getString("Key", null);
		} catch (Exception e) {
			Log.d("Key", "123");
		}
		add1 = (EditText)findViewById(R.id.add1);
		add1.setTextColor(Color.GRAY);
		add1.addTextChangedListener(a1textWatcher);
		add2 = (EditText) findViewById(R.id.add2);
		add2.setTextColor(Color.GRAY);
		add2.addTextChangedListener(a2textWatcher);
		add3 = (EditText) findViewById(R.id.add3);
		add3.setTextColor(Color.GRAY);
		add3.addTextChangedListener(a3textWatcher);
		ecpcode = (EditText) findViewById(R.id.cpcode);
		ecpcode.setTextColor(Color.GRAY);
		ecpcode.addTextChangedListener(ctextWatcher);
		cnational = (EditText) findViewById(R.id.cnational);
		eccity = (EditText) findViewById(R.id.ccity);
		eccity.setTextColor(Color.GRAY);
		eccity.addTextChangedListener(cctextWatcher);
		ecstate = (EditText) findViewById(R.id.cstate);
		btstate = (EditText)findViewById(R.id.btstate);
		btstate.setTextColor(Color.GRAY);
		ecstate.setTextColor(Color.GRAY);
		ecstate.addTextChangedListener(cstextWatcher);
		cemail = (EditText) findViewById(R.id.cemail);
		cemail.setTextColor(Color.GRAY);
		cemail.addTextChangedListener(cetextWatcher);
		cprefix = (EditText) findViewById(R.id.cprefix);
		cnumber = (EditText) findViewById(R.id.cnumber);
		cnumber.setTextColor(Color.GRAY);
		cnumber.addTextChangedListener(cnumtextWatcher);
		ccac = (EditText) findViewById(R.id.ccac);
		ccac.setTextColor(Color.GRAY);
		ccac.addTextChangedListener(ccactextWatcher);
		cacButton = (Button) findViewById(R.id.cacCodeBtn);
		ccac.setVisibility(View.GONE);
		cacButton.setVisibility(View.GONE);
		btcancel = (Button) findViewById(R.id.btcancel);
		btsubmit = (Button) findViewById(R.id.btsubmit);
		btcancel.setOnClickListener(cancel_button_click_listener);
		btsubmit.setOnClickListener(submit_button_click_listener);

		cacButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean connection = new ConnectionDetector(ProfileAfterUpdate.this)
				.isConnectStatus();
				if (connection) {	
					String ph = cprefix.getText().toString()+"-"+cnumber.getText().toString();
					String ph1 = cmnp+"-"+cmbs;
					if(!ph.equals(ph1)){
					  new MobileCheckTask().execute();
					}
				}else{
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("No connection available.");
				}
				
			}
		});
		cnational.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
					countryset();
			}
		});

		btstate.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus){
					if(ccountry.equals("MY")){
					    stateset();
					}				
				}
			}
		});
		btstate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ccountry.equals("MY")){
				    stateset();
				}				
			}
		});
		cprefix.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
					prefixset();
			}
		});

		new retreiveData().execute(CID, Key);
		
	}
	public String s1 = "";
	public String s2 = "";
	public String s3 = "";
	public String s4 = "";
	public String s5 = "";
	public String s6 = "";
	public String s7 = "";
	public String s8 = "";
	public String s9 = "";
	TextWatcher a1textWatcher = new TextWatcher() {   
		boolean flg1 = false;
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add1.setText("Address 1");
        		add1.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s1 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();        	
        	if(s1.length() > tmp.length() && s1.equals("Address 1") && c1 == false){
        		add1.setText("");
        	}
            if(s1.equals("Address 1") && s.length() == (s1.length() + 1)){
            	for(int i = 0; i < s1.length(); i++){
	            	int a = tmp.indexOf(s1.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add1.setText(tmp);
            	add1.setSelection(1);
            	add1.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher a2textWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add2.setText("Address 2");
        		add2.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s2 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
           	
        	if(s2.length() > tmp.length() && s2.equals("Address 2") && c1 == false){
        		add2.setText("");
        	}
            if(s2.equals("Address 2") && s.length() == (s2.length() + 1)){
            	for(int i = 0; i < s2.length(); i++){
	            	int a = tmp.indexOf(s2.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add2.setText(tmp);
            	add2.setSelection(1);
            	add2.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher a3textWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add3.setText("Address 3");
        		add3.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s3 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
           	
        	if(s3.length() > tmp.length() && s3.equals("Address 3") && c1 == false){
        		add3.setText("");
        	}
            if(s3.equals("Address 3") && s.length() == (s3.length() + 1)){
            	for(int i = 0; i < s3.length(); i++){
	            	int a = tmp.indexOf(s3.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add3.setText(tmp);
            	add3.setSelection(1);
            	add3.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher ctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ecpcode.setText("Post Code");
        		ecpcode.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s4 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s4.length() > tmp.length() && s4.equals("Post Code") && c1 == false){
        		ecpcode.setText("");
        	}
            if(s4.equals("Post Code") && s.length() == (s4.length() + 1)){
            	for(int i = 0; i < s4.length(); i++){
	            	int a = tmp.indexOf(s4.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ecpcode.setText(tmp);
            	ecpcode.setSelection(1);
            	ecpcode.setTextColor(Color.BLACK);            	
            }
        }  
    };

	TextWatcher cctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		eccity.setText("City");
        		eccity.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s5 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s5.length() > tmp.length() && s5.equals("City") && c1 == false){
        		eccity.setText("");
        	}
            if(s5.equals("City") && s.length() == (s5.length() + 1)){
            	for(int i = 0; i < s5.length(); i++){
	            	int a = tmp.indexOf(s5.substring(i, i+1));
	            	if(a != -1)
	            		tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	eccity.setText(tmp);
            	eccity.setSelection(1);
            	eccity.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cstextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ecstate.setText("State");
        		ecstate.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s6 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s6.length() > tmp.length() && s6.equals("State") && c1 == false){
        		ecstate.setText("");
        	}
            if(s6.equals("State") && s.length() == (s6.length() + 1)){
            	for(int i = 0; i < s6.length(); i++){
	            	int a = tmp.indexOf(s6.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ecstate.setText(tmp);
            	ecstate.setSelection(1);
            	ecstate.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cetextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		cemail.setText("Email");
        		cemail.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s7 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	//Pattern p = Pattern.compile("Email"); 
			//if (!p.matcher(tmp).matches()){
        	if(s7.length() > tmp.length() && s7.equals("Email") && c1 == false){
        		cemail.setText("");        		
        	}
            if(s7.equals("Email") && s.length() == (s7.length() + 1)){
            	for(int i = 0; i < s7.length(); i++){
	            	int a = tmp.indexOf(s7.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	cemail.setText(tmp);
            	cemail.setSelection(1);
            	cemail.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cnumtextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		cnumber.setText("Number");
        		cnumber.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s8 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s8.length() > tmp.length() && s8.equals("Number") && c1 == false){
        		cnumber.setText("");
        	}
            if(s8.equals("Number") && s.length() == (s8.length() + 1)){
            	for(int i = 0; i < s8.length(); i++){
	            	int a = tmp.indexOf(s8.substring(i, i+1));
	            	if(a == -1){
	            		break;
	            	}
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	cnumber.setText(tmp);
            	cnumber.setSelection(1);
            	cnumber.setTextColor(Color.BLACK);            	
            }
            if(!cnumber.getText().toString().equals(cmbs)){
        		ccac.setVisibility(View.VISIBLE);
        		ccac.setTextColor(Color.GRAY);
        		cacButton.setVisibility(View.VISIBLE);
        		mobileflg = true;
            }else{
	    		ccac.setVisibility(View.GONE);
	    		cacButton.setVisibility(View.GONE);
            }
        }  
    };
	TextWatcher ccactextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ccac.setText("TAC Code");
        		ccac.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s9 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s9.length() > tmp.length() && s9.equals("TAC Code") && c1 == false){
        		ccac.setText("");
        	}
            if(s9.equals("TAC Code") && s.length() == (s9.length() + 1)){
            	for(int i = 0; i < s9.length(); i++){
	            	int a = tmp.indexOf(s9.substring(i, i+1));
	            	if(a != -1)
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ccac.setText(tmp);
            	ccac.setSelection(1);
            	ccac.setTextColor(Color.BLACK);            	
            }
        }  
    };

	private OnClickListener submit_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			boolean connection = new ConnectionDetector(ProfileAfterUpdate.this)
			.isConnectStatus();
			if (connection) {
				if(add1.getText().toString().equals("Address 1")){
					a1 = "";
				}else{
					a1 = add1.getText().toString();
				}
				if(add2.getText().toString().equals("Address 2")){
					a2 = "";
				}else{
					a2 = add2.getText().toString();
				}
				if(add3.getText().toString().equals("Address 3")){
					a3 = "";
				}else{
					a3 = add3.getText().toString();
				}
				if(ecpcode.getText().toString().equals("Post Code")){
					ac = "";
				}else{
					ac = ecpcode.getText().toString();
				}
				if(eccity.getText().toString().equals("City")){
					acity = "";
				}else{
					acity = eccity.getText().toString();
				}
				if(ecstate.getText().toString().equals("State") && !ccountry.equals("MY")){
					astate = "";
				}else if(btstate.getText().toString().equals("State") && ccountry.equals("MY")){
					astate = "";
				}else if(ccountry.equals("MY")){
					astate = btstate.getText().toString();
				}else if(!ccountry.equals("MY")){
					astate = ecstate.getText().toString();
				}
				if(ccac.getText().toString().equals("TAC Code")){
					acac = "";
				}else{
					acac = ccac.getText().toString();
				}
				if(cnumber.getText().toString().equals("Number")){
					anum = "";
				}else{
					anum = cnumber.getText().toString();
				}
				if(cemail.getText().toString().equals("Email")){
					aemail = "";
				}else{
					aemail = cemail.getText().toString();
				}
				Pattern p2 = Pattern.compile("[ ]*"); 
				String newnumber = cprefix.getText().toString() + anum;
				if(mobileflg && acac.equals("") && !newnumber.equals(oldnumber)){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("TAC Code is empty.");
				}else if(p2.matcher(acity).matches()){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("City is empty.");
				}else if(a1.equals("")){				
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("Address 1 is empty.");
				}else if(ac.equals("")){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("Post Code is empty.");
				}else if(acity.equals("")){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("City is empty.");
				}else if(astate.equals("")){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("State is empty.");
				}else if(p2.matcher(astate).matches()){
					new ConnectionDetector(ProfileAfterUpdate.this).Alert("State is empty.");
				}else{
					if(a2.indexOf("Address")!=-1){
						a2 = "";
					}
					if(a3.indexOf("Address")!=-1){
						a3 = "";
					}
					if(a2 == null)a2 = "";
					if(a3 == null)a3 = "";		
					if(mobileflg && !newnumber.equals(oldnumber) && !acac.equals("")){
						cmnp = prefixnumber;
						mobileflg = false;
						updateProfile();
					}else if(mobileflg && !newnumber.equals(oldnumber)){
						new ConnectionDetector(ProfileAfterUpdate.this).Alert("TAC Code is empty.");
					}else{
						updateProfile();
					}
				}
				
			}else{
				new ConnectionDetector(ProfileAfterUpdate.this).Alert("No connection available.");
			}
		}
	};
	public void updateProfile(){
		if(mobileflg)mobileflg = false;
		cmnp = cprefix.getText().toString();
		new sendingData().execute();
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("name", name);
		editor.putString("gender", gender);
		editor.putString("nat", nat);
		editor.putString("newIc", newIc);
		editor.putString("oldIc", oldIc);
		editor.putString("address1", a1);
		editor.putString("address2", a2);
		editor.putString("address3", a3);
		editor.putString("pcode", ac);
		editor.putString("city", acity);
		editor.putString("state", astate);
		editor.putString("cmnp", cmnp);
		editor.putString("cmbs", anum);
		editor.putString("ccountry", ccountry);
		editor.putString("email", aemail);
		editor.putString("cac", acac);
		editor.commit();
	}
	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ProfileAfterUpdate.this);
			pDialog.setMessage("Submitting...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {

				MemberWebService quest = new MemberWebService();

				caddress1 = a1;
				caddress2 = a2;
				caddress3 = a3;
				ccity1 = acity;
				email = aemail;
				cmbs = anum;
				cpcode1 = ac;
				cstate1 = astate;
				cac = acac;
				List<HashMap<String, String>> temp = quest.PmUpdate(CID, Key,cac,
						caddress1, caddress2, caddress3, ccity1, ccountry,
						email, cmnp, cmbs, cpcode1, cstate1);
				if (temp.get(0).get("status").equals("0")){
					if(!temp.get(0).get("message").equals("")){
						if(temp.get(0).get("errorMessage").indexOf("TAC")!=-1){
							return temp.get(0).get("errorMessage");
						}else if(!temp.get(0).get("errorMessage").equals("")){
							return temp.get(0).get("errorMessage");
						}else{
							return temp.get(0).get("message");
						}
					}else{
						return temp.get(0).get("errorMessage");
					}
					
				}else
					return "1";
			} catch (Exception e) {
				this.exception = e;
				return "CID Update failed.";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				//Alert("successful");
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ProfileAfterUpdate.this);
				builder.setMessage("Successful.")
						.setTitle("CTOS")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
										Intent intent = new Intent(ProfileAfterUpdate.this,
												CTOSMainActivity.class);
										intent.putExtra("active", "profile");
										startActivity(intent);
										finish();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
				
			} else {
				Alert(feed);
			}
		}
	}
	
	class reqSMS extends AsyncTask<String, String, String> {

		private Exception exception;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
			pDialog = new ProgressDialog(ProfileAfterUpdate.this);
			pDialog.setMessage("Reading TAC Code...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		protected String doInBackground(String... arg0) {
			try {
				userInfo.put("cac", "");
				userInfo.put("serial", "");
				MemberWebService cacWs = new MemberWebService();
				
				List<HashMap<String, String>> temp1 = cacWs
						.PmCac(	newIc,
								oldIc,
								ccountry,
								//userInfo.get("country"),
								cprefix.getText().toString(),
								cnumber.getText().toString());
				if(temp1.get(0).get("status").equals("0")){
					//ReturnMessage.showAlertDialog(this, "Email incorrect.");
					//etEmail.requestFocus();
					userInfo.put("cac", "");
					userInfo.put("serial", "");
					return "";
				}else{
					cmnp = cprefix.getText().toString();
					ContentResolver contentResolver = getContentResolver();
					Cursor cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
	
					int indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
					int indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
					int indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
					
					if ( indexBody < 0 || !cursor.moveToFirst() ) return "success";
					
					boolean flg = true;
					Calendar c = Calendar.getInstance();
					long lasttime = c.get(Calendar.HOUR) * 3600 + c.get(Calendar.MINUTE)*60 + c.get(Calendar.SECOND);
					String serial1 = temp1.get(0).get("cacSerialNo");
					cursor.moveToNext();
					int i = 0;
					while(flg){
						i++;
						String str = cursor.getString(indexBody);
						String cac1 = "";
						String serial2 = "";
						int index = 0;
						if((index = str.indexOf("TAC Code:")) != -1){
							cac1 = str.substring(index+10, index+16);
							int index1 = str.indexOf("Serial No:");
							serial2 = str.substring(index1+11, index).trim();
						}						
						if((index = serial2.indexOf(serial1)) != -1){
							flg = false;
							userInfo.put("cac", cac1);
							userInfo.put("serial", serial2);					
						}else{
						}
						if(i > 2){
							cursor.close();
							contentResolver = getContentResolver();
							cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
							
							indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
							indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
							indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
							cursor.moveToNext();
							i = 0;
						}else{
							cursor.moveToNext();
						}
						c = Calendar.getInstance();
						long curtime = c.get(Calendar.HOUR) * 3600 + c.get(Calendar.MINUTE) * 60 + c.get(Calendar.SECOND);
						if(Math.abs(lasttime - curtime) > 15){
							flg = false;
							userInfo.put("cac", "");
							userInfo.put("serial", "");
						}
						try{
							Thread.sleep(200);
						}catch(Exception e){}
					}
					//do
					//{
						//String str = "Sender: " + cursor.getString( indexAddr ) + "\n" + cursor.getString( indexBody );
						//smsList.add( str );
					//}
					//while( cursor.moveToNext() );
				}
				cacflg = true;
				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (feed.equals("success")) {
				Log.d("success", "success");
				ccac.setText(userInfo.get("cac"));
				if(userInfo.get("cac").length() > 4){
					ccac.setTextColor(Color.BLACK);
				}
			}
		
		}
		

	}
	public void Alert(String msg) {
		new AlertDialog.Builder(ProfileAfterUpdate.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						//new reqSMS().execute();
					}
				}).show();
	}
	public void Alert3(String msg) {
		new AlertDialog.Builder(ProfileAfterUpdate.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(ProfileAfterUpdate.this,
								CTOSMainActivity.class);
						startActivity(intent);
						finish();
					}
				}).show();
	}
	public void Alert2(String msg) {
		new AlertDialog.Builder(ProfileAfterUpdate.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(ProfileAfterUpdate.this,
								CTOSLoginPage.class);
						startActivity(intent);
						finish();
					}
				}).show();
	}

	public void Alert1(String msg) {
		new AlertDialog.Builder(ProfileAfterUpdate.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						new reqSMS().execute();
					}
				}).show();
	}
	class retreiveData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ProfileAfterUpdate.this);
			pDialog.setMessage("Verify CID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
			boolean connection = new ConnectionDetector(ProfileAfterUpdate.this)
			.isConnectStatus();
			if (connection) {				
			}else{
				finish();
			}
		}

		protected String doInBackground(String... urls) {
			try {
				String tempSelection1 = "", tempselectionList = "", tempselectionListCode = "";
				MemberWebService nation1 = new MemberWebService();
				List<HashMap<String, String>> temp1 = nation1
						.getLsMobilePrefix();
				int sizeTemp1 = temp1.size();
				selectionList1 = new String[sizeTemp1];
				for (int i = 0; i < sizeTemp1; i++) {
					selectionList1[i] = temp1.get(i).get("code");
					tempSelection1 = tempSelection1 + temp1.get(i).get("code")
							+ "|";
				}

				MemberWebService nation = new MemberWebService();
				List<HashMap<String, String>> temp2 = nation.getLsCountry();
				int sizeTemp = temp2.size();
				selectionList = new String[sizeTemp];
				selectionListCode = new String[sizeTemp];
				for (int i = 0; i < sizeTemp; i++) {
					selectionList[i] = temp2.get(i).get("name");
					selectionListCode[i] = temp2.get(i).get("code");
					tempselectionList = tempselectionList
							+ temp2.get(i).get("name") + "|";
					tempselectionListCode = tempselectionListCode
							+ temp2.get(i).get("code") + "|";
				}
				MemberWebService nation11 = new MemberWebService();
				List<HashMap<String, String>> temp11 = nation11.getLsState();
				int sizeTemp11 = temp11.size();
				selectionList2 = new String[sizeTemp11];
				selectionListCode2 = new String[sizeTemp11];
				for (int i = 0; i < sizeTemp11; i++) {
					selectionList2[i] = temp11.get(i).get("name");
					selectionListCode2[i] = temp11.get(i).get("code");
				}

				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest.PmView(urls[0],
						urls[1]);

				if (temp.get(0).containsKey("status")) {
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("message");
					else
						return temp.get(0).get("message");
				} else {
					if (temp.get(1).get("status").equals("0"))
						return temp.get(1).get("message");
					else {
						// city="",state="",pcode="",country=""
						name = temp.get(1).get("name");
						nat = temp.get(1).get("nationality");
						newIc = temp.get(1).get("nic");
						oldIc = temp.get(1).get("ic");
						gender = temp.get(1).get("gender");
						if (gender.equals("L"))
							gender = "MALE";
						else
							gender = "FEMALE";
						address1 = temp.get(1).get("address1");
						if (temp.get(1).containsKey("address2"))
							address2 = temp.get(1).get("address2");
						if (temp.get(1).containsKey("address3"))
							address3 = temp.get(1).get("address3");
						city = temp.get(1).get("city");
						state = temp.get(1).get("state");
						pcode = temp.get(1).get("postcode");
						if (temp.get(1).containsKey("myKadPhoto"))
							cardphoto = temp.get(1).get("myKadPhoto");

						// cstate = "", caddress1 = "", caddress2 = "",
						// caddress3 = "",
						// cpcode = "", ccountry = "", ccity = "", cmbs = "",
						// cmnp = ""

						caddress1 = temp.get(0).get("address1");
						caddress2 = temp.get(0).get("address2");
						caddress3 = temp.get(0).get("address3");
						state12 = temp.get(0).get("state");
						pcode12 = temp.get(0).get("postcode");
						city12 = temp.get(0).get("city");
						cmbs = temp.get(0).get("mobileNumberSuffix");
						cmnp = temp.get(0).get("mobileNumberPrefix");
						email = temp.get(0).get("email");
						ccountry = temp.get(0).get("country");


						return temp.get(1).get("status");
					}
				}

			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				// tvName, tvNational, tvIc, tvOld, tvGender, tvAddress;
				try{
					c1 = true;
					if(caddress1 == null)caddress1 = "";
					add1.setText(caddress1);
					if(!add1.getText().toString().equals("Address 1")){
						add1.setTextColor(Color.BLACK);
					}
					if(caddress2 == null)caddress2 = "";
					add2.setText(caddress2);
					if(!add2.getText().toString().equals("Address 2")){
						add2.setTextColor(Color.BLACK);
					}
					if(caddress3 == null)caddress3 = "";
					add3.setText(caddress3);
					if(!add3.getText().toString().equals("Address 3")){
						add3.setTextColor(Color.BLACK);
					}
					if(pcode12 == null){
						pcode12 = "";
					}
					ecpcode.setText(pcode12);
					if(!ecpcode.getText().toString().equals("Post Code")){
						ecpcode.setTextColor(Color.BLACK);
					}
					for (int i = 0; i < selectionListCode.length; i++) {
						if (selectionListCode[i].equals(ccountry)){
							cnational.setText(selectionList[i]);
						}
					}
					if(city12 == null){
						city12 = "";
					}
					eccity.setText(city12);
					//try{
						//eccity.setText(ccity);
					//}catch(Exception e){
						//ccity = "";
						//eccity.setText(ccity);
					//}
					if(!eccity.getText().toString().equals("City")){
						eccity.setTextColor(Color.BLACK);
					}
					if(state12 == null){
						state12 = "";
					}
					if(ccountry.equals("MY")){
						btstate.setText(state12);
						btstate.setVisibility(View.VISIBLE);
						ecstate.setVisibility(View.GONE);
						if(!btstate.getText().toString().equals("State")){
							btstate.setTextColor(Color.BLACK);
						}
					}else{
						ecstate.setText(state12);
						btstate.setVisibility(View.GONE);
						ecstate.setVisibility(View.VISIBLE);
						if(!ecstate.getText().toString().equals("State")){
							ecstate.setTextColor(Color.BLACK);
						}
					}
					//try{
					//}catch(Exception e){
						//cstate = "";
						//ecstate.setText(cstate);
					//}
					if(email == null){
						email = "";
					}
					cemail.setText(email);
					if(!cemail.getText().toString().equals("Email")){
						cemail.setTextColor(Color.BLACK);
					}
					if(cmbs == null){
						cmbs = "";
					}
					cnumber.setText(cmbs);
					if(!cnumber.getText().toString().equals("Number")){
						cnumber.setTextColor(Color.BLACK);
					}
					if(cmnp == null){
						cmnp = "";
					}
					cprefix.setText(cmnp);
					if(cac == null){
						cac = "";
					}
					if(cac == null)cac = "";
					ccac.setText(cac);
					if(!ccac.getText().toString().equals("Tac code")){
						ccac.setTextColor(Color.BLACK);
					}
					oldnumber = cmnp + cmbs;
					c1 = false;
				}catch(Exception e){
					
				}
			} else {
				Alert(feed);
			}
		}
	}

	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent(ProfileAfterUpdate.this,
					CTOSMainActivity.class);
			intent.putExtra("active", "profile");
			startActivity(intent);
			finish();
		}
	};
	private void countryset() {
		AlertDialog.Builder countryBuilder = new AlertDialog.Builder(
				ProfileAfterUpdate.this);
		countryBuilder.setTitle("Pick a country:");
		countryBuilder.setItems(selectionList,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						cnational.setText(selectionList[item]);
						ccountry = selectionListCode[item];
						if(ccountry.equals("MY")){
							btstate.setVisibility(View.VISIBLE);
							ecstate.setVisibility(View.GONE);
						}else{
							btstate.setVisibility(View.GONE);
							ecstate.setVisibility(View.VISIBLE);
						}
						dialog.dismiss();
					}
				});
		AlertDialog countryAlert = countryBuilder.create();
		countryAlert.show();
	};
	private void stateset() {
		AlertDialog.Builder countryBuilder = new AlertDialog.Builder(
				ProfileAfterUpdate.this);
		countryBuilder.setTitle("Pick a state:");
		countryBuilder.setItems(selectionList2,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						btstate.setText(selectionList2[item]);
						btstate.setTextColor(Color.BLACK);
						csstate = selectionListCode2[item];
						dialog.dismiss();
					}
				});
		AlertDialog countryAlert = countryBuilder.create();
		countryAlert.show();
	};

	private void prefixset() {
		AlertDialog.Builder prefixBuilder = new AlertDialog.Builder(
				ProfileAfterUpdate.this);
		prefixBuilder.setTitle("Pick a prefix:");
		prefixBuilder.setItems(selectionList1,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						cprefix.setText(selectionList1[item]);
			            if((!cprefix.getText().toString().equals(cmnp))){
			        		ccac.setVisibility(View.VISIBLE);
			        		ccac.setTextColor(Color.GRAY);
			        		cacButton.setVisibility(View.VISIBLE);
			        		mobileflg = true;
			            }else{
				    		ccac.setVisibility(View.GONE);
				    		cacButton.setVisibility(View.GONE);
			            }
			            prefixnumber = selectionList1[item];						
						dialog.dismiss();
					}
				});
		AlertDialog prefixAlert = prefixBuilder.create();
		prefixAlert.show();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent service = new Intent(this,
				CTOSMainActivity.class);
		service.putExtra("active", "profile");
		this.startActivity(service);
		this.finish();
	}
	

	class MobileCheckTask extends AsyncTask<String, String, String> {

		private Exception exception;
		ProgressDialog MyDialog;
		protected String doInBackground(String... urls) {
			try {
				MemberWebService mobile = new MemberWebService();
				List<HashMap<String, String>> temp = mobile.getCheckMobile(cprefix.getText().toString(), cnumber.getText().toString());
				if (temp.get(0).get("status").equals("1"))
					return "Success";
				else
					return temp.get(0).get("errorMessage");
			} catch (Exception e) {
				this.exception = e;
				return "Internet is invalid.";
			}
		}
		protected void onPreExecute() {
			super.onPreExecute();	
			//MyDialog = ProgressDialog.show(RegisterSeventhPage.this, "",
					//"Registering...", true);

			MyDialog = new ProgressDialog(ProfileAfterUpdate.this);
			MyDialog.setMessage("Mobile number checking...");
			MyDialog.setIndeterminate(false);
			MyDialog.setCancelable(false);
			MyDialog.show();
		}
		protected void onPostExecute(String feed) {
			MyDialog.dismiss();
			if (feed.equals("Success")) {
				Log.d("success", "Success");
				Alert1("Your TAC number will be sent via SMS to your registered mobile phone number " + cprefix.getText().toString() + "-" + cnumber.getText().toString());
			}else{
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ProfileAfterUpdate.this);
				builder.setMessage("" + feed)
						.setTitle("CTOS")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();										
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}

}