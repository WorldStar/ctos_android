package com.justmobile.ctos.profiletab;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.ReturnMessage;

public class RegisterSixthPage extends Activity implements
		OnClickListener {
	int requestCode;
	static Button btFrontIC, btBackIC;
	ImageView ivFrontIC, ivBackIC;
	File sdImageMainDirectory;
	String tempDirectory;
	HashMap<String, String> userInfo = new HashMap<String, String>();
	HashMap<String, String> userInfoFront = new HashMap<String, String>();
	HashMap<String, String> userInfoBack = new HashMap<String, String>();
	private static final int CAMERA_REQUEST = 1888; 
	private static final int CAMERA_REQ = 1999; 
	private final int ACTIVITY_FRONT_IC_GALLERY = 1;
	private final int ACTIVITY_FRONT_IC_CAMERA = 2;
	private final int ACTIVITY_BACK_IC_GALLERY = 3;
	private final int ACTIVITY_BACK_IC_CAMERA = 4;
	 String frontImage="";
	 String BackImage="";
	 SharedPreferences settings;
	 SharedPreferences.Editor editor;
    Typeface font1, font2;
    Button cancel, agree;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_ic);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		TextView etpnum = (TextView)findViewById(R.id.etpNum);
		etpnum.setVisibility(View.VISIBLE);
		etpnum.setText("Page(4/5)");
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		settings = this.getSharedPreferences("REG", Context.MODE_PRIVATE);
		editor = settings.edit();
		
		btFrontIC = (Button) findViewById(R.id.btFrontIC);
		btBackIC = (Button) findViewById(R.id.btBackIC);

		ivFrontIC = (ImageView) findViewById(R.id.ivFrontIC);
		ivBackIC = (ImageView) findViewById(R.id.ivBackIC);
		if(settings.getString("front", null) != null && !settings.getString("front", null).equals("")){
			Bitmap frontIC;
			byte[] front;
			front = Base64.decode(settings.getString("front", null), Base64.DEFAULT);
			frontIC = BitmapFactory.decodeByteArray(front, 0, front.length);
			int width = frontIC.getWidth();
			int height = frontIC.getHeight();
			float ratio = (float)200 / (float)width;
			//height = (int)((float)height * (float)ratio);
			
			ivFrontIC.setImageBitmap(Bitmap.createScaledBitmap(frontIC, width, height,
					false));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			frontIC.compress(Bitmap.CompressFormat.PNG, 100, baos);    
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            frontImage = Base64.encodeToString(b, Base64.DEFAULT);
		}
		if(settings.getString("back", null) != null && !settings.getString("back", null).equals("")){
			Bitmap backIC;
			byte[] back;
			back = Base64.decode(settings.getString("back", null), Base64.DEFAULT);
			backIC = BitmapFactory.decodeByteArray(back, 0, back.length);
			int width = backIC.getWidth();
			int height = backIC.getHeight();
			float ratio = (float)200 / (float)width;
			//height = (int)((float)height * (float)ratio);
			ivBackIC.setImageBitmap(Bitmap.createScaledBitmap(backIC, width, height,
					false));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			backIC.compress(Bitmap.CompressFormat.PNG, 100, baos);    
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            BackImage = Base64.encodeToString(b, Base64.DEFAULT);
		}
		btFrontIC.setOnClickListener(this);
		btBackIC.setOnClickListener(this);

		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		Log.d("userInfo @ intent 6th page", userInfo.toString());
	}

	@Override
	public void onClick(View v) {
		// variable for startActivityForResult

		// to open gallery
		final Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);//INTERNAL_CONTENT_URI
		if(v.getId() == R.id.btCancel){
			Intent intent1 = new Intent(this, RegisterFifthPage.class);
			intent1.putExtra("userInfo", userInfo);
			this.startActivity(intent1);
			this.finish();
		}else if(v.getId() == R.id.btAgree){
			if (!frontImage.equals("") && !BackImage.equals("")) {
				editor.putString("front", frontImage);
				editor.putString("back", BackImage);
				editor.commit();
				boolean connection = new ConnectionDetector(this)
				.isConnectStatus();
				if (connection) {
					Intent intent = new Intent(this, RegisterSeventhPage.class);
					intent.putExtra("userInfo", userInfo);
					this.startActivity(intent);
					this.finish();
				}else{
					new ConnectionDetector(this).Alert("No connection available.");
				}
			} else {
				ReturnMessage.showAlertDialog(this,
						"Please insert your IC pictures.");
			}
		}else if (v.getId() == R.id.btFrontIC) {
			new AlertDialog.Builder(this)
					.setTitle("Where you want to take your picture?")
					.setPositiveButton("Gallery",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog4,
										int which) {
									// TODO Auto-generated method stub
									startActivityForResult(i,
											ACTIVITY_FRONT_IC_GALLERY);//
								}
							})
					.setNeutralButton("Camera",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog5,
										int arg1) {
									// TODO Auto-generated method stub
									Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
					                startActivityForResult(cameraIntent, CAMERA_REQUEST); 

								}
							}).show();
		}else if(v.getId() == R.id.btBackIC){
			new AlertDialog.Builder(this)
					.setTitle("Where you want to take your picture?")
					.setPositiveButton("Gallery",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog4,
										int which) {
									// TODO Auto-generated method stub
									startActivityForResult(i,
											ACTIVITY_BACK_IC_GALLERY);
								}
							})
					.setNeutralButton("Camera",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog5,
										int arg1) {
									// TODO Auto-generated method stub
									Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
					                startActivityForResult(cameraIntent, CAMERA_REQ); 
								}
							}).show();
		}
	}

//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//
//		switch (requestCode) {
//		case 1:
//			if (resultCode == RESULT_OK) {
//				ivFrontIC.setImageBitmap(getBitmapFromGallery(data));
//				frontICDirectory = tempDirectory;
//			}
//			break;
//		case 2:
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inSampleSize = 4;
//
//			if(!sdImageMainDirectory.equals(null)){
//			Bitmap frontIC = BitmapFactory.decodeFile(
//					sdImageMainDirectory.toString(), options);
//			Log.d("sdImageMainDirectory", sdImageMainDirectory.toString());
//			frontICDirectory = sdImageMainDirectory.toString();
//			Log.d("front", frontIC.getWidth() + " " + frontIC.getHeight());
//			ivFrontIC.setImageBitmap(Bitmap.createScaledBitmap(frontIC, 512,
//					328, false));
//			}
//			break;
//		case 3:
//			if (resultCode == RESULT_OK) {
//				ivBackIC.setImageBitmap(getBitmapFromGallery(data));
//				backICDirectory = tempDirectory;
//			}
//			break;
//		case 4:
//			BitmapFactory.Options options1 = new BitmapFactory.Options();
//			options1.inSampleSize = 4;
//
//			Bitmap backIC = BitmapFactory.decodeFile(
//					sdImageMainDirectory.toString(), options1);
//			Log.d("sdImageMainDirectory", sdImageMainDirectory.toString());
//			backICDirectory = sdImageMainDirectory.toString();
//			ivBackIC.setImageBitmap(Bitmap.createScaledBitmap(backIC, 512, 328,
//					false));
//			break;
//		}
//	}
	protected void onActivityResult(int requestCode, int resultCodeBack, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCodeBack == RESULT_OK && null != data) {  
            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
            Bitmap resizedbitmap=Bitmap.createScaledBitmap(photo, 200, 150, true);
            int width = photo.getWidth();
    		int height = photo.getHeight();
    		int density = photo.getDensity();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();  
            resizedbitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            frontImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.d("String",frontImage);
            ivFrontIC.setImageBitmap(resizedbitmap);
            editor.putString("front", frontImage);
			editor.commit();
         
        }else if (requestCode == CAMERA_REQ && resultCodeBack == RESULT_OK && null != data) {  
            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
            Bitmap resizedbitmap=Bitmap.createScaledBitmap(photo, 200, 150, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();  
            resizedbitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            BackImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.d("StringBack",BackImage);
            ivBackIC.setImageBitmap(resizedbitmap);
			editor.putString("back", BackImage);
			editor.commit();
         
        }else if (resultCodeBack == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { android.provider.MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();             
            //GalleryImageChange gallery = new GalleryImageChange();
            //gallery.setAlbumImageDrawble( picturePath, ivFrontIC ) ;
            Bitmap yourSelectedImage = BitmapFactory.decodeFile(picturePath);
            int width = yourSelectedImage.getWidth();
    		int height = yourSelectedImage.getHeight();
    		int density = yourSelectedImage.getDensity();
    		float ratio = (float)200 / (float)width;
    		height = (int)((float)height * (float)ratio);
            Bitmap resizedImage = Bitmap.createScaledBitmap(yourSelectedImage, 200,
    				150, false);
            
            width = resizedImage.getWidth();
    		height = resizedImage.getHeight();
    		density = resizedImage.getDensity();
    		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    		//resizedImage.compress(Bitmap.CompressFormat.PNG, 100, baos); 
    		resizedImage.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, baos); 
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            if(requestCode == ACTIVITY_FRONT_IC_GALLERY){
	            frontImage = Base64.encodeToString(b, Base64.DEFAULT);
	            ivFrontIC.setImageBitmap(resizedImage);
	            editor.putString("front", frontImage);
				editor.commit();
            }else if(requestCode == ACTIVITY_BACK_IC_GALLERY){
            	BackImage = Base64.encodeToString(b, Base64.DEFAULT);
                ivBackIC.setImageBitmap(resizedImage); 
				editor.putString("back", BackImage);
				editor.commit();      
            }
            //ivFrontIC.setImageBitmap(BitmapFactory.decodeFile(picturePath));
         	
        }
        /*if (requestCode == ACTIVITY_BACK_IC_GALLERY && resultCodeBack == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { android.provider.MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close(); 
            Bitmap yourSelectedImage = BitmapFactory.decodeFile(picturePath);
    		int width = yourSelectedImage.getWidth();
    		int height = yourSelectedImage.getHeight();
    		float ratio = (float)200 / (float)width;
    		height = (int)((float)height * (float)ratio);
            Bitmap resizedImage = Bitmap.createScaledBitmap(yourSelectedImage, 200,
    				height, false);
    		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
    		resizedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);    
            byte[] b = baos.toByteArray();
            //String encodedImage = Base64.encode(b, Base64.DEFAULT);
            BackImage = Base64.encodeToString(b, Base64.DEFAULT);
            ivBackIC.setImageBitmap(resizedImage);          
         
        }*/
       
    } 

	public Bitmap getBitmapFromGallery(Intent data) {

		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaColumns.DATA };

		Cursor cursor = getContentResolver().query(selectedImage,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();

		tempDirectory = filePath;
		Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
		Bitmap resizedImage = Bitmap.createScaledBitmap(yourSelectedImage, 512,
				384, false);
		return resizedImage;
	}

	public void StartCamera() {
		try {
			File root = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "CTOS" + File.separator);
			root.mkdirs();
			sdImageMainDirectory = new File(root, "CTOS"
					+ System.currentTimeMillis() + ".png");
		} catch (Exception e) {
			Log.d("Camara Error", e.toString());
		}
	}
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterFifthPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}
}