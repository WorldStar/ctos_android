package com.justmobile.ctos.profiletab;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.ReturnMessage;

public class RegisterIntroPage extends Activity implements OnClickListener {
	Button translateEn;
	Button translateMy;
	int tr = 0;
	WebView webview;
	static String bm = "", en = "";
	String initial = "";
	HashMap<String, String> userInfo = new HashMap<String, String>();
	
	private ProgressDialog pDialog;
	Typeface font1,font2;
	float initscale = 0f;
	Button cancel, agree;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_intro);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setText("I Agree");
		btlog.setVisibility(View.GONE);
		btlog.setOnClickListener(this);
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);
	}

	public void onClick(View v){
		if(v.getId() == R.id.btCancel){
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				Intent intent = new Intent(this, RegisterSecondPage.class);
				this.startActivity(intent);
				this.finish();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}else if(v.getId() == R.id.btAgree){
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				SharedPreferences settings = this.getSharedPreferences("REG", Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("front", "");
				editor.putString("back", "");
				editor.commit();
				//userInfo.put("Malaysian", namemalaysian);
				//userInfo.put("WARGANEGARA", warganegara);
				Intent intent = new Intent(this, RegisterThirdPage.class);
				intent.putExtra("userInfo", userInfo);
				this.startActivity(intent);
				this.finish();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterSecondPage.class);
		this.startActivity(intent);
		this.finish();
	}
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

			Intent intent = new Intent(this, CTOSMainActivity.class);
			this.startActivity(intent);
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}*/

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}