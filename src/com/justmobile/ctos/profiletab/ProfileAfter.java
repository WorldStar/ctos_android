package com.justmobile.ctos.profiletab;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.Splash;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.infotab.Info;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.sms.SmsReceiver;
import com.justmobile.ctos.webservice.MemberWebService;

public class ProfileAfter extends TabGroupActivity implements OnClickListener {
	TextView tvName, tvNational, tvIc, tvEmail, tvMobile, tvOld, tvGender, tvAddress;
	SharedPreferences settings;
	HashMap<String, String> userInfo = new HashMap<String, String>();
	LinearLayout lay;
	ScrollView scrollview;
	Button more_btn,cacButton;
	RelativeLayout mylayout;
	ImageView imageViewprof;
	String CID, Key;
	boolean cacflg = false; 
	String selectionList[], selectionListCode[],selectionList2[], selectionListCode2[], selectionList1[];
	private ProgressDialog pDialog;
	String np = "";
	String npc = "";
	String op = "";
	String a1 = "";
	String a2 = "";
	String a3 = "";
	String ac = "";
	String acity = "";
	String astate = "";
	String acac = "";
	String anum = "";
	String aemail = "";
	String name = "", nat = "", newIc = "", oldIc = "", passport = "", gender = "",
			address1 = "", address2 = "", address3 = "", city = "", state = "",
			pcode = "", country = "", cardphoto = "";
	String cstate = "", caddress1 = "", caddress2 = "", caddress3 = "",
			cpcode = "", csstate = "", ccountry = "", ccity = "", cmbs = "", cmnp = "",
			email = "";
	String oldPass="";
	String cac="";
	LinearLayout updatescroll;
	Button btUpdate, btChange, btcancel, btsubmit, btcancel1, btsubmit1;
	EditText add1, add2, add3, ecpcode, eccity, ecstate, cnational, cemail,
			cprefix, ccac, cnumber, oldpass, newpass, newpassconfirm;

	PopupWindow pw, pwchange;
	Dialog cDialog;
	public boolean flg = false;
	Typeface font1, font2;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setHasOptionsMenu(true);
		settings = getSharedPreferences("CTOS",
				Context.MODE_PRIVATE);
		try {
			Key = settings.getString("Key", "");
			CID = settings.getString("CID", "");
			oldPass = settings.getString("pass", "");
		} catch (Exception e) {
			Log.d("Key", "123");
		}
		setContentView(R.layout.profiletabafter);		
		ImageView logo = (ImageView)findViewById(R.id.logo);
		ScrollView showScroll = (ScrollView)findViewById(R.id.showScroll);
		
		if(CID != null && !CID.equals("")){	
			logo.setVisibility(View.GONE);
			showScroll.setVisibility(View.VISIBLE);
			//btLogin.setBackgroundResource(R.drawable.cancel1);
			//btLoginForgotPassword.setBackgroundResource(R.drawable.cancel2);
			// ActionBar actionBar = getSherlockActivity().getSupportActionBar();
			// actionBar.setBackgroundDrawable(getResources().getDrawable(
			// R.drawable.topbar01));
			font1= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRIB.TTF");
			 font2= Typeface.createFromAsset(getAssets(),
						"fonts/CALIBRI.TTF");
			TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
			tvPageTitle.setText("Profile");
			tvPageTitle.setTypeface(font1);
			Button btlog = (Button) findViewById(R.id.btLog);
			//btlog.setVisibility(View.GONE);
			if(CID != null && !CID.equals("")){
				 btlog.setText("Logout");
			 }else{
				 btlog.setText("Login");
			 }
			btlog.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
						
						if(CID != null && !CID.equals("")){
							Alert5();
						 }else{
							 Intent intent = new Intent(ProfileAfter.this,
										CTOSLoginPage.class);								
							startActivity(intent);
							finish();
						 }
				}
			});
			tvName = (TextView) findViewById(R.id.tvName);
			tvNational = (TextView) findViewById(R.id.tvNational);
			tvIc = (TextView) findViewById(R.id.tvIc);
			tvOld = (TextView) findViewById(R.id.tvOld);
			tvGender = (TextView) findViewById(R.id.tvGender);
			tvAddress = (TextView) findViewById(R.id.tvAddress);
			tvEmail = (TextView)findViewById(R.id.tvEmail);
			tvMobile = (TextView) findViewById(R.id.tvMobile);
			btChange = (Button) findViewById(R.id.btChange);
			btUpdate = (Button) findViewById(R.id.btUpdate);
			btChange.setBackgroundResource(R.drawable.cancel1);
			btUpdate.setBackgroundResource(R.drawable.cancel1);
			imageViewprof = (ImageView) findViewById(R.id.imageViewprof);
			
			scrollview = (ScrollView)findViewById(R.id.scrollView1);
			//LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scrollview.getLayoutParams();
	        //scrollview.setLayoutParams(params);
			updatescroll = (LinearLayout)findViewById(R.id.updateView);
			updatescroll.setVisibility(View.GONE);
			
			/*update view*/
			
			//LinearLayout.LayoutParams conparams = (LinearLayout.LayoutParams) lay.getLayoutParams();
			//conparams.height = 0;
	        //lay.setLayoutParams(conparams);
	        
	        
	        
			//mylayout = (RelativeLayout)v.findViewById(R.id.myLayout);
			//final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
	                //RelativeLayout.LayoutParams.FILL_PARENT,
	                //160);
			//mylayout.setLayoutParams(layoutParams);
			/*more_btn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(!flg){
						LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scrollview.getLayoutParams();
			            params.height = Splash.width - 110;
			            scrollview.setLayoutParams(params);
						LinearLayout.LayoutParams conparams = (LinearLayout.LayoutParams) lay.getLayoutParams();
						conparams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
				        lay.setLayoutParams(conparams);
						lay.setVisibility(View.VISIBLE);
						flg = true;
					}else{
						LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scrollview.getLayoutParams();
			            params.height = Splash.width - 15;
			            scrollview.setLayoutParams(params);
						LinearLayout.LayoutParams conparams = (LinearLayout.LayoutParams) lay.getLayoutParams();
						conparams.height = 0;
				        lay.setLayoutParams(conparams);
						lay.setVisibility(View.GONE);
						flg = false;
					}
				}
			});*/
			btUpdate.setOnClickListener(this);
			btChange.setOnClickListener(this);

			/*if (settings.contains("got")) {
				if (settings.getString("got", "").equals("1")) {
					new initdataloader().execute();
				} else
					new retreiveData().execute(CID, Key);
			} else*/
			boolean connection = new ConnectionDetector(ProfileAfter.this)
			.isConnectStatus();
			if (connection) {
				new retreiveData().execute(CID, Key);
			}else{
				Alert3("No connection available.");
			}
				
		}else{
			logo.setVisibility(View.VISIBLE);
			showScroll.setVisibility(View.GONE);
			logprocess();
		}

	}
	public void logprocess(){
		//Alert2("Please log in to view/edit your profile.");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Please log in to view/edit your profile.")
				.setCancelable(false)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						Intent intent = new Intent(ProfileAfter.this,
								CTOSLoginPage.class);
						 intent.putExtra("logocontent", "1");
						startActivity(intent);
						finish();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	public String s1 = "";
	public String s2 = "";
	public String s3 = "";
	public String s4 = "";
	public String s5 = "";
	public String s6 = "";
	public String s7 = "";
	public String s8 = "";
	public String s9 = "";
	TextWatcher a1textWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add1.setText("Address 1");
        		add1.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s1 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s1.equals("Address 1") && s.length() == (s1.length() + 1)){
            	for(int i = 0; i < s1.length(); i++){
	            	int a = tmp.indexOf(s1.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add1.setText(tmp);
            	add1.setSelection(1);
            	add1.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher a2textWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add2.setText("Address 2");
        		add2.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s2 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s2.equals("Address 2") && s.length() == (s2.length() + 1)){
            	for(int i = 0; i < s2.length(); i++){
	            	int a = tmp.indexOf(s2.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add2.setText(tmp);
            	add2.setSelection(1);
            	add2.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher a3textWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		add3.setText("Address 3");
        		add3.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s3 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s3.equals("Address 3") && s.length() == (s3.length() + 1)){
            	for(int i = 0; i < s3.length(); i++){
	            	int a = tmp.indexOf(s3.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	add3.setText(tmp);
            	add3.setSelection(1);
            	add3.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher ctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ecpcode.setText("Post Code");
        		ecpcode.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s4 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s4.equals("Post Code") && s.length() == (s4.length() + 1)){
            	for(int i = 0; i < s4.length(); i++){
	            	int a = tmp.indexOf(s4.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ecpcode.setText(tmp);
            	ecpcode.setSelection(1);
            	ecpcode.setTextColor(Color.BLACK);            	
            }
        }  
    };

	TextWatcher cctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		eccity.setText("City");
        		eccity.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s5 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s5.equals("City") && s.length() == (s5.length() + 1)){
            	for(int i = 0; i < s5.length(); i++){
	            	int a = tmp.indexOf(s5.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	eccity.setText(tmp);
            	eccity.setSelection(1);
            	eccity.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cstextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ecstate.setText("State");
        		ecstate.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s6 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s6.equals("State") && s.length() == (s6.length() + 1)){
            	for(int i = 0; i < s6.length(); i++){
	            	int a = tmp.indexOf(s6.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ecstate.setText(tmp);
            	ecstate.setSelection(1);
            	ecstate.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cetextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		cemail.setText("Email");
        		cemail.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s7 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s7.equals("Email") && s.length() == (s7.length() + 1)){
            	for(int i = 0; i < s7.length(); i++){
	            	int a = tmp.indexOf(s7.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	cemail.setText(tmp);
            	cemail.setSelection(1);
            	cemail.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cnumtextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		cnumber.setText("Number");
        		cnumber.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s8 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s8.equals("Number") && s.length() == (s8.length() + 1)){
            	for(int i = 0; i < s8.length(); i++){
	            	int a = tmp.indexOf(s8.substring(i, i+1));
	            	if(a == -1){
	            		break;
	            	}
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	cnumber.setText(tmp);
            	cnumber.setSelection(1);
            	cnumber.setTextColor(Color.BLACK);            	
            }
            if(!cnumber.getText().toString().equals(cmbs)){
        		ccac.setVisibility(View.VISIBLE);
        		cacButton.setVisibility(View.VISIBLE);
            }else{
	    		ccac.setVisibility(View.GONE);
	    		cacButton.setVisibility(View.GONE);
            }
        }  
    };
	TextWatcher ccactextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ccac.setText("Tac code");
        		ccac.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s9 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
            if(s9.equals("Tac code") && s.length() == (s9.length() + 1)){
            	for(int i = 0; i < s9.length(); i++){
	            	int a = tmp.indexOf(s9.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ccac.setText(tmp);
            	ccac.setSelection(1);
            	ccac.setTextColor(Color.BLACK);            	
            }
        }  
    };
	class initdataloader extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(String... urls) {
			try {
				name = settings.getString("name", "");
				nat = settings.getString("nat", "");
				newIc = settings.getString("newIc", "");
				oldIc = settings.getString("oldIc", "");
				gender = settings.getString("gender", "");
				if (gender.equals("L"))
					gender = "MALE";
				else
					gender = "FEMALE";
				address1 = settings.getString("address1", "");
				
				address2 = settings.getString("address2", "");
						
				address3 = settings.getString("address3", "");
				
				city = settings.getString("city", "");
				state = settings.getString("state", "");
				pcode = settings.getString("pcode", "");
				caddress1 = settings.getString("caddress1", "");
				caddress2 = settings.getString("caddress2", "");
				caddress3 = settings.getString("caddress3", "");
				cstate = settings.getString("cstate", "");
				cpcode = settings.getString("cpcode", "");
				ccity = settings.getString("ccity", "");
				cmbs = settings.getString("cmbs", "");
				cmnp = settings.getString("cmnp", "");
				email = settings.getString("email", "");
				cardphoto = settings.getString("cardphoto", "");
				ccountry = settings.getString("ccountry", "");
				cac = settings.getString("cac", "");
				String tempSelection1 = settings.getString("tempSelection1",
						"");
				String tempselectionList = settings.getString(
						"tempselectionList", "");
				String tempselectionListCode = settings.getString(
						"tempselectionListCode", "");

				Log.d("masuk", tempselectionList);

				StringTokenizer st = new StringTokenizer(tempSelection1, "|");
				selectionList1 = new String[st.countTokens()];
				int k = 0;
				while (st.hasMoreElements()) {
					selectionList1[k] = st.nextElement().toString();
					k++;
				}
				StringTokenizer st1 = new StringTokenizer(tempselectionList,
						"|");
				selectionList = new String[st1.countTokens()];
				k = 0;
				while (st1.hasMoreElements()) {
					selectionList[k] = st1.nextElement().toString();
					k++;
				}
				StringTokenizer st2 = new StringTokenizer(
						tempselectionListCode, "|");
				selectionListCode = new String[st2.countTokens()];
				k = 0;
				while (st2.hasMoreElements()) {
					selectionListCode[k] = st2.nextElement().toString();
					k++;
				}

				return "1";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (feed.equals("1")) {
				tvName.setText(name);
				tvNational.setText(nat);
				tvIc.setText(newIc);
				tvOld.setText(oldIc);
				tvGender.setText(gender);
				tvEmail.setText(email);
				tvMobile.setText("+6 "+ cmnp + "-"+cmbs);
				if (cardphoto.length() > 1) {
					byte[] html = Base64.decode(cardphoto, Base64.DEFAULT);
					Bitmap bm = BitmapFactory.decodeByteArray(html, 0,
							html.length);
					imageViewprof.setImageBitmap(Bitmap.createScaledBitmap(bm,
							100, 100, false));
				}
				if(caddress2.indexOf("Address")!=-1 || caddress2 == null)caddress2 = "";
				if(caddress3.indexOf("Address")!=-1 || caddress3 == null)caddress3 = "";
				if(ccity.indexOf("City")!=-1 || ccity == null)ccity = "";
				if(cstate.indexOf("State")!=-1 || cstate == null)cstate = "";
				
				tvAddress.setText(caddress1 + " " + caddress2 + " " + caddress3
						+ " " + cpcode + " " + ccity + " " + cstate);
				
			} else {
				Alert(feed);
			}
		}
	}

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			getSherlockActivity().startActivity(intent);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.commit();
			getSherlockActivity().finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}*/

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(CID != null && !CID.equals("")){	
			if (view.getId() == R.id.btUpdate) {
				//LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scrollview.getLayoutParams();
	            //params.height = Splash.width - 15;
	            //scrollview.setLayoutParams(params);

				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel2);
				boolean connection = new ConnectionDetector(ProfileAfter.this)
				.isConnectStatus();
				if (connection) {	
					Intent intent = new Intent(ProfileAfter.this,
							ProfileAfterUpdate.class);
					startActivity(intent);
					finish();
					//initiatePopupWindow();
				}else{
					new ConnectionDetector(ProfileAfter.this).Alert("No connection available.");
				}
				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
				
			}else if(view.getId() == R.id.btChange){
				//LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) scrollview.getLayoutParams();
	            //params.height = Splash.width - 15;
	            //scrollview.setLayoutParams(params);
	
				boolean connection = new ConnectionDetector(ProfileAfter.this)
				.isConnectStatus();
				if (connection) {
					initiatePopupWindowchange();
				}else{
					new ConnectionDetector(ProfileAfter.this).Alert("No connection available.");
				}
				btChange.setBackgroundResource(R.drawable.cancel2);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
			}else{
				if(CID != null && !CID.equals("")){			
				}else{
					logprocess();
				}
			}
		}else{
			logprocess();
		}
	}

	class retreiveData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ProfileAfter.this);
			pDialog.setMessage("Verify CID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
			boolean connection = new ConnectionDetector(ProfileAfter.this)
			.isConnectStatus();
			if (connection) {				
			}else{
				finish();
			}
		}

		protected String doInBackground(String... urls) {
			try {
				String tempSelection1 = "", tempselectionList = "", tempselectionListCode = "";
				MemberWebService nation1 = new MemberWebService();
				List<HashMap<String, String>> temp1 = nation1
						.getLsMobilePrefix();
				int sizeTemp1 = temp1.size();
				selectionList1 = new String[sizeTemp1];
				for (int i = 0; i < sizeTemp1; i++) {
					selectionList1[i] = temp1.get(i).get("code");
					tempSelection1 = tempSelection1 + temp1.get(i).get("code")
							+ "|";
				}

				MemberWebService nation = new MemberWebService();
				List<HashMap<String, String>> temp2 = nation.getLsCountry();
				int sizeTemp = temp2.size();
				selectionList = new String[sizeTemp];
				selectionListCode = new String[sizeTemp];
				for (int i = 0; i < sizeTemp; i++) {
					selectionList[i] = temp2.get(i).get("name");
					selectionListCode[i] = temp2.get(i).get("code");
					tempselectionList = tempselectionList
							+ temp2.get(i).get("name") + "|";
					tempselectionListCode = tempselectionListCode
							+ temp2.get(i).get("code") + "|";
				}
				MemberWebService nation11 = new MemberWebService();
				List<HashMap<String, String>> temp11 = nation11.getLsState();
				int sizeTemp11 = temp11.size();
				selectionList2 = new String[sizeTemp11];
				selectionListCode2 = new String[sizeTemp11];
				for (int i = 0; i < sizeTemp11; i++) {
					selectionList2[i] = temp11.get(i).get("name");
					selectionListCode2[i] = temp11.get(i).get("code");
				}

				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest.PmView(urls[0],
						urls[1]);

				if (temp.get(0).containsKey("status")) {
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("message");
					else
						return temp.get(0).get("message");
				} else {
					if (temp.get(1).get("status").equals("0"))
						return temp.get(1).get("message");
					else {
						// city="",state="",pcode="",country=""
						name = temp.get(1).get("name");
						nat = temp.get(1).get("nationality");
						newIc = temp.get(1).get("nic");
						passport = temp.get(1).get("passport");
						oldIc = temp.get(1).get("ic");
						gender = temp.get(1).get("gender");
						if (gender.equals("L"))
							gender = "MALE";
						else
							gender = "FEMALE";
						address1 = temp.get(1).get("address1");
						if (temp.get(1).containsKey("address2"))
							address2 = temp.get(1).get("address2");
						if (temp.get(1).containsKey("address3"))
							address3 = temp.get(1).get("address3");
						city = temp.get(1).get("city");
						state = temp.get(1).get("state");
						pcode = temp.get(1).get("postcode");
						if (temp.get(1).containsKey("myKadPhoto"))
							cardphoto = temp.get(1).get("myKadPhoto");

						// cstate = "", caddress1 = "", caddress2 = "",
						// caddress3 = "",
						// cpcode = "", ccountry = "", ccity = "", cmbs = "",
						// cmnp = ""

						caddress1 = temp.get(0).get("address1");
						caddress2 = temp.get(0).get("address2");
						caddress3 = temp.get(0).get("address3");
						cstate = temp.get(0).get("state");
						cpcode = temp.get(0).get("postcode");
						ccity = temp.get(0).get("city");
						cmbs = temp.get(0).get("mobileNumberSuffix");
						cmnp = temp.get(0).get("mobileNumberPrefix");
						email = temp.get(0).get("email");
						ccountry = temp.get(0).get("country");
						
						SharedPreferences.Editor editor = settings.edit();
						editor.putString("got", "1");
						editor.putString("name", name);
						editor.putString("nat", nat);
						editor.putString("newIc", newIc);
						editor.putString("passport", passport);
						editor.putString("oldIc", oldIc);
						editor.putString("gender", temp.get(1).get("gender"));
						editor.putString("address1", address1);
						editor.putString("address2", address2);
						editor.putString("address3", address3);
						editor.putString("city", city);
						editor.putString("state", state);
						editor.putString("pcode", pcode);
						editor.putString("caddress1", caddress1);
						editor.putString("caddress2", caddress2);
						editor.putString("caddress3", caddress3);
						editor.putString("cstate", cstate);
						editor.putString("cpcode", cpcode);
						editor.putString("ccity", ccity);
						editor.putString("cmbs", cmbs);
						editor.putString("cmnp", cmnp);
						editor.putString("email", temp.get(0).get("email"));
						editor.putString("ccountry", ccountry);
						editor.putString("tempSelection1", tempSelection1);
						editor.putString("tempselectionList", tempselectionList);
						editor.putString("tempselectionListCode",
								tempselectionListCode);
						editor.putString("cardphoto", cardphoto);
						editor.commit();

						return temp.get(1).get("status");
					}
				}

			} catch (Exception e) {
				this.exception = e;
				return "Internet is invalid.";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				// tvName, tvNational, tvIc, tvOld, tvGender, tvAddress;
				try{
					if(name == null)name = "";
					if(nat == null)nat = "";
					if(newIc == null)newIc = "";
					if(passport == null)passport = "";
					if(oldIc == null)oldIc = "";
					if(gender == null)gender = "";
					if(email == null)email = "";
					if(ccountry == null)ccountry = "";
					if(ccountry.equals("MY")){
						nat = "WARGANEGARA";	
						tvIc.setText(newIc);					
					}else{
						nat = "FOREIGNER";
						tvIc.setText(passport);
					}
					tvName.setText(name);
					tvNational.setText(nat);
					//tvIc.setText(newIc);
					tvOld.setText(oldIc);
					tvGender.setText(gender);
					tvEmail.setText(email);
					tvMobile.setText("+6 "+ cmnp + "-"+cmbs);
					if (cardphoto.length() > 1) {
						byte[] html = Base64.decode(cardphoto, Base64.DEFAULT);
						Bitmap bm = BitmapFactory.decodeByteArray(html, 0,
								html.length);
						int width = bm.getWidth();
						int height = bm.getHeight();
						float ratio = (float)100 / (float)width;
						height = (int)((float)height * (float)ratio);
						imageViewprof.setImageBitmap(Bitmap.createScaledBitmap(bm,
								100, height, false));						
					}
					if(caddress2 == null || caddress2.indexOf("Address")!=-1){
						caddress2 = "";
					}
					if(caddress3 == null || caddress3.indexOf("Address")!=-1){
					    caddress3 = "";
					}
					if(ccity == null || ccity.indexOf("City")!=-1)ccity = "";
					if(cstate == null || cstate.indexOf("State")!=-1 )cstate = "";
					tvAddress.setText(caddress1 + " " + caddress2 + " " + caddress3
							+ " " + cpcode + " " + ccity + " " + cstate);
				}catch(Exception e){
					Alert("invalid data for internet.");
				}
			} else {
				Alert3(feed);
				
			}
		}
	}

	public void Alert(String msg) {
		new AlertDialog.Builder(ProfileAfter.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						//new reqSMS().execute();
					}
				}).show();
	}
	public void Alert3(String msg) {
		new AlertDialog.Builder(ProfileAfter.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(ProfileAfter.this,
								CTOSMainActivity.class);
						startActivity(intent);
						finish();
					}
				}).show();
	}
	public void Alert2(String msg) {
		new AlertDialog.Builder(ProfileAfter.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(ProfileAfter.this,
								CTOSLoginPage.class);
						startActivity(intent);
						finish();
					}
				}).show();
	}
	public void Alert5() {
		new AlertDialog.Builder(ProfileAfter.this)
		.setTitle("CTOS")
		.setMessage("Are you sure to log out?")

		.setPositiveButton("YES", new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            // Do nothing but close the dialog

			Intent intent = new Intent(ProfileAfter.this,
					CTOSMainActivity.class);
			startActivity(intent);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.remove("CID");
			editor.remove("CIDSTATUS");
			editor.commit();
			finish();
            dialog.dismiss();
	    }
	
	    })
	
	    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
	
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing
	            dialog.dismiss();
	        }
	    })
	    .show();
   }
	public void Alert1(String msg) {
		new AlertDialog.Builder(ProfileAfter.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						new reqSMS().execute();
					}
				}).show();
	}

	private void initiatePopupWindow() {
		try {
			/*cDialog = new Dialog(this);
			cDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			//cDialog.setContentView(R.layout.changepassword);
			
			LinearLayout layout = (LinearLayout) LayoutInflater.from(this).inflate(
					R.layout.profileupdate, null);
			cDialog.setContentView(layout);
			add1 = (EditText)layout.findViewById(R.id.add1);
			add1.setTextColor(Color.GRAY);
			add1.addTextChangedListener(a1textWatcher);
			add2 = (EditText) layout.findViewById(R.id.add2);
			add2.setTextColor(Color.GRAY);
			add2.addTextChangedListener(a2textWatcher);
			add3 = (EditText) layout.findViewById(R.id.add3);
			add3.setTextColor(Color.GRAY);
			add3.addTextChangedListener(a3textWatcher);
			ecpcode = (EditText) layout.findViewById(R.id.cpcode);
			ecpcode.setTextColor(Color.GRAY);
			ecpcode.addTextChangedListener(ctextWatcher);
			cnational = (EditText) layout.findViewById(R.id.cnational);
			eccity = (EditText) layout.findViewById(R.id.ccity);
			eccity.setTextColor(Color.GRAY);
			eccity.addTextChangedListener(cctextWatcher);
			ecstate = (EditText) layout.findViewById(R.id.cstate);
			ecstate.setTextColor(Color.GRAY);
			ecstate.addTextChangedListener(cstextWatcher);
			cemail = (EditText) layout.findViewById(R.id.cemail);
			cemail.setTextColor(Color.GRAY);
			cemail.addTextChangedListener(cetextWatcher);
			cprefix = (EditText) layout.findViewById(R.id.cprefix);
			cnumber = (EditText) layout.findViewById(R.id.cnumber);
			cnumber.setTextColor(Color.GRAY);
			cnumber.addTextChangedListener(cnumtextWatcher);
			ccac = (EditText) layout.findViewById(R.id.ccac);
			ccac.setTextColor(Color.GRAY);
			ccac.addTextChangedListener(ccactextWatcher);
			cacButton = (Button) layout.findViewById(R.id.cacCodeBtn);
			ccac.setVisibility(View.GONE);
			cacButton.setVisibility(View.GONE);
			btcancel = (Button) layout.findViewById(R.id.btcancel);
			btsubmit = (Button) layout.findViewById(R.id.btsubmit);
			btcancel.setOnClickListener(cancel_button_click_listener);
			btsubmit.setOnClickListener(submit_button_click_listener);

			cacButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boolean connection = new ConnectionDetector(ProfileAfter.this)
					.isConnectStatus();
					if (connection) {	
						String ph = cprefix.getText().toString()+"-"+cnumber.getText().toString();
						String ph1 = cmnp+"-"+cmbs;
						if(!ph.equals(ph1)){
						  Alert1("Your TAC number will be sent via SMS to your registered mobile phone number " + cprefix.getText().toString() + "-" + cnumber.getText().toString());
						}
					}else{
						new ConnectionDetector(ProfileAfter.this).Alert("No connection available.");
					}
					
				}
			});
			cnational.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus)
						countryset();
				}
			});

			ecstate.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus){
						if(ccountry.equals("MY")){
						    stateset();
						}				
					}
				}
			});
			ecstate.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(ccountry.equals("MY")){
					    stateset();
					}				
				}
			});
			cprefix.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus)
						prefixset();
				}
			});
			add1.setText(caddress1);
			if(!add1.getText().toString().equals("Address 1")){
				add1.setTextColor(Color.BLACK);
			}
			add2.setText(caddress2);
			if(!add2.getText().toString().equals("Address 2")){
				add2.setTextColor(Color.BLACK);
			}
			add3.setText(caddress3);
			if(!add3.getText().toString().equals("Address 3")){
				add3.setTextColor(Color.BLACK);
			}
			ecpcode.setText(cpcode);
			if(!ecpcode.getText().toString().equals("Post Code")){
				ecpcode.setTextColor(Color.BLACK);
			}
			for (int i = 0; i < selectionListCode.length; i++) {
				if (selectionListCode[i].equals(ccountry)){
					cnational.setText(selectionList[i]);
				}
			}
			eccity.setText(ccity);
			if(!eccity.getText().toString().equals("City")){
				eccity.setTextColor(Color.BLACK);
			}
			try{
			  ecstate.setText(cstate);
			}catch(Exception e){
				cstate = "";
				ecstate.setText(cstate);
			}
			if(!ecstate.getText().toString().equals("State")){
				ecstate.setTextColor(Color.BLACK);
			}
			cemail.setText(email);
			if(!cemail.getText().toString().equals("Email")){
				cemail.setTextColor(Color.BLACK);
			}
			cnumber.setText(cmbs);
			if(!cnumber.getText().toString().equals("Number")){
				cnumber.setTextColor(Color.BLACK);
			}
			cprefix.setText(cmnp);
			if(cac == null){
				cac = "";
			}
			ccac.setText(cac);
			if(!ccac.getText().toString().equals("Tac code")){
				ccac.setTextColor(Color.BLACK);
			}
			add1.setText(caddress1);
			if(!caddress1.equals("") && caddress1 != null){
				add1.setTextColor(Color.BLACK);
			}
			add2.setText(caddress2);
			if(!caddress2.equals("") && caddress2 != null){
				add2.setTextColor(Color.BLACK);
			}
			add3.setText(caddress3);
			if(!caddress3.equals("") && caddress3 != null){
				add3.setTextColor(Color.BLACK);
			}
			ecpcode.setText(cpcode);
			if(!cpcode.equals("") && cpcode != null){
				ecpcode.setTextColor(Color.BLACK);
			}
			for (int i = 0; i < selectionListCode.length; i++) {
				if (selectionListCode[i].equals(ccountry))
					cnational.setText(selectionList[i]);
			}
			eccity.setText(ccity);
			if(!ccity.equals("") && ccity != null){
				eccity.setTextColor(Color.BLACK);
			}
			ecstate.setText(cstate);
			if(!cstate.equals("") && cstate != null){
				ecstate.setTextColor(Color.BLACK);
			}
			cemail.setText(email);
			if(!email.equals("") && email != null){
				cemail.setTextColor(Color.BLACK);
			}
			cnumber.setText(cmbs);
			if(!cmbs.equals("") && cmbs != null){
				cnumber.setTextColor(Color.BLACK);
			}
			cprefix.setText(cmnp);
			if(cac == null){
				cac = "";
			}
			ccac.setText(cac);
			if(!cac.equals("") && cac != null){
				ccac.setTextColor(Color.BLACK);
			}
			cDialog.show();
			btChange.setBackgroundResource(R.drawable.cancel1);
			btUpdate.setBackgroundResource(R.drawable.cancel1);
			*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initiatePopupWindowchange() {
		try {
			cDialog = new Dialog(this);
			cDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			//cDialog.setContentView(R.layout.changepassword);

			RelativeLayout layout = (RelativeLayout) LayoutInflater.from(this).inflate(
					R.layout.changepassword, null);
			cDialog.setContentView(layout);
			
			//Drawable background = layout.getBackground();
			//background.setAlpha(180);

			//pwchange = new PopupWindow(layout, LayoutParams.MATCH_PARENT,
					//LayoutParams.MATCH_PARENT, true);
			//pwchange.showAtLocation(layout, Gravity.CENTER, 0, 0);

			btcancel1 = (Button) layout.findViewById(R.id.btcancel);
			btsubmit1 = (Button) layout.findViewById(R.id.btsubmit);
			btcancel1.setOnClickListener(cancel_button_click_listener1);
			btsubmit1.setOnClickListener(submit_button_click_listener1);

			oldpass = (EditText) layout.findViewById(R.id.oldpass);
			//oldpass.setSingleLine();
			oldpass.setTextColor(Color.GRAY);
			oldpass.addTextChangedListener(optextWatcher);
			newpass = (EditText) layout.findViewById(R.id.newpass);
			//newpass.setSingleLine();
			newpass.setTextColor(Color.GRAY);
			newpass.addTextChangedListener(nptextWatcher);
			newpassconfirm = (EditText) layout.findViewById(R.id.newpassconf);
			//newpassconfirm.setSingleLine();
			newpassconfirm.setTextColor(Color.GRAY);
			newpassconfirm.addTextChangedListener(npctextWatcher);
			// WebServiceBase a = new WebServiceBase();
			// a.parse("test");
			cDialog.show();
			btChange.setBackgroundResource(R.drawable.cancel1);
			btUpdate.setBackgroundResource(R.drawable.cancel1);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String ss1 = "";
	public String ss2 = "";
	public String ss3 = "";
	TextWatcher optextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		oldpass.setText("Old Password");
        		oldpass.setTextColor(Color.GRAY);
        		oldpass.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	ss1 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(ss1.length() > tmp.length() && ss1.equals("Old Password")){
        		oldpass.setText("");
        	}
            if(ss1.equals("Old Password") && s.length() == (ss1.length() + 1)){
            	for(int i = 0; i < ss1.length(); i++){
	            	int a = tmp.indexOf(ss1.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	oldpass.setText(tmp);         	
        		oldpass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	oldpass.setSelection(1);
            	oldpass.setTextColor(Color.BLACK);   
            }
        }  
    };
	TextWatcher nptextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		newpass.setText("New Password");
        		newpass.setTextColor(Color.GRAY);
        		newpass.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	ss2 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(ss2.length() > tmp.length() && ss2.equals("New Password")){
        		newpass.setText("");
        	}
            if(ss2.equals("New Password") && s.length() == (ss2.length() + 1)){
            	for(int i = 0; i < ss2.length(); i++){
	            	int a = tmp.indexOf(ss2.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	newpass.setText(tmp);           	
            	newpass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	newpass.setSelection(1);
            	newpass.setTextColor(Color.BLACK); 
            }
        }  
    };
	TextWatcher npctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		newpassconfirm.setText("Confirm New Password");
        		newpassconfirm.setTextColor(Color.GRAY);
        		newpassconfirm.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	ss3 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(ss3.length() > tmp.length() && ss3.equals("Confirm New Password")){
        		newpassconfirm.setText("");
        	}
            if(ss3.equals("Confirm New Password") && s.length() == (ss3.length() + 1)){
            	for(int i = 0; i < ss3.length(); i++){
	            	int a = tmp.indexOf(ss3.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	newpassconfirm.setText(tmp);     	
            	newpassconfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	newpassconfirm.setSelection(1);
            	newpassconfirm.setTextColor(Color.BLACK);       
            }
        }  
    };
	private void countryset() {
		AlertDialog.Builder countryBuilder = new AlertDialog.Builder(
				ProfileAfter.this);
		countryBuilder.setTitle("Pick a country:");
		countryBuilder.setItems(selectionList,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						cnational.setText(selectionList[item]);
						ccountry = selectionListCode[item];
						dialog.dismiss();
					}
				});
		AlertDialog countryAlert = countryBuilder.create();
		countryAlert.show();
	};
	private void stateset() {
		AlertDialog.Builder countryBuilder = new AlertDialog.Builder(
				ProfileAfter.this);
		countryBuilder.setTitle("Pick a state:");
		countryBuilder.setItems(selectionList2,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						ecstate.setText(selectionList2[item]);
						ecstate.setTextColor(Color.BLACK);
						csstate = selectionListCode2[item];
						dialog.dismiss();
					}
				});
		AlertDialog countryAlert = countryBuilder.create();
		countryAlert.show();
	};

	private void prefixset() {
		AlertDialog.Builder prefixBuilder = new AlertDialog.Builder(
				ProfileAfter.this);
		prefixBuilder.setTitle("Pick a prefix:");
		prefixBuilder.setItems(selectionList1,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						// TODO Auto-generated method stub
						cprefix.setText(selectionList1[item]);
						cmnp = selectionList1[item];
						dialog.dismiss();
					}
				});
		AlertDialog prefixAlert = prefixBuilder.create();
		prefixAlert.show();
	}

	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			if (cDialog.isShowing())
				cDialog.dismiss();
				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
			
		}
	};
	private OnClickListener cancel_button_click_listener1 = new OnClickListener() {
		public void onClick(View v) {
			if (cDialog.isShowing())
				cDialog.dismiss();
				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
			
		}
	};
	
	
	private OnClickListener submit_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			boolean connection = new ConnectionDetector(ProfileAfter.this)
			.isConnectStatus();
			if (connection) {
				if(add1.getText().toString().equals("Address 1")){
					a1 = "";
				}else{
					a1 = add1.getText().toString();
				}
				if(add2.getText().toString().equals("Address 2")){
					a2 = "";
				}else{
					a2 = add2.getText().toString();
				}
				if(add3.getText().toString().equals("Address 3")){
					a3 = "";
				}else{
					a3 = add3.getText().toString();
				}
				if(ecpcode.getText().toString().equals("Post Code")){
					ac = "";
				}else{
					ac = ecpcode.getText().toString();
				}
				if(eccity.getText().toString().equals("City")){
					acity = "";
				}else{
					acity = eccity.getText().toString();
				}
				if(ecstate.getText().toString().equals("State")){
					astate = "";
				}else{
					astate = ecstate.getText().toString();
				}
				if(ccac.getText().toString().equals("Tac code")){
					acac = "";
				}else{
					acac = ccac.getText().toString();
				}
				if(cnumber.getText().toString().equals("Number")){
					anum = "";
				}else{
					anum = cnumber.getText().toString();
				}
				if(cemail.getText().toString().equals("Email")){
					aemail = "";
				}else{
					aemail = cemail.getText().toString();
				}
				if(a1 == ""){
					new ConnectionDetector(ProfileAfter.this).Alert("Address 1 is empty.");
				}else if(ac == ""){
					new ConnectionDetector(ProfileAfter.this).Alert("Post Code is empty.");
				}else if(acity == ""){
					new ConnectionDetector(ProfileAfter.this).Alert("City is empty.");
				}else if(astate == ""){
					new ConnectionDetector(ProfileAfter.this).Alert("State is empty.");
				}else{
					if(a2.indexOf("Address")!=-1){
						a2 = "";
					}
					if(a3.indexOf("Address")!=-1){
						a3 = "";
					}
					if(a2 == null)a2 = "";
					if(a3 == null)a3 = "";
					new sendingData().execute();
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("name", name);
					editor.putString("gender", gender);
					editor.putString("nat", nat);
					editor.putString("newIc", newIc);
					editor.putString("oldIc", oldIc);
					editor.putString("address1", a1);
					editor.putString("address2", a2);
					editor.putString("address3", a3);
					editor.putString("pcode", ac);
					editor.putString("city", acity);
					editor.putString("state", astate);
					editor.putString("cmnp", cmnp);
					editor.putString("cmbs", anum);
					editor.putString("ccountry", ccountry);
					editor.putString("email", aemail);
					editor.putString("cac", acac);
					editor.commit();
				}
				
				tvName.setText(name);
				tvNational.setText(nat);
				tvIc.setText(newIc);
				tvOld.setText(oldIc);
				tvGender.setText(gender);
				tvEmail.setText(email);
				tvMobile.setText("+6 "+ cmnp + "-"+cmbs);
				tvAddress.setText(add1.getText().toString() + " " + add2.getText().toString() + " " + add3.getText().toString()
						+ " " + ecpcode.getText().toString() + " " + eccity.getText().toString() + " " + ecstate.getText().toString());
				
			}else{
				new ConnectionDetector(ProfileAfter.this).Alert("No connection available.");
			}
		}
	};
	private OnClickListener submit_button_click_listener1 = new OnClickListener() {
		public void onClick(View v) {
			if(newpass.getText().toString().equals("New Password")){
				np = "";
			}else{
				np = newpass.getText().toString();
			}
			if(newpassconfirm.getText().toString().equals("Confirm New Password")){
				npc = "";
			}else{
				npc = newpassconfirm.getText().toString();
			}
			if(oldpass.getText().toString().equals("Old Password")){
				op = "";
			}else{
				op = oldpass.getText().toString();
			}
			if(op == ""){
				new ConnectionDetector(ProfileAfter.this).Alert("Old Password is empty.");
			}else if(np == ""){
				new ConnectionDetector(ProfileAfter.this).Alert("New Password is empty.");
			}else if(npc == ""){
				new ConnectionDetector(ProfileAfter.this).Alert("Confirm Password is empty.");
			}else{
				Pattern p = Pattern.compile("[a-zA-z0-9]*"); 
				if (!p.matcher(np).matches()){
		    		Alert("Password should be alpha numeric.");
		    		return;
	    		}else{
	    			boolean f = false, g = false, com = false;
	    			for(int i = 0; i < np.length(); i++){
	    				String a = np.substring(i, i+1);
	    				try{
	    					int su = Integer.parseInt(a);
	    					f = true;
	    					if(g == false){
	    						com = true; break;
	    					}
	    				}catch(Exception e){
	    					g = true;
	    					if(f == true){
	    						com = true; break;
	    					}
	    				}
	    			}
	    			if(f == false || g == false){
	    				com = true;
	    			}

	    			if(com == true){
	    				boolean com2 = false;
	    				f = false;g = false;
	    				for(int i = 0; i < np.length(); i++){
	    					String a = np.substring(i, i+1);
	    					try{
	    						int su = Integer.parseInt(a);
	    						f = true;
	    						if(g == true){
	    							com2 = true; break;
	    						}
	    					}catch(Exception e){
	    						g = true;
	    						if(f == false){
	    							com2 = true; break;
	    						}
	    					}
	    				}
	    				if(f == false || g == false){
	    					com2 = true;
	    				}
	    				if(com2 == false){
	    					com = false;
	    				}
	    			}
	    			if(com == true){
	    				Alert("Password should be alpha numeric. Example: abc1234");
	    				return;
	    			}else{
						boolean connection = new ConnectionDetector(ProfileAfter.this)
						.isConnectStatus();
						if (connection) {
							new changeData().execute();
						}else{
							new ConnectionDetector(ProfileAfter.this).Alert("No connection available.");
						}
	    			}
		    	}
			}
		}
	};
	
	
	

	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ProfileAfter.this);
			pDialog.setMessage("Submitting...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {

				MemberWebService quest = new MemberWebService();

				caddress1 = a1;
				caddress2 = a2;
				caddress3 = a3;
				ccity = acity;
				email = aemail;
				cmbs = anum;
				cpcode = ac;
				cstate = astate;
				cac = acac;
				List<HashMap<String, String>> temp = quest.PmUpdate(CID, Key,cac,
						caddress1, caddress2, caddress3, ccity, ccountry,
						email, cmnp, cmbs, cpcode, cstate);
				if (temp.get(0).get("status").equals("0")){
					if(!temp.get(0).get("message").equals("")){
						if(temp.get(0).get("errorMessage").indexOf("TAC")!=-1){
							return temp.get(0).get("errorMessage");
						}
						return temp.get(0).get("message");						
					}else{
						return temp.get(0).get("errorMessage");
					}
					
				}else
					return "1";
			} catch (Exception e) {
				this.exception = e;
				return "CID Update failed.";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				Alert("successful");
				tvEmail.setText(email);
				tvMobile.setText("+6 "+ cmnp + "-"+cmbs);
				//pw.dismiss();
				if(caddress2.indexOf("Address")!=-1 || caddress2 == null)caddress2 = "";
				if(caddress3.indexOf("Address")!=-1 || caddress3 == null)caddress3 = "";
				tvAddress.setText(caddress1 + " " + caddress2 + " " + caddress3
						+ " " + cpcode + " " + ccity + " " + cstate);
				//updatescroll.setVisibility(View.GONE);
				//scrollview.setVisibility(View.VISIBLE);
				if(cDialog.isShowing())
					cDialog.dismiss();
				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
			} else {
				Alert(feed);
			}
		}
	}
	
	class reqSMS extends AsyncTask<String, String, String> {

		private Exception exception;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
			pDialog = new ProgressDialog(ProfileAfter.this);
			pDialog.setMessage("Reading Tac code...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		protected String doInBackground(String... arg0) {
			try {
				userInfo.put("cac", "");
				userInfo.put("serial", "");
				MemberWebService cacWs = new MemberWebService();
				
				List<HashMap<String, String>> temp1 = cacWs
						.PmCac(	newIc,
								oldIc,
								ccountry,
								//userInfo.get("country"),
								cmnp,
								cnumber.getText().toString());
				
				if(temp1.get(0).get("status").equals("0")){
					//ReturnMessage.showAlertDialog(this, "Email incorrect.");
					//etEmail.requestFocus();
					userInfo.put("cac", "");
					userInfo.put("serial", "");
					return "";
				}else{
					ContentResolver contentResolver = getContentResolver();
					Cursor cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
	
					int indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
					int indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
					int indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
					
					if ( indexBody < 0 || !cursor.moveToFirst() ) return "success";
					
					boolean flg = true;
					Calendar c = Calendar.getInstance();
					long lasttime = c.get(Calendar.HOUR) * 3600 + c.get(Calendar.MINUTE)*60 + c.get(Calendar.SECOND);
					String serial1 = temp1.get(0).get("cacSerialNo");
					cursor.moveToNext();
					int i = 0;
					while(flg){
						i++;
						String str = cursor.getString(indexBody);
						String cac1 = "";
						String serial2 = "";
						int index = 0;
						if((index = str.indexOf("TAC Code:")) != -1){
							cac1 = str.substring(index+10, index+16);
							int index1 = str.indexOf("Serial No:");
							serial2 = str.substring(index1+11, index).trim();
						}						
						if((index = serial2.indexOf(serial1)) != -1){
							flg = false;
							userInfo.put("cac", cac1);
							userInfo.put("serial", serial2);					
						}else{
						}
						if(i > 2){
							cursor.close();
							contentResolver = getContentResolver();
							cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
							
							indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
							indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
							indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
							cursor.moveToNext();
							i = 0;
						}else{
							cursor.moveToNext();
						}
						c = Calendar.getInstance();
						long curtime = c.get(Calendar.HOUR) * 3600 + c.get(Calendar.MINUTE) * 60 + c.get(Calendar.SECOND);
						if(Math.abs(lasttime - curtime) > 30){
							flg = false;
							userInfo.put("cac", "");
							userInfo.put("serial", "");
						}
						try{
							Thread.sleep(200);
						}catch(Exception e){}
					}
					//do
					//{
						//String str = "Sender: " + cursor.getString( indexAddr ) + "\n" + cursor.getString( indexBody );
						//smsList.add( str );
					//}
					//while( cursor.moveToNext() );
				}
				cacflg = true;
				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (pDialog.isShowing())
				pDialog.dismiss();

			if (feed.equals("success")) {
				Log.d("success", "success");
			}
			ccac.setText(userInfo.get("cac"));
		
		}
		

	}

	class changeData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ProfileAfter.this);
			pDialog.setMessage("Submitting...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {

				MemberWebService quest = new MemberWebService();
				String oldp = op;
				String newp = np;
				String newpc = npc;
				if(oldp.equals(oldPass))
				{
					List<HashMap<String, String>> temp = quest.PmChgPswd(CID, Key,
							newp, newpc);
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("errorMessage");
					else{
						Key = temp.get(0).get("key");
						SharedPreferences.Editor editor = settings.edit();						
						editor.putString("Key", Key);
						editor.commit();
						return "1";
					}
				}
				else
					return "Invalid old password";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				Alert("successful");
				cDialog.dismiss();
				btChange.setBackgroundResource(R.drawable.cancel1);
				btUpdate.setBackgroundResource(R.drawable.cancel1);
			} else {
				Alert(feed);
			}
		}
	}

	
}