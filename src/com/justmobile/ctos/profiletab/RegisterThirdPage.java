package com.justmobile.ctos.profiletab;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import android.R.array;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.webservice.MemberWebService;

public class RegisterThirdPage extends Activity implements
		OnClickListener, OnEditorActionListener {

	RadioGroup rgNationality;
	EditText etName, etIC, etOldIC, etAddress1, etAddress2, etAddress3, etCity,
			etPostcode, etState;
	Button btGender, btCountry, btState;
	TextView tvCitizenship;

	String selectionList[], selectionListCode[];
	String sGender = "",genderType="";
	String sCountry, sState;
	String sNationality, sStatename;
	String sCitizenship;
	Iterator itr;
	String name;
	boolean icflg = false;
	boolean checkflg = false;
	String selectionList1[], selectionListCode1[];
	String selectionList2[], selectionListCode2[];
	RadioButton rblocal;
	// used to store user data
	HashMap<String, String> userInfo = new HashMap<String, String>();
	Typeface font1,font2;
	Button cancel, agree;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_user1);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		TextView etpnum = (TextView)findViewById(R.id.etpNum);
		etpnum.setVisibility(View.VISIBLE);
		etpnum.setText("Page(1/5)");
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		new RetreiveFeedTask().execute();
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);

		etName = (EditText) findViewById(R.id.etName);
		etIC = (EditText) findViewById(R.id.etIC);
		etOldIC = (EditText) findViewById(R.id.etOldIC);
		etOldIC.setSingleLine();

		etAddress1 = (EditText) findViewById(R.id.etAddress1);
		etAddress2 = (EditText) findViewById(R.id.etAddress2);
		etAddress3 = (EditText) findViewById(R.id.etAddress3);
		etCity = (EditText) findViewById(R.id.etCity);
		etCity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if((actionId == EditorInfo.IME_ACTION_DONE) ||
			            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			        // TODO Auto-generated method stub
					etPostcode.requestFocus();
			    }
				return false;
			}
		});
		etPostcode = (EditText) findViewById(R.id.etPostcode);
		etPostcode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if((actionId == EditorInfo.IME_ACTION_DONE) ||
			            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			        // TODO Auto-generated method stub
					btState.requestFocus();
			    }
				return false;
			}
		});
		etState = (EditText) findViewById(R.id.etState);

		btCountry = (Button) findViewById(R.id.btCountry);
		btState = (Button) findViewById(R.id.btState);
		sCountry = "MY";
		sState = "";
		sCitizenship = "WARGANEGARA";
		sNationality = "MALAYSIA";
		sStatename = "";
		if(userInfo.get("country") != null){
			btCountry.setText(userInfo.get("nationality"));
			sCountry = userInfo.get("country");
			sNationality = userInfo.get("nationality");
			btState.setText(sStatename);
			btState.setOnClickListener(this);
			//btState.setText(userInfo.get("sStateName"));
			//sState = userInfo.get("sState");
			//sStatename = userInfo.get("sStateName");
		}else{
			btCountry.setText(sNationality);
			btCountry.setOnClickListener(null);
			sCountry = "MY";
			sStatename = "";
			btState.setText(sStatename);
			btState.setOnClickListener(this);
			sState = "";
			etIC.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_CLASS_NUMBER);
		}
		btGender = (Button) findViewById(R.id.btGender);
		btGender.setOnClickListener(this);


		tvCitizenship = (TextView) findViewById(R.id.tvCitizenship);

		rgNationality = (RadioGroup) findViewById(R.id.rgNationality);
	
		etName.setText(userInfo.get("name"));
		etIC.setText(userInfo.get("nic"));
		etOldIC.setText(userInfo.get("ic"));
		etAddress1.setText(userInfo.get("address1"));
		etAddress2.setText(userInfo.get("address2"));
		etAddress3.setText(userInfo.get("address3"));
		etCity.setText(userInfo.get("city"));
		etPostcode.setText(userInfo.get("postcode"));
		etPostcode.setOnEditorActionListener(this);
		etState.setOnEditorActionListener(this);
		
		//etState.setText(userInfo.get("state"));
		btGender.setText(userInfo.get("genderType"));
		sGender=userInfo.get("gender");
		genderType=userInfo.get("genderType");
		/*back process*/
		rblocal = (RadioButton)findViewById(R.id.rbLocal);
		RadioButton rbforeigner = (RadioButton)findViewById(R.id.rbForeigner);
		checkflg = false;
		if(userInfo.get("nationality") == null){
			etState.setVisibility(View.GONE);
			btState.setVisibility(View.VISIBLE);
			rblocal.setChecked(true);
		}else if(userInfo.get("nationality").equals("MALAYSIA")){
			etState.setVisibility(View.GONE);
			btState.setVisibility(View.VISIBLE);
			btState.setText(userInfo.get("sStateName"));
			sState = userInfo.get("sState");
			sStatename = userInfo.get("sStateName");
			btState.setOnClickListener(this);
			rblocal.setChecked(true);
			etIC.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_CLASS_NUMBER);
		}else{
			etState.setVisibility(View.VISIBLE);
			btState.setVisibility(View.GONE);
			etOldIC.setVisibility(View.GONE);
			etState.setText(userInfo.get("state"));
			rbforeigner.setChecked(true);			
			icflg = true;
			etIC.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		}
		etPostcode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if((actionId == EditorInfo.IME_ACTION_DONE) ||
			            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
			        // TODO Auto-generated method stub
					if(rblocal.isChecked()){
						btState.requestFocus();
					}else{
						etState.requestFocus();
					}
			    }
				return false;
			}
		});
		rgNationality
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						// TODO Auto-generated method stub
						if(checkedId == R.id.rbLocal) {
							etOldIC.setVisibility(View.VISIBLE);
							etIC.setHint("IC No.  *");
							etIC.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_CLASS_NUMBER);
							
							if(icflg == true)
								etIC.setText("");
							icflg = false;
							etState.setVisibility(View.GONE);
							btState.setVisibility(View.VISIBLE);
							btCountry.setText("MALAYSIA");
							btCountry.setOnClickListener(null);
							btState.setOnClickListener(RegisterThirdPage.this);
							sCountry = "MY";
							sCitizenship = "WARGANEGARA";
							sNationality = "MALAYSIA";
							sStatename = "";
							btState.setText(sStatename);
							sState = "";
							// Log.d("sNationalityWebsirvice",sNationality);
						}
						else if(checkedId == R.id.rbForeigner){
							etState.setVisibility(View.VISIBLE);
							btState.setVisibility(View.GONE);
							etOldIC.setVisibility(View.GONE);
							etIC.setHint("Passport No.  *");
							etIC.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
							if(icflg == false)
								etIC.setText("");
							icflg = true;
							//btState.setOnClickListener(RegisterThirdPage.this);
							btCountry.setText("");
							btCountry.setHint("Please choose a country  *");
							btCountry
									.setOnClickListener(RegisterThirdPage.this);
							sCountry = "";
							sNationality = "";
							sCitizenship = "FOREIGNER";
						}
					}
				});
		etIC.addTextChangedListener(new TextWatcher() {
            int len=0;
            String str1="";
            boolean flg = false;
            @Override
            public void afterTextChanged(Editable s) {
            	if(icflg == false){
	                String str = etIC.getText().toString(); 
	                 if(str.length() == 8){//len check for backspace 
	                	// etIC.append("-");
	                	 etIC.setSelection(8);
	                }
	                 if(str.length()== 11){//len check for backspace 
	                	 //etIC.append("-");
	                	 etIC.setSelection(11);
	                }
            	}
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

             str1 = arg0.toString(); 
              len = arg0.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String s1 = "";
                if(icflg == false){
                	if(s.length() == 0 && flg == true){
                		flg = false;
                		etIC.setSelection(0);
                	}
	                if(s.length() > 0){
	                	if(flg == true){
		 					flg = false;
		 					etIC.setSelection(s.length());
		         		}else{
			            	String s2 = s.toString().substring(s.length()-1, s.length());
			            	Pattern p;
			            	//if(checkflg){
			            		//p = Pattern.compile("[a-zA-Z0-9]*");
			            		//checkflg = false;
			            	//}else{
			            		p = Pattern.compile("[0-9]*");
			            	//}
			                 
			                if(!s2.equals("-")){
				 				if (!p.matcher(s2).matches()){	 
				 					flg = true;					
				 					etIC.setText(s.toString().substring(0 , s.length() - 1));
				         		}
			                }
		         		}
	                }
	            	if(s.length() == 7 && len == 6){
	            		s1 = s.toString().substring(s.length()-1, s.length());
	            		etIC.setText(str1 + "-" + s1);            		
	            	}
	            	if(s.length() == 10 && len == 9){
	            		s1 = s.toString().substring(s.length()-1, s.length());            		
	            		etIC.setText(str1 + "-" + s1);
	            	}
                }else{
                	if(s.length() == 0 && flg == true){
                		flg = false;
                	}
	                if(s.length() > 0){
	                	if(flg == true){
		 					flg = false;
		 					etIC.setSelection(s.length());
		         		}else{
			            	String s2 = s.toString().substring(s.length()-1, s.length());
			                Pattern p = Pattern.compile("[a-zA-Z0-9]*");			                
			 				if (!p.matcher(s2).matches()){	 
			 					flg = true;					
			 					etIC.setText(s.toString().substring(0 , s.length() - 1));
			         		}
		         		}
	                }
                }
            }

			


        }); 
	}
	
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	    if((actionId == EditorInfo.IME_ACTION_DONE) ||
	            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
	        // TODO Auto-generated method stub
	    	//if(etState.length() > 0){	    	
	    	if(sCountry.equals("MY") && v.getId() == R.id.etPostcode){
	    		onAgree();
	    	}else if(!sCountry.equals("MY") && v.getId() == R.id.etState){
	    		onAgree();
	    	}
	    }
	    return false;
	}
	
	@Override
	public void onClick(View v){
		if(v.getId() == R.id.btCancel){
			Intent intent1 = new Intent(this, RegisterIntroPage.class);
			intent1.putExtra("userInfo", userInfo);
			this.startActivity(intent1);
			this.finish();
		}else if(v.getId() == R.id.btAgree){
			onAgree();
		}else if (v.getId() == R.id.btGender) {

			AlertDialog.Builder genderBuilder = new AlertDialog.Builder(this);
			genderBuilder.setTitle("Pick your gender:");
			genderBuilder.setItems(selectionList1,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btGender.setText(selectionList1[item]);
							genderType=selectionList1[item];
							sGender = selectionListCode1[item];
							etAddress1.requestFocus();
							Log.d("gender", sGender);
							dialog.dismiss();
						}
					});
			AlertDialog genderAlert = genderBuilder.create();
			genderAlert.show();
		}else if(v.getId() == R.id.btCountry){

			AlertDialog.Builder countryBuilder = new AlertDialog.Builder(this);
			countryBuilder.setTitle("Pick a country:");
			countryBuilder.setItems(selectionList,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btCountry.setText(selectionList[item]);
							sCountry = selectionListCode[item];
							sNationality = selectionList[item];
							Log.d("country selected", sCountry
									+ selectionListCode[item]);
							
							dialog.dismiss();
						}
					});
			AlertDialog countryAlert = countryBuilder.create();
			countryAlert.show();
		}else if(v.getId() == R.id.btState){

			AlertDialog.Builder countryBuilder = new AlertDialog.Builder(this);
			countryBuilder.setTitle("Pick a State:");
			countryBuilder.setItems(selectionList2,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							// TODO Auto-generated method stub
							btState.setText(selectionList2[item]);
							sState = selectionListCode2[item];
							sStatename = selectionList2[item];
							
							dialog.dismiss();
						}
					});
			AlertDialog countryAlert = countryBuilder.create();
			countryAlert.show();
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterIntroPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}

	class RetreiveFeedTask extends AsyncTask<String, String, String> {

		private Exception exception;

		protected String doInBackground(String... urls) {
			try {
				MemberWebService nation = new MemberWebService();
				List<HashMap<String, String>> temp = nation.getLsCountry();
				int sizeTemp = temp.size();
				selectionList = new String[sizeTemp];
				selectionListCode = new String[sizeTemp];
				for (int i = 0; i < sizeTemp; i++) {
					selectionList[i] = temp.get(i).get("name");
					selectionListCode[i] = temp.get(i).get("code");
				}
				MemberWebService nation1 = new MemberWebService();
				List<HashMap<String, String>> temp1 = nation1.getGender();
				int sizeTemp1 = temp1.size();
				selectionList1 = new String[sizeTemp1];
				selectionListCode1 = new String[sizeTemp1];
				for (int k = 0; k < sizeTemp1; k++) {
					selectionList1[k] = temp1.get(k).get("name");
					selectionListCode1[k] = temp1.get(k).get("code");
				}
				MemberWebService nation2 = new MemberWebService();
				List<HashMap<String, String>> temp2 = nation2.getLsState();
				int sizeTemp2 = temp2.size();
				selectionList2 = new String[sizeTemp2];
				selectionListCode2 = new String[sizeTemp2];
				for (int i = 0; i < sizeTemp2; i++) {
					selectionList2[i] = temp2.get(i).get("name");
					selectionListCode2[i] = temp2.get(i).get("code");
				}
				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (feed.equals("success")) {
				Log.d("success", "success");
			}
		}
	}
	public void onAgree(){

		try{
		Pattern p2 = Pattern.compile("[ ]*"); 
		Pattern p3 = Pattern.compile("[a-zA-Z]*"); 
		if (etName.length() == 0) {
			ReturnMessage.showAlertDialog(this,
					"Full Name is empty");
			etName.setHintTextColor(Color.RED);
			etName.requestFocus();
			
		} else if (rgNationality.getCheckedRadioButtonId() == R.id.rbLocal
				&& etIC.length()==0) {

			ReturnMessage.showAlertDialog(this, "IC No. empty");
			etIC.setHintTextColor(Color.RED);
			etIC.requestFocus();
		
		}
		else if (rgNationality.getCheckedRadioButtonId() == R.id.rbForeigner
				&& p3.matcher(etIC.getText().toString()).matches()) {

			ReturnMessage.showAlertDialog(this, "Passport No. Incorrect Format");
			etIC.setHintTextColor(Color.RED);
			etIC.requestFocus();

		}
		else if (rgNationality.getCheckedRadioButtonId() == R.id.rbForeigner
				&& etIC.length() < 6) {

			ReturnMessage.showAlertDialog(this, "Passport No. empty");
			etIC.setHintTextColor(Color.RED);
			etIC.requestFocus();

		}else if (rgNationality.getCheckedRadioButtonId() == R.id.rbForeigner
				&& btCountry.isSelected()) {

			ReturnMessage.showAlertDialog(this, "Please choose a country");
			btCountry.setHintTextColor(Color.RED);
			

		} else if (sGender.equals("")) {
			ReturnMessage.showAlertDialog(this, "Please select gender");
			btGender.setHintTextColor(Color.RED);
		} else if (etAddress1.length() == 0 || p2.matcher(etAddress1.getText().toString()).matches()) {
			ReturnMessage.showAlertDialog(this, "Address empty");
			etAddress1.setHintTextColor(Color.RED);
			etAddress1.requestFocus();
		} else if (etCity.length() == 0 || p2.matcher(etCity.getText().toString()).matches()) {
			ReturnMessage.showAlertDialog(this, "City empty");
			etCity.setHintTextColor(Color.RED);
			etCity.requestFocus();
		} else if (etPostcode.length() == 0) {
			ReturnMessage.showAlertDialog(this, "Postcode empty");
			etPostcode.setHintTextColor(Color.RED);
			etPostcode.requestFocus();
		} else if ((etState.length() == 0 && !sCountry.equals("MY"))) {
			ReturnMessage.showAlertDialog(this, "State empty");
			etState.setHintTextColor(Color.RED);
			etState.requestFocus();
		}else if(p2.matcher(etState.getText().toString()).matches() && !sCountry.equals("MY")){
			ReturnMessage.showAlertDialog(this, "State empty");
			etState.setHintTextColor(Color.RED);
			etState.requestFocus();
		}else if (sCountry.equals("")) {			
			ReturnMessage.showAlertDialog(this, "Country empty");
			btCountry.setHintTextColor(Color.RED);
		} else if (sCountry.equals("MY") && sState.equals("")) {
			ReturnMessage.showAlertDialog(this, "State empty");
			btState.setHintTextColor(Color.RED);
		} else {
			if ((rgNationality.getCheckedRadioButtonId() == R.id.rbLocal
					&& etIC.length()>1) || rgNationality.getCheckedRadioButtonId() == R.id.rbForeigner) {

				if((etIC.length()<14 || etIC.length()>14) && rgNationality.getCheckedRadioButtonId() == R.id.rbLocal)
				{
					ReturnMessage.showAlertDialog(this, "IC No. is incorrect");
					etIC.setHintTextColor(Color.RED);
					etIC.requestFocus();
				}else{
					String fs = "";
					String ss = "";
					if(rgNationality.getCheckedRadioButtonId() == R.id.rbLocal){
					  fs = etIC.getText().toString().substring(6,7);
					  ss = etIC.getText().toString().substring(9,10);
					}
					 if((!fs.equals("-") || !ss.equals("-")) && rgNationality.getCheckedRadioButtonId() == R.id.rbLocal)
					{
						ReturnMessage.showAlertDialog(this, "IC No. wrong format. xxxxxx-xx-xxxx");
							etIC.setHintTextColor(Color.RED);
							etIC.requestFocus();
					}else{
						boolean com1 = false;
						Pattern p = Pattern.compile("[ a-zA-z]*"); 
        				String tmp1 = etName.getText().toString();
        				if (!p.matcher(tmp1).matches()){
        					com1 = true;
        					ReturnMessage.showAlertDialog(this, "Full Name is incorrect.");
                		}else{
                			p = Pattern.compile("[ ]*");
                			if (!p.matcher(tmp1).matches()){
	        				
								userInfo.put("nationality", sNationality);
								userInfo.put("name", etName.getText().toString());
								userInfo.put("ic", etOldIC.getText().toString());
								userInfo.put("nic", etIC.getText().toString());
								userInfo.put("passport", etIC.getText().toString());
								userInfo.put("address1", etAddress1.getText().toString());
								userInfo.put("address2", etAddress2.getText().toString());
								userInfo.put("address3", etAddress3.getText().toString());
								userInfo.put("postcode", etPostcode.getText().toString());
								userInfo.put("city", etCity.getText().toString());
								if(sCountry.equals("MY")){
									userInfo.put("sState", sState);
									userInfo.put("sStateName", sStatename);	
									userInfo.put("state", sStatename);				
								}else{
									userInfo.put("state", etState.getText().toString());
									
								}
								userInfo.put("country", sCountry);
								userInfo.put("citizenship", sCitizenship);
								userInfo.put("gender", sGender);
								userInfo.put("genderType", genderType);
								//userInfo.remove("corresflg");
								Log.d("userInfo @ register 3rd page", userInfo.toString());
	
								boolean connection = new ConnectionDetector(this)
								.isConnectStatus();
								if (connection) {
									Intent intent = new Intent(this, RegisterFourthPage.class);
									intent.putExtra("userInfo", userInfo);
									this.startActivity(intent);
									finish();
								}else{
									new ConnectionDetector(this).Alert("No connection available.");
								}	
	                		}else{
	                			ReturnMessage.showAlertDialog(this, "Full Name is incorrect.");
	                		}
                		}
					}
				}
				
			}
		}
		}catch(Exception e){
			ReturnMessage.showAlertDialog(this, "Please fill in the details.");
		}
	}

}