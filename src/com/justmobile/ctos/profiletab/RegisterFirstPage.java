package com.justmobile.ctos.profiletab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.library.*;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.ReturnMessage;

public class RegisterFirstPage extends Activity implements OnClickListener {
	Button translateEn;
	Button translateMy;
	int tr = 0;
	WebView webview;
	static String bm = "", en = "";
	String initial = "";
	private ProgressDialog pDialog;
	Typeface font1,font2;
	float initscale = 0f;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_tc);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setText("I Agree");
		btlog.setOnClickListener(this);
		TextView title = (TextView) findViewById(R.id.registerTitle);
		//title.setText("Terms & Conditions : CTOS IDENTITY (CID) Registration");
		title.setVisibility(View.GONE);
		translateEn = (Button) findViewById(R.id.btEng);
		translateEn.setBackgroundResource(R.drawable.agree2);
		translateMy = (Button) findViewById(R.id.btMy);
		//title.setText("Terms & Condition : CTOS IDENTITY (CID) Registration");
		webview = (WebView) findViewById(R.id.registerWebview);
		initscale =  1.6685696f;//webview.getScale();
		//float scale = 100 * initscale;
		//webview.setInitialScale((int) scale);
		/*webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setSupportZoom(true);
		//webview.getSettings().setUseWideViewPort(true);
		webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		*/
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setBuiltInZoomControls(true);
		new sendingData()
				.execute(
						"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/da4b9237bacccdf19c0760cab7aec4a8359010b0",
						"1");

		translateEn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {				
					boolean connection = new ConnectionDetector(RegisterFirstPage.this)
					.isConnectStatus();
					if (connection) {
						
						//float scale = 1;
						//webview.setInitialScale((int) scale);
							new sendingData()
									.execute(
											"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/da4b9237bacccdf19c0760cab7aec4a8359010b0",
											"1");
						translateEn.setBackgroundResource(R.drawable.agree2);
						translateMy.setBackgroundResource(R.drawable.agree1);
					}else{
						new ConnectionDetector(RegisterFirstPage.this).Alert("No connection available.");
					}
			}
		});
		translateMy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				

					boolean connection = new ConnectionDetector(RegisterFirstPage.this)
					.isConnectStatus();
					if (connection) {
						//float scale = 1;//70 * initscale;
						//webview.setInitialScale((int) scale);
							new sendingData()
									.execute(
											"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/77de68daecd823babbb58edb1c8e14d7106e83bb",
											"0");// bm
						translateEn.setBackgroundResource(R.drawable.agree1);
						translateMy.setBackgroundResource(R.drawable.agree2);
					}else{
						new ConnectionDetector(RegisterFirstPage.this).Alert("No connection available.");
					}
			}
		});
	}

	public void onClick(View v){
		if(v.getId() == R.id.btLog){
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				Intent intent = new Intent(this, RegisterSecondPage.class);
				this.startActivity(intent);
				this.finish();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, CTOSMainActivity.class);
		this.startActivity(intent);
		this.finish();
	}
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

			Intent intent = new Intent(this, CTOSMainActivity.class);
			this.startActivity(intent);
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}*/
	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;
		String urlString = "";
		String po = "";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RegisterFirstPage.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				Post post = new Post();
				po = "";
				po = post.CallWebService(urls[0], "1", "");
				urlString = urls[0];
				if (urls[1].equals("1"))
					initial = "1";
				else
					initial = "0";

				return "1";
			} catch (Exception e) {
				this.exception = e;
				return "0";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if(feed.equals("1")){
				try{
					String finalString="";
					for(int i=0;i<po.length();i++)
					{
						if(po.charAt(i)!='\\')
								finalString+=po.charAt(i);
						
					}
					String sXML = "";
					sXML = finalString.replaceAll("rn", "");
					int index = 0;
					if((index = sXML.indexOf("content"))!=-1){
						sXML = sXML.substring(index+10, sXML.length()-2);
					}
					//Spanned span = Html.fromHtml(sXML);
					//sXML = span.toString();
					//sXML = TextUtils.htmlEncode(sXML);
					
					
					
					webview.loadData(sXML, "text/html", "utf-8");	
					
					
				}catch(Exception e){
					ReturnMessage.showAlertDialog(RegisterFirstPage.this,
							"Internet is invalid.");
				}
			}else{
				ReturnMessage.showAlertDialog(RegisterFirstPage.this,
						"Internet is invalid.");
			}
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}