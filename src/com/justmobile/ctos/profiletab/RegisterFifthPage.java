package com.justmobile.ctos.profiletab;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.profiletab.ProfileAfter.reqSMS;
import com.justmobile.ctos.sms.SmsReceiver;
import com.justmobile.ctos.webservice.MemberWebService;
import com.justmobile.ctos.webservice.WebServiceLink;

public class RegisterFifthPage extends Activity implements
		OnClickListener, OnEditorActionListener {
	Iterator itr;
	EditText etPassword, etConfirmPassword, etSecurityQs, etSecurityAns,
			etConfirmSecurityAns, tvCac;
	Button btSecurityQs, btCac;
	TextView tvChooseSQ, tvCreateSQ;
	String[] selectionList;
	String selectionListCode[];
	String sSecurityQs = "";
	String secQuest ="";
	HashMap<String, String> userInfo = new HashMap<String, String>();
	Boolean isChooseSQ = true, isCreateSQ = false;
    String name,code,question1,question2,question3,question4;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    Typeface font1, font2;
    Button cancel, agree;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiletab_register_user3);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Registration");
		tvPageTitle.setTypeface(font1);
		TextView etpnum = (TextView)findViewById(R.id.etpNum);
		etpnum.setVisibility(View.VISIBLE);
		etpnum.setText("Page(3/5)");
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		cancel = (Button) findViewById(R.id.btCancel);
		agree = (Button) findViewById(R.id.btAgree);
		cancel.setOnClickListener(this);
		agree.setOnClickListener(this);
		userInfo = (HashMap<String, String>) getIntent().getSerializableExtra(
				"userInfo");
		Log.d("userInfo @ intent 5th page", userInfo.toString());
		new RetreiveFeedTask().execute();
		settings = this.getSharedPreferences("REG", Context.MODE_PRIVATE);
		editor = settings.edit();
		
		tvCac = (EditText) findViewById(R.id.tvCac);
		tvCac.setText(userInfo.get("cac"));
		
		etPassword = (EditText) findViewById(R.id.etPassword);
		//etPassword.addTextChangedListener(ptextWatcher);
		etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
		//etConfirmPassword.addTextChangedListener(cptextWatcher);
		etSecurityQs = (EditText) findViewById(R.id.etSecurityQs);
		etSecurityAns = (EditText) findViewById(R.id.etSecurityAns);
		etConfirmSecurityAns = (EditText) findViewById(R.id.etConfirmSecurityAns);
		etConfirmSecurityAns.setOnEditorActionListener(this);
		btSecurityQs = (Button) findViewById(R.id.btSecurityQs);
		btSecurityQs.setSingleLine();
		btSecurityQs.setOnClickListener(this);

		tvCreateSQ = (TextView) findViewById(R.id.tvCreateSQ);
		tvChooseSQ = (TextView) findViewById(R.id.tvChooseSQ);
		if(userInfo.get("password") != null){
			etPassword.setText(userInfo.get("password"));
			etConfirmPassword.setText(userInfo.get("confirmPassword"));
			btSecurityQs.setText(userInfo.get("secQuest"));

			sSecurityQs = userInfo.get("standardSecurityQuestion");
			secQuest = userInfo.get("secQuest");
			etSecurityAns.setText(userInfo.get("securityAnswer"));
			etConfirmSecurityAns.setText(userInfo.get("securityAnswer"));			
		}
		tvCreateSQ.setOnClickListener(this);
		tvChooseSQ.setOnClickListener(this);
		
	}
	class SMSTask extends AsyncTask<String, String, String> {

		private Exception exception;
		protected String doInBackground(String... arg0) {
			try {
				userInfo.put("cac", "");
				userInfo.put("serial", "");
				MemberWebService cacWs = new MemberWebService();
				List<HashMap<String, String>> temp1 = cacWs
						.PmCac(	userInfo.get("nic"),
								userInfo.get("passport"),
								userInfo.get("country"),
								userInfo.get("mobileNumberPrefix"),
								userInfo.get("mobileNumberSuffix"));
				
				if(temp1.get(0).get("status").equals("0")){
					//ReturnMessage.showAlertDialog(this, "Email incorrect.");
					//etEmail.requestFocus();
					userInfo.put("cac", "");
					userInfo.put("serial", "");
					return "";
				}else{
					
					ContentResolver contentResolver = getContentResolver();
					Cursor cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
	
					int indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
					int indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
					int indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
					
					if ( indexBody < 0 || !cursor.moveToFirst() ) return "success";
					
					boolean flg = true;
					Calendar c = Calendar.getInstance();
					long starttime = c.get(Calendar.HOUR) * 3600 + c.get(Calendar.MINUTE)*60 + c.get(Calendar.SECOND);
					String serial1 = temp1.get(0).get("cacSerialNo");
					userInfo.put("serial", serial1);
					cursor.moveToNext();
					int i = 0;
					while(flg){
						i++;
						String str = cursor.getString(indexBody);
						String cac1 = "";
						String serial2 = "";
						int index = 0;
						if((index = str.indexOf("TAC Code:")) != -1){
							cac1 = str.substring(index+10, index+16);
							int index1 = str.indexOf("Serial No:");
							serial2 = str.substring(index1+11, index).trim();
						}						
						if((index = serial2.indexOf(serial1)) != -1){
							flg = false;
							userInfo.put("cac", cac1);
							//userInfo.put("serial", serial2);					
						}else{
						}
						if(i > 2){
							cursor.close();
							contentResolver = getContentResolver();
							cursor = contentResolver.query( Uri.parse( "content://sms/inbox" ), null, null, null, null);
							
							indexBody = cursor.getColumnIndex( SmsReceiver.BODY );
							indexAddr = cursor.getColumnIndex( SmsReceiver.ADDRESS );
							indexDate = cursor.getColumnIndex( SmsReceiver.DATE);
							cursor.moveToNext();
							i = 0;
						}else{
							cursor.moveToNext();
						}				
						try{
							Thread.sleep(200);
						}catch(Exception e){}
						Calendar c1 = Calendar.getInstance();
						long endtime = c1.get(Calendar.HOUR) * 3600 + c1.get(Calendar.MINUTE)*60 + c1.get(Calendar.SECOND);
						if(Math.abs(starttime - endtime) > 20){
							flg = false;
						}
					}
					//do
					//{
						//String str = "Sender: " + cursor.getString( indexAddr ) + "\n" + cursor.getString( indexBody );
						//smsList.add( str );
					//}
					//while( cursor.moveToNext() );
				}
				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {

			if (feed.equals("success")) {
				Log.d("success", "success");
			}
			String cc = userInfo.get("cac");
			if(cc == null){cc = "";}
			try{
				if(!cc.equals("")){
					tvCac.setText(userInfo.get("cac"));
				}
			}catch(Exception e){
				
			}
			/*
			 * 
			MerchantList.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					lvMerchant.onRefreshComplete();
					lvNearby.onRefreshComplete();					
				}
			});	
			 * */
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();	
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btCancel){
			Intent intent1 = new Intent(this, RegisterFourthPage.class);
			intent1.putExtra("userInfo", userInfo);
			this.startActivity(intent1);
			this.finish();
		}else if(v.getId() == R.id.btAgree){
			onAgree();
		}else if (v.getId() == R.id.btSecurityQs) {		
			if(selectionList != null && selectionList.length > 1){
				AlertDialog.Builder SQBuilder = new AlertDialog.Builder(this);
				SQBuilder.setTitle("Pick a security question:");
				SQBuilder.setItems(selectionList,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int item) {
								// TODO Auto-generated method stub
								btSecurityQs.setText(selectionList[item]);
								sSecurityQs = selectionListCode[item];
								secQuest = selectionList[item];
								Log.d("security qs selected", sSecurityQs);
								dialog.dismiss();
							}
						});
				AlertDialog SQAlert = SQBuilder.create();
				SQAlert.show();
			}else{
				ReturnMessage.showAlertDialog(this,
						"Slowly Internet. Please try again.");
			}
		}else if(v.getId() == R.id.tvCreateSQ){
			isCreateSQ = true;
			isChooseSQ = false;
			btSecurityQs.setVisibility(View.GONE);
			tvCreateSQ.setVisibility(View.GONE);
			etSecurityQs.setVisibility(View.VISIBLE);
			tvChooseSQ.setVisibility(View.VISIBLE);
			tvCac.setVisibility(View.VISIBLE);
		}else if(v.getId() == R.id.tvChooseSQ){
			isCreateSQ = false;
			isChooseSQ = true;
			btSecurityQs.setVisibility(View.VISIBLE);
			tvCreateSQ.setVisibility(View.VISIBLE);
			etSecurityQs.setVisibility(View.GONE);
			tvChooseSQ.setVisibility(View.GONE);
		}
	}
	class RetreiveFeedTask extends AsyncTask<String, String, String> {

		private Exception exception;

		protected String doInBackground(String... urls) {
			try {
				MemberWebService nation = new MemberWebService();
				List<HashMap<String, String>> temp = nation.getLsStdScrQuestion();
				int sizeTemp = temp.size();
				selectionList = new String[sizeTemp];
				selectionListCode = new String[sizeTemp];
				for (int i = 0; i < sizeTemp; i++) {
					selectionList[i] = temp.get(i).get("name");
					selectionListCode[i] = temp.get(i).get("code");
					Log.d(selectionList[i], selectionList[i]);
				}
				return "success";
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			if (feed.equals("success")) {
				Log.d("success", "success");
			}
			if(WebServiceLink.cacflg == true){
				if(userInfo.get("tacflg").equals("0")){
					new SMSTask().execute();
				}
				WebServiceLink.cacflg = false;
			}
		}
	}
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, RegisterFourthPage.class);
		intent.putExtra("userInfo", userInfo);
		this.startActivity(intent);
		this.finish();
	}
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	    if((actionId == EditorInfo.IME_ACTION_DONE) ||
	            (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
	        // TODO Auto-generated method stub
	    	//if(etState.length() > 0){	    	
	    	onAgree();
	    }
	    return false;
	}
	public void onAgree(){

		try{
		if (etPassword.length() == 0) {
			ReturnMessage.showAlertDialog(this, "Password is empty.");
			etPassword.requestFocus();
		} else if (etPassword.length() < 6) {
			ReturnMessage.showAlertDialog(this,
					"Password must be at least 6 alphanumeric.");
			etPassword.requestFocus();
		} else if (etConfirmPassword.length() == 0) {
			ReturnMessage.showAlertDialog(this,
					"Confirm Password is empty.");
			etConfirmPassword.requestFocus();
		} else if (etConfirmPassword.length() < 6) {
			ReturnMessage.showAlertDialog(this,
					"Confirm Password must be at least 6 alphanumeric.");
			etConfirmPassword.requestFocus();
		} else if (!etPassword.getText().toString()
				.equals(etConfirmPassword.getText().toString())) {
			ReturnMessage.showAlertDialog(this,
					"Password and Confirm Password do not match.");
			etConfirmPassword.requestFocus();
		} else if (sSecurityQs.equals("") && isChooseSQ == true) {
			ReturnMessage.showAlertDialog(this,
					"Security Question is required");
		} else if (etSecurityQs.length() == 0 && isCreateSQ == true) {
			ReturnMessage.showAlertDialog(this,
					"Security Question is required");
			etSecurityQs.requestFocus();
		} else if (tvCac.length() < 6) {
			ReturnMessage.showAlertDialog(this,
					"Incorrect TAC.");
			tvCac.requestFocus();
		} else if (etSecurityAns.length() == 0) {
			ReturnMessage.showAlertDialog(this,
					"Security answer is empty.");
			etSecurityAns.requestFocus();
		} else if (etConfirmSecurityAns.length() == 0) {
			ReturnMessage.showAlertDialog(this,
					"Confirm security answer is empty.");
			etConfirmSecurityAns.requestFocus();
		} else if (!etSecurityAns.getText().toString()
				.equals(etConfirmSecurityAns.getText().toString())) {
			ReturnMessage
					.showAlertDialog(this,
							"Security Answer and Confirm Security Answer do not match.");
			etConfirmSecurityAns.requestFocus();
		} else {
			String tmp = etPassword.getText().toString();
			

			Pattern p = Pattern.compile("[a-zA-z0-9]*"); 
			if (!p.matcher(tmp).matches()){
				new ConnectionDetector(this).Alert("Password should be alpha numeric.");
    		}else{
    			boolean f = false, g = false, com = false;
    			for(int i = 0; i < etPassword.length(); i++){
    				String a = etPassword.getText().toString().substring(i, i+1);
    				try{
    					int su = Integer.parseInt(a);
    					f = true;
    					if(g == false){
    						com = true; break;
    					}
    				}catch(Exception e){
    					g = true;
    					if(f == true){
    						com = true; break;
    					}
    				}
    			}
    			if(f == false || g == false){
    				com = true;
    			}
    			if(com == true){
    				boolean com2 = false;
    				f = false;g = false;
    				for(int i = 0; i < etPassword.length(); i++){
        				String a = etPassword.getText().toString().substring(i, i+1);
        				try{
        					int su = Integer.parseInt(a);
        					f = true;
        					if(g == true){
        						com2 = true; break;
        					}
        				}catch(Exception e){
        					g = true;
        					if(f == false){
        						com2 = true; break;
        					}
        				}
        			}
        			if(f == false || g == false){
        				com2 = true;
        			}
        			if(com2 == false){
        				com = false;
        			}
    			}
    			boolean com1 = false;
    			/*Pattern p1 = Pattern.compile("[ a-zA-z0-9]*"); 
				
    			
    			if(isCreateSQ == true){
    				String tmp1 = etSecurityQs.getText().toString();
    				if (!p1.matcher(tmp1).matches()){
    					com1 = true;
    					new ConnectionDetector(this).Alert("Security question should be alpha numeric.");
            		}
    			}*/
    			if(com == true){
    				new ConnectionDetector(this).Alert("Password should be alpha numeric. Example: abc1234");
    			}else if(com1 == true){
    			}else{        			
					userInfo.put("password", etPassword.getText().toString());
					userInfo.put("confirmPassword", etConfirmPassword.getText()
							.toString());
					userInfo.put("secQuest", secQuest);
					userInfo.put("cac", tvCac.getText().toString());
					if (isChooseSQ == true) {
						userInfo.put("standardSecurityQuestion", sSecurityQs);
						userInfo.put("customSecurityQuestion", "");
					} else if (isCreateSQ == true) {
						userInfo.put("standardSecurityQuestion", "0");
						userInfo.put("customSecurityQuestion", etSecurityQs
								.getText().toString());
					}
					userInfo.put("securityAnswer", etSecurityAns.getText()
							.toString());	
					//editor.putString("front", "");
					//editor.putString("back", "");
					//editor.commit();
					boolean connection = new ConnectionDetector(this)
					.isConnectStatus();
					if (connection) {
						Intent intent = new Intent(this, RegisterSixthPage.class);
						intent.putExtra("userInfo", userInfo);
						this.startActivity(intent);
						this.finish();
					}else{
						new ConnectionDetector(this).Alert("No connection available.");
					}
    			}
    		}
			
		}
		}catch(Exception e){
			ReturnMessage.showAlertDialog(this, "Please fill in the details.");
		}
	}
}