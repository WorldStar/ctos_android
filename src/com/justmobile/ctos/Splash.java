package com.justmobile.ctos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;



public class Splash extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
		
		setContentView(R.layout.splash);
		
		Thread splashTimer = new Thread(){
			public void run(){
				try{
					int splashTimer = 0;
					while (splashTimer < 100){
						sleep (100);
						splashTimer += 100;
					}
					//if(isNetworkAvailable()==true)
			        //{ 
		            	SharedPreferences ref = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		            	ref.edit().clear().commit();
		            	//isNetworkConnected();
		            	 Intent intent = new Intent(Splash.this, CTOSMainActivity.class);
		            	 intent.putExtra("active", "splash");
		                startActivity(intent);
		                finish();
			        //}else{
			        	 //finish();
			        //}
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					finish();
				}
			}
			
		};
		
		splashTimer.start();
	}
	public void onResume(Bundle savedInstanceState){
	    super.onResume();
	}
	private boolean isNetworkAvailable() {
		final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
	    
	    if(activeNetwork != null && activeNetwork.getState() == NetworkInfo.State.CONNECTED)
	    	return true;
	    else
	    	return false;
	    
	}
	
}

