package com.justmobile.ctos.library;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

public class ConvertImage {

	// Encode image from file path to base64, return as String
	public static String fileToBase64(String path) throws IOException {
		byte[] bytes = ConvertImage.fileToByteArray(path);
		return Base64.encodeBytes(bytes);
	}

	public static byte[] fileToByteArray(String path) throws IOException {
		File imagefile = new File(path);
		byte[] data = new byte[(int) imagefile.length()];
		FileInputStream fis = new FileInputStream(imagefile);
		fis.read(data);
		fis.close();
		return data;
	}

	// Decode base64 to file
	public static void base64ToFile(String path, String strBase64)
			throws IOException {
		byte[] bytes = Base64.decode(strBase64);
		byteArrayToFile(path, bytes);
	}

	public static void byteArrayToFile(String path, byte[] bytes)
			throws IOException {
		File imagefile = new File(path);
		File dir = new File(imagefile.getParent());
		if (!dir.exists()) {
			dir.mkdirs();
		}
		FileOutputStream fos = new FileOutputStream(imagefile);
		fos.write(bytes);
		fos.close();
	}

	// Encode bitmap to base64, return as String
	public static String bitmapToBase64(Bitmap bitmap) {
		byte[] bytes = ConvertImage.bitmapToByteArray(bitmap);
		return Base64.encodeBytes(bytes);
	}

	public static byte[] bitmapToByteArray(Bitmap bitmap) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 100, bos);
		byte[] bitmapData = bos.toByteArray();
		return bitmapData;
	}

	// Decode base64 to bitmap
	public static Bitmap base64ToBitmap(String strBase64) throws IOException {
		byte[] bitmapdata = Base64.decode(strBase64);
		Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0,
				bitmapdata.length);
		return bitmap;
	}


}