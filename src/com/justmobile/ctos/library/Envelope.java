package com.justmobile.ctos.library;

public class Envelope {

	final String header = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
			+ "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>";
	final String footer = "</soap:Body></soap:Envelope>";
	final String header2 = "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"><S:Body>";
	final String footer2 = "</S:Body></S:Envelope>";

	private String functionName;
	private String nameSpace;
	private String body = "";

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String name, String value) {
		body += "<" + name + ">" + value + "</" + name + ">";
	}

	public String getEnvelope() {
		return header2 + "<ns2:" + functionName + " xmlns:ns2=\"" + nameSpace
				+ "\">" + body + "</ns2:" + functionName + ">" + footer2;

	}

	public void setStartTag(String tag) {
		body += "<" + tag + ">";
	}

	public void setEndTag(String tag) {
		body += "</" + tag + ">";
	}
}