package com.justmobile.ctos.library;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PullXML {
	public static List<HashMap<String,String>> main (String string)
	throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput( new StringReader(string));
		int eventType = xpp.getEventType();
		String tag = "";
		String value = "";
		List<HashMap<String, String>> resultMaps = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> temp = new HashMap<String, String>();
		
		boolean save = false;
		
		while (eventType != XmlPullParser.END_DOCUMENT) {
			
			if(eventType == XmlPullParser.START_TAG) {
				
				tag = xpp.getName();
				
				if(tag.compareTo("return") == 0)
					save = true;
				
			} else if(eventType == XmlPullParser.TEXT) {
				
				value = xpp.getText();
				
				if (value == null)
					value = "";
				
				//only save the value under DocumentElement
				if (save){
					
					//if already have the same key, put it into array
					if (temp.containsKey(tag)){
						
						resultMaps.add(temp);
						temp = new HashMap<String, String>();
						
					}
					temp.put(tag, value);
				}
				
			} else if(eventType == XmlPullParser.END_TAG) {

				if(xpp.getName().compareTo("DocumentElement") == 0)
					save = false;
			} 
			
			eventType = xpp.next();
		}
		
		//add last element if not empty
		if (!temp.isEmpty()){
			resultMaps.add(temp);
		}
		
		return resultMaps;
	}
}
