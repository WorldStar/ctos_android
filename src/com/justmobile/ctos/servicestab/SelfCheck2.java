package com.justmobile.ctos.servicestab;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.R;
import com.justmobile.ctos.webservice.MemberWebService;

public class SelfCheck2 extends Activity {
	SharedPreferences settings;
	String email="",CID="",key="";
	TextView tvSelf;
	private ProgressDialog pDialog;
	Typeface font1, font2;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.servicestab_selfcheck2);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Self Check");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		try {
			email = settings.getString("email", null);
			CID= settings.getString("CID", null);
			key = settings.getString("Key", null);
		} catch (Exception e) {
			Log.d("Key", "123");
		}
		
		
		tvSelf = (TextView)findViewById(R.id.tvSelf); 
		String temail = "";
		if(email == null || email.equals("")){
			temail = "Your request of Self Check Report is successfully submitted. Your Self Check report has been sent to your CID email account ("+CID+"@ctosid.com.my).";
		}else{
			temail = "("+email+")";
			temail = "Your request of self check report is successfully submitted. " +
					"Your Self Check report has been sent to your CID email account ("+CID+"@ctosid.com.my) " +
					"and your personal email account "+temail+" that you have provided to us during registration. ";
		}
		tvSelf.setText(temail);
		Button btViewPDF = (Button) findViewById(R.id.btViewPDF);
		Button btViewPDFBack = (Button) findViewById(R.id.btViewPDFBack);
		btViewPDF.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SelfCheck2.this, SelfCheck3.class);
				SelfCheck2.this.startActivity(intent);
				finish();
			}
		});
		btViewPDFBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SelfCheck2.this, SelfCheck1.class);
				SelfCheck2.this.startActivity(intent);
				finish();
			}
		});
		String selcheck = settings.getString("selchecked", "");
		//if(selcheck == null)selcheck = "";
		if(selcheck == null || !selcheck.equals("1")){
			new retreiveData().execute(CID,key);
		}
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("selchecked", "0");
		editor.commit();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent service = new Intent(this,
				SelfCheck1.class);
		this.startActivity(service);
		this.finish();
	}
	class retreiveData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SelfCheck2.this);
			pDialog.setMessage("Verify Report ID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest.FnSelfcheck(urls[0],
						urls[1]);

				for (int i = 0; i < temp.size(); i++) {
					Log.d("mg", temp.get(i).toString());
				}

				if (temp.get(0).containsKey("status")) {
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("message");
					else{
						SharedPreferences.Editor editor = settings.edit();
						editor.putString("reportno", temp.get(0).get("reportNo"));
						editor.putString("reportid", temp.get(0).get("reportId"));
						editor.commit();
						return temp.get(0).get("status");
					}
				} else {

						return "Couldn't retrieve data";
				}

			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				

			} else {
				Alert(feed);
			}
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(SelfCheck2.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
}