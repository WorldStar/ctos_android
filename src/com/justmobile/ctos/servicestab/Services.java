package com.justmobile.ctos.servicestab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.infotab.Info;
import com.justmobile.ctos.profiletab.ProfileAfter;

public class Services extends TabGroupActivity implements OnClickListener {
	SharedPreferences settings;
	Typeface font1, font2;
	String CID="";
	//String cidstatus;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.servicestab_firstin);
		settings = getSharedPreferences("CTOS",
				Context.MODE_PRIVATE);
		CID = settings.getString("CID", "");
		//setHasOptionsMenu(true);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Services");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		//btlog.setVisibility(View.GONE);
		if(CID != null && !CID.equals("")){
			 btlog.setText("Logout");
		 }else{
			 btlog.setText("Login");
		 }
		btlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					
					if(CID != null && !CID.equals("")){
						Alert5();
					 }else{
						 Intent intent = new Intent(Services.this,
									CTOSLoginPage.class);
						startActivity(intent);
						finish();
					 }
			}
		});
		Button btSelfCheck = (Button) findViewById(R.id.btSelfCheck);
		String cidstatus = settings.getString("CIDSTATUS", null);
		LinearLayout chlayout= (LinearLayout)findViewById(R.id.chLayout);
		LinearLayout chlayout1= (LinearLayout)findViewById(R.id.chLayout1);
		ScrollView chcontent= (ScrollView)findViewById(R.id.chContent);
		if(cidstatus != null && cidstatus.equals("97")){
			btSelfCheck.setBackgroundResource(R.drawable.cancel2);
			chlayout.setVisibility(View.GONE);
			chlayout1.setVisibility(View.GONE);
			chcontent.setVisibility(View.VISIBLE);
		}else{
			btSelfCheck.setBackgroundResource(R.drawable.cancel1);
			if(cidstatus == null || cidstatus.equals("")){
				chlayout.setVisibility(View.VISIBLE);
				chlayout1.setVisibility(View.VISIBLE);
				chcontent.setVisibility(View.GONE);
			}else{
				chlayout.setVisibility(View.VISIBLE);
				chlayout1.setVisibility(View.VISIBLE);
				chcontent.setVisibility(View.GONE);
			}
		}
		//Button btConsent = (Button) v.findViewById(R.id.btConsent);
		Button btCTOSIDEmail = (Button) findViewById(R.id.btCTOSIDEmail);
		TextView textemail = (TextView)findViewById(R.id.temail);
		Linkify.addLinks(textemail, Linkify.ALL);
		textemail.setMovementMethod(LinkMovementMethod.getInstance());
		textemail.setLinksClickable(true);
		TextView textphone = (TextView)findViewById(R.id.tPhone);
		Linkify.addLinks(textphone, Linkify.ALL);
		textphone.setMovementMethod(LinkMovementMethod.getInstance());
		textphone.setLinksClickable(true);
		if(cidstatus == null)cidstatus = "";
		if(!cidstatus.equals("97")){
			btSelfCheck.setOnClickListener(this);
		}
		//btConsent.setOnClickListener(this);
		btCTOSIDEmail.setOnClickListener(this);
	}

	public void Alert5() {
		new AlertDialog.Builder(Services.this)
		.setTitle("CTOS")
		.setMessage("Are you sure to log out?")

		.setPositiveButton("YES", new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            // Do nothing but close the dialog

			Intent intent = new Intent(Services.this,
					CTOSMainActivity.class);
			startActivity(intent);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.remove("CID");
			editor.remove("CIDSTATUS");
			editor.commit();
			finish();
            dialog.dismiss();
	    }
	
	    })
	
	    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
	
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing
	            dialog.dismiss();
	        }
	    })
	    .show();
   }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btSelfCheck) {
			if(CID != null && !CID.equals("")){
					boolean connection = new ConnectionDetector(Services.this)
					.isConnectStatus();
					if (connection) {
						Intent selfCheck = new Intent(Services.this, SelfCheck1.class);
						startActivity(selfCheck);
					}else{
						new ConnectionDetector(Services.this).Alert("No connection available.");
					}
			}else{
				Alert("Please log in to perform Self Check");
			}
			//getSherlockActivity().finish();
		}else if(v.getId() == R.id.btCTOSIDEmail){
			
			boolean connection = new ConnectionDetector(Services.this)
			.isConnectStatus();
			if (connection) {
				Intent CTOSIDEmail = new Intent(Services.this,
						CTOSIDEmail.class);
				startActivity(CTOSIDEmail);
			}else{
				new ConnectionDetector(Services.this).Alert("No connection available.");
			}
			//getSherlockActivity().finish();
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(Services.this).setTitle("CTOS")
				.setMessage(msg)
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(Services.this,
								CTOSLoginPage.class);
						 intent.putExtra("logocontent", "2");
						startActivity(intent);
						finish();
						dialog.dismiss();
					}
				}).show();

	}

}