package com.justmobile.ctos.servicestab;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.R;

public class CTOSIDEmail1 extends SherlockActivity {

	static String url = "http://ctosid.ctos.com.my:8888/mail/";
	static String pdf = "http://dspace.mit.edu/bitstream/handle/1721.1/6432/AIM-864.pdf";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.topbar01));

		WebView webview = (WebView) findViewById(R.id.webView);
		webview.getSettings().setLoadWithOverviewMode(true);
		webview.getSettings().setUseWideViewPort(true);
		webview.getSettings().setBuiltInZoomControls(true);
		// webview being your WebView object reference.
		float scale = webview.getScale();
		scale = 10.0f * scale;
		webview.setInitialScale((int)scale);
		webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

		webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				Log.d("url", url);
			}
		});
		webview.loadUrl(url);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
		}*/
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
	}
}