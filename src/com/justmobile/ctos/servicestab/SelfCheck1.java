package com.justmobile.ctos.servicestab;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.infotab.Info;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.profiletab.RegisterFirstPage;



public class SelfCheck1 extends Activity implements OnClickListener {

	WebView selfCheckWebView;
	private ProgressDialog pDialog;
	static String bm = "", en = "";
	String initial = "";
	Button translate;
	int tr = 0;
	Typeface font1, font2;
	Button translateEn, translateMy;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.servicestab_selfcheck1);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Self Check");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		
		translateEn = (Button) findViewById(R.id.btEn);
		translateEn.setBackgroundResource(R.drawable.agree2);
		translateMy = (Button) findViewById(R.id.btMy);
		

		selfCheckWebView = (WebView) findViewById(R.id.SelfCheckWebView);
		selfCheckWebView.getSettings().setBuiltInZoomControls(true);
		Button btReadStatutory = (Button) findViewById(R.id.btReadStatutory);
		Button btCancel = (Button) findViewById(R.id.btCancel);
		String content = "<body>Display content of the Statutory Notification</body>";
		//selfCheckWebView.loadDataWithBaseURL(null, content, "text/html","UTF-8", null);
		//selfCheckWebView.loadUrl("http://www.justmobile.com.my/test/ctos/pages/Explanation%20and%20Statutory%20Notification%20(English).xml");
		new sendingDataSelf()
		.execute(
				"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/0716d9708d321ffb6a00818614779e779925365c",
				"1");
		
		btReadStatutory.setOnClickListener(this);
		btCancel.setOnClickListener(this);
		translateEn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {				
					boolean connection = new ConnectionDetector(SelfCheck1.this)
					.isConnectStatus();
					if (connection) {
						new sendingDataSelf()
						.execute(
								"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/0716d9708d321ffb6a00818614779e779925365c",
								"1");
						translateEn.setBackgroundResource(R.drawable.agree2);
						translateMy.setBackgroundResource(R.drawable.agree1);
					}else{
						new ConnectionDetector(SelfCheck1.this).Alert("No connection available.");
					}
			}
		});
		translateMy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				

					boolean connection = new ConnectionDetector(SelfCheck1.this)
					.isConnectStatus();
					if (connection) {
						new sendingDataSelf()
						.execute(
								"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/472b07b9fcf2c2451e8781e944bf5f77cd8457c8",
								"0");// bm bm
						translateEn.setBackgroundResource(R.drawable.agree1);
						translateMy.setBackgroundResource(R.drawable.agree2);
					}else{
						new ConnectionDetector(SelfCheck1.this).Alert("No connection available.");
					}
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btReadStatutory) {

			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				Intent intent = new Intent(this, SelfCheck2.class);
				this.startActivity(intent);
				this.finish();
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
		}else if(v.getId() == R.id.btCancel){
			/*Intent service = new Intent(this,
					com.justmobile.ctos.afterLogin.AfterLogin.class);
			this.startActivity(service);
			this.finish();*/
			//onBackPressed();
			finish();
		}
	}

	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent service = new Intent(this,
				com.justmobile.ctos.afterLogin.AfterLogin.class);
		this.startActivity(service);
		this.finish();
	}*/
	class sendingDataSelf extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SelfCheck1.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				Post post = new Post();
				String po = post.CallWebService(urls[0], "1", "");
				if (urls[1].equals("1"))
					initial = "1";
				else
					initial = "0";

				return po;
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		@Override
		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			/*String sXML = feed.replaceAll("[\r\n]+", "");
			// String test = sXML.replaceAll("[\r\n]+", "");
			DocumentBuilder db = null;
			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(sXML));

			Document doc = null;
			try {
				doc = db.parse(is);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			NodeList nodes = doc.getElementsByTagName("page");
			Element element = (Element) nodes.item(0);
			NodeList name = element.getElementsByTagName("page_name");
			Element line = (Element) name.item(0);
			if (initial.equals("1"))
				en = line.getTextContent();
			else
				bm = line.getTextContent();

			//webview.loadDataWithBaseURL(null, getCharacterDataFromElement(line), "text/html", "UTF-8", null);
			selfCheckWebView.loadDataWithBaseURL(null,
					line.getTextContent(), "text/html", "UTF-8",
					null);
			*/
			try{
				String finalString="";
				for(int i=0;i<feed.length();i++)
				{
					if(feed.charAt(i)!='\\')
							finalString+=feed.charAt(i);
					
				}
				String sXML = "";
				sXML = finalString.replaceAll("rn", "");
				int index = 0;
				if((index = sXML.indexOf("content"))!=-1){
					sXML = sXML.substring(index+10, sXML.length()-2);
				}
				//Spanned span = Html.fromHtml(sXML);
				//sXML = span.toString();
				//sXML = TextUtils.htmlEncode(sXML);
				
				
				
				selfCheckWebView.loadData(sXML, "text/html", "utf-8");	
				
				
			}catch(Exception e){
				ReturnMessage.showAlertDialog(SelfCheck1.this,
						"Internet is invalid.");
			}
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}