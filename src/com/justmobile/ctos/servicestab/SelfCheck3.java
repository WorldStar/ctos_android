package com.justmobile.ctos.servicestab;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.R;
import com.justmobile.ctos.webservice.MemberWebService;


public class SelfCheck3 extends Activity {

	SharedPreferences settings;
	String status = "";
	String CID = "", key = "", reId = "", reNo = "", report = "";
	private ProgressDialog pDialog;
	WebView webview;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview1);


		settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		try {
			status = settings.getString("Status", null);
			CID = settings.getString("CID", null);
			key = settings.getString("Key", null);
			reId = settings.getString("reportid", "162");
			reNo = settings.getString("reportno", "31033-20121024101144");
		} catch (Exception e) {
			status = "-1";
		}
		
		Log.d("selfcheck", status);
		new retreiveData().execute(CID, key, reId, reNo);
		webview = (WebView) findViewById(R.id.webView);
		float scale = 60 * webview.getScale();
		webview.setInitialScale( (int) scale );
		webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
		webview.getSettings().setDefaultTextEncodingName("utf-8");

	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//Intent service = new Intent(this, com.justmobile.ctos.afterLogin.AfterLogin.class);
		//this.startActivity(service);
		//this.finish();
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("selchecked", "1");
		editor.commit();
		Intent service = new Intent(this,
				SelfCheck2.class);
		this.startActivity(service);
		this.finish();
	}

	/*public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode == KeyEvent.KEYCODE_BACK) {
	     //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
	    		Intent i = new Intent(Registration_third.this,
						SignIn.class);
				startActivity(i);
				finish();
	    	 return true;
	     }
	     return super.onKeyDown(keyCode, event);    
	}*/
	class retreiveData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SelfCheck3.this);
			pDialog.setMessage("Verify Report ID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest.FnSelfcheckReport(
						urls[0], urls[1], urls[2], urls[3]);

				for (int i = 0; i < temp.size(); i++) {
					Log.d("mg", temp.get(i).toString());
				}

				if (temp.get(0).containsKey("status")) {
					if (temp.get(0).get("status").equals("0"))
						return temp.get(0).get("message");
					else {
						report = temp.get(0).get("report");
						return temp.get(0).get("status");
					}
				} else {

					return "Couldn't retrieve data";
				}

			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				byte[] html = Base64.decode(report, Base64.DEFAULT);
				String encodeHtml = new String(html);
				//float scale = 0.25f * webview.getScale();
				//webview.setInitialScale((int)scale);
				float scale = 60 * webview.getScale();
				webview.setInitialScale( (int) scale );
				webview.loadDataWithBaseURL(null, encodeHtml, "text/html", "UTF-8", null);
				
			} else {
				Alert(feed);
			}
		}
	}

	public void Alert(String msg) {
		new AlertDialog.Builder(SelfCheck3.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	/*PGh0bWwgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIGxhbmc9ImVuLVVTIj4KPFNUWUxFIFRZUEU9InRleHQvY3NzIj4KCQkJICAgVEZPT1Qge2JhY2tncm91bmQtY29sb3I6IHdoaXRlOyBjb2xvcjogYmxhY2s7IH0KICAgICAgVEFCTEUge2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OHB0OyBiYWNrZ3JvdW5kLWNvbG9yOiNGQ0UwQzI7fQogICAgICBUSCB7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZTo4cHQ7IGNvbG9yOiMwMDAwMDA7fSAKICAgICAgVEQge2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OHB0OyBjb2xvcjpibGFjazt2ZXJ0aWNhbC1hbGlnbjp0b3B9IAogICAgICBIMSB7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZToyNHB0OyBjb2xvcjpyZWQ7fQogICAgICBIMiB7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZToxMnB0OyBjb2xvcjpibHVlO30KICAgICAgSU5QVVQuVEVYVCB7Y29sb3I6cmVkOyBiYWNrZ3JvdW5kLWNvbG9yOmJsdWU7fQoJdGguYm9sZCAgICAgIHsgdGV4dC1hbGlnbjogcmlnaHQ7IGZvbnQtZmFtaWx5OiBBcmlhbDsgZm9udC1zaXplOiA4cHQ7IGNvbG9yOiAjMDAwMDAwIH0KICAgICAgQk9EWSB7Zm9udC1mYW1pbHk6QXJpYSxzYW5zLXNlcmlmOyBmb250LXNpemU6MTBwdDsgY29sb3I6YmxhY2s7IGJhY2tncm91bmQtY29sb3I6ICNGQ0UwQzI7IH0KICAgICAgUCB7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZTo4cHQ7IGNvbG9yOmJsYWNrO30KICAgICAgUC5ub3JtYWwge2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OHB0OyBjb2xvcjpibGFjazt9CiAgICAgIFAuZml4d2lkdGgge2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OHB0OyBjb2xvcjpibGFjazt9CiAgICAgIFAucGFnZWJyZWFrIHsgcGFnZS1icmVhay1iZWZvcmU6IGFsd2F5czt9CgogICAgICAgIAoJQTpsaW5rICB7dGV4dC1kZWNvcmF0aW9uOiBub25lO30KIAlBOnZpc2l0ZWQge3RleHQtZGVjb3JhdGlvbjogbm9uZTt9CiAJQTphY3RpdmUge3RleHQtZGVjb3JhdGlvbjogbm9uZTt9CiAgICAgICAgdGgubGVmdCAgICAgIHsgdGV4dC1hbGlnbjogbGVmdDsgZm9udC1mYW1pbHk6IEFyaWFsOyBmb250LXNpemU6IDhwdDsgY29sb3I6ICMwMDAwMDAgfQogICAgICB0aC5yaWdodCAgICAgIHsgdGV4dC1hbGlnbjogcmlnaHQ7IGZvbnQtZmFtaWx5OiBBcmlhbDsgZm9udC1zaXplOiA4cHQ7IGNvbG9yOiAjMDAwMDAwIH0KICAgICAgdGguY2FwdGlvbiB7d2lkdGg6MTUwO30KICAgICAgICAKICAgICAgICB0ZC5udW1iZXIge3RleHQtYWxpZ246cmlnaHQ7fQogICAgICAgPC9TVFlMRT4KPHN0eWxlIG1lZGlhPSJzY3JlZW4scHJpbnQiIHR5cGU9InRleHQvY3NzIj4KICAgICAgLmhlYWRpbmcge2ZvbnQtZmFtaWx5OiBBcmlhbCwgc2FuLXNlcmlmOyBmb250LXdlaWdodDogYm9sZDt9CiAgICAgIC5oZWFkZXJ7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZToxOHB0OyBmb250LXdlaWdodDogYm9sZDt9CiAgICAgIC5oZWFkZXIxe2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6MTRwdDsgZm9udC13ZWlnaHQ6IGJvbGQ7fQogICAgICAudWxfaGVhZGVyMXtmb250LWZhbWlseTpBcmlhbDsgZm9udC1zaXplOjE0cHQ7ICBmb250LXdlaWdodDogYm9sZDsgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7fQogICAgICAgLmhlYWRlcjJ7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZToxMnB0OyBmb250LXdlaWdodDogYm9sZDt9CiAgICAgIC51bF9oZWFkZXIye2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6MTJwdDsgIGZvbnQtd2VpZ2h0OiBib2xkOyB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTt9CiAgICAgICAuaGVhZGVyM3tmb250LWZhbWlseTpBcmlhbDsgZm9udC1zaXplOjEwcHQ7IGZvbnQtd2VpZ2h0OiBib2xkO30KICAgICAgLnVsX2hlYWRlcjN7Zm9udC1mYW1pbHk6QXJpYWw7IGZvbnQtc2l6ZToxMHB0OyAgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO30KICAgICAgIC5oZWFkZXI0e2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OXB0OyBmb250LXdlaWdodDogYm9sZDt9CiAgICAgIC51bF9oZWFkZXI0e2ZvbnQtZmFtaWx5OkFyaWFsOyBmb250LXNpemU6OXB0OyAgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO30KICAgICAgLm1haW50YWJsZSB7IHRleHQtYWxpZ246Y2VudGVyOyB9CiAgICAgLnRoX3JpZ2h0IHsgdGV4dC1hbGlnbjpyaWdodDsgZm9udC13ZWlnaHQ6Ym9sZDt9CiAgICAgLnRkX2xlZnQgeyB0ZXh0LWFsaWduOiBsZWZ0O30KICAgICAuc2lnbmF0dXJlIHsgd2lkdGg6IDEwZW07IGJvcmRlci1zdHlsZTpub25lO30KICAgICA8L3N0eWxlPgo8Ym9keT4KPHRhYmxlIHhtbG5zPSIiIHhtbG5zOmZvPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L1hTTC9Gb3JtYXQiIGNsYXNzPSJtYWludGFibGUiIHdpZHRoPSIxMDAlIiBhbGlnbj0iY2VudGVyIj4KPFRSPgo8VEggY2xhc3M9InRoX2xlZnQiPkRBVEU6PC9USD48VEQgY2xhc3M9InRkX2xlZnQiPjIwMTItMTAtMjI8L1REPjxUSCBjbGFzcz0idGhfcmlnaHQiPlRJTUU6PC9USD48VEQgY2xhc3M9InRkX2xlZnQiPjE4OjI0OjE5PC9URD48VEggY2xhc3M9InRoX3JpZ2h0Ij5SRVBPUlQgTk86PC9USD48VEQgY2xhc3M9InRkX2xlZnQiPjkzMTEtMjAxMjEwMjIxODI0MTg8L1REPjxUSCBjbGFzcz0idGhfcmlnaHQiPkF0dGVuZGVkIGJ5OjwvVEg+PFREIGNsYXNzPSJ0ZF9sZWZ0Ij5zZWxmY2hlY2tjZHM8L1REPgo8L1RSPgo8L3RhYmxlPgo8RElWIGNsYXNzPSJoZWFkZXIiIGFsaWduPSJjZW50ZXIiPkNUT1MgU0VMRiBDSEVDSyBSRVBPUlQgREFURUQgMjAxMi0xMC0yMjwvRElWPgo8RElWIGNsYXNzPSJoZWFkZXIxIiBhbGlnbj0iY2VudGVyIj5BdCB0aGUgcmVxdWVzdCBvZiBUT0ggQkFOTiBMRUUgJm5ic3A7Cgk5Nzc5MjIxIC8gCgkwMDAwMDAwMDAwMDA8L0RJVj4KPGJyPgo8RElWPkRhdGUgOiA8c3BhbiBzdHlsZT0ibWFyZ2luLWxlZnQ6M2
*/
	

}