package com.justmobile.ctos.infotab;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockMapActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.R;
import com.justmobile.ctos.library.HelloItemizedOverlay;

public class Contact1 extends MapActivity {
	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// // TODO Auto-generated method stub
	// View v = inflater.inflate(R.layout.infotab_contact, container, false);
	// return v;
	// }
	Button navigate;
	TextView tvEmail, tvPhone;
	String lat = "", lon = "";
	Typeface font1, font2;
	String preinfo = "";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.infotab_contact);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		 Intent intent = getIntent();

		preinfo = intent.getStringExtra("info");
		if(preinfo == null)preinfo = "";
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		LinearLayout textLayout = (LinearLayout)findViewById(R.id.textlayout);
		textLayout.setVisibility(View.GONE);
		tvPageTitle.setText("Location Map");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		//btlog.setVisibility(View.GONE);
		btlog.setText("  Navigate  ");
		btlog.setVisibility(View.GONE);
		MapView mapView = (MapView) findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);

		List<Overlay> mapOverlays = mapView.getOverlays();
		Drawable drawable = this.getResources().getDrawable(R.drawable.map_pin);
		HelloItemizedOverlay itemizedoverlay = new HelloItemizedOverlay(
				drawable, this);

		GeoPoint point = new GeoPoint((int) (3.16315 * 1e6),
				(int) (101.717000 * 1e6));

		OverlayItem overlayitem = new OverlayItem(point, "CTOS", "");
		itemizedoverlay.addOverlay(overlayitem);
		mapOverlays.add(itemizedoverlay);
		MapController mapController = mapView.getController();
		mapController.animateTo(point);
		mapController.setZoom(15);
		tvEmail = (TextView) findViewById(R.id.tvEmail);
		Linkify.addLinks(tvEmail, Linkify.ALL);
		tvEmail.setMovementMethod(LinkMovementMethod.getInstance());
		tvEmail.setLinksClickable(true);
		tvPhone = (TextView) findViewById(R.id.tvPhone);
		
		btlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				Location loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				//locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
				final LocationListener listener = new LocationListener() {

			        public void onLocationChanged(Location location) {
			        }

			        public void onProviderDisabled(String provider) {
			        }

			        public void onProviderEnabled(String provider) {
			        }

			        public void onStatusChanged(String provider, int status, Bundle extras) {
			        }
			    };
			    mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
			    loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				
			    
				if (loc != null) {
					lat = String.valueOf(loc.getLatitude());
				    //Log.v("gps", lat.toString());
				    lon = String.valueOf(loc.getLongitude());
				    //Log.v("gps", lat.toString());
				} else {
					Toast.makeText(Contact1.this,
							"GPS is not available in your devide",
							Toast.LENGTH_SHORT).show();
				}
				final Intent intent = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse("http://maps.google.com/maps?"
								+ "saddr="
								+ lat
								+ ","
								+ lon
								+ "&daddr=Ctos Sdn Bhd Kampung Baru Kuala Lumpur Federal Territory of Kuala Lumpur"));
				// + src_lat + ","+ src_long));
				intent.setClassName("com.google.android.apps.maps",
						"com.google.android.maps.MapsActivity");
				startActivity(intent);
			}
		});
		/*tvEmail.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent emailIntent = new Intent(
						android.content.Intent.ACTION_SENDTO);

				
				emailIntent.setType("plain/text");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
						new String[] { "gen@ctos.com.my" });
				emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Subject");
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

				//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(Intent.createChooser(emailIntent, "Send mail..."));
				
			}
		});*/
		tvPhone.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				
			}
		});

		/*navigate = (Button) findViewById(R.id.btNavigate);
		navigate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				Location loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				//locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
				final LocationListener listener = new LocationListener() {

			        public void onLocationChanged(Location location) {
			        }

			        public void onProviderDisabled(String provider) {
			        }

			        public void onProviderEnabled(String provider) {
			        }

			        public void onStatusChanged(String provider, int status, Bundle extras) {
			        }
			    };
			    mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
			    loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				
			    
				if (loc != null) {
					lat = String.valueOf(loc.getLatitude());
				    //Log.v("gps", lat.toString());
				    lon = String.valueOf(loc.getLongitude());
				    //Log.v("gps", lat.toString());
				} else {
					Toast.makeText(Contact.this,
							"GPS is not available in your devide",
							Toast.LENGTH_SHORT).show();
				}
				final Intent intent = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse("http://maps.google.com/maps?"
								+ "saddr="
								+ lat
								+ ","
								+ lon
								+ "&daddr=Ctos Sdn Bhd Kampung Baru Kuala Lumpur Federal Territory of Kuala Lumpur"));
				// + src_lat + ","+ src_long));
				intent.setClassName("com.google.android.apps.maps",
						"com.google.android.maps.MapsActivity");
				startActivity(intent);
			}
		});*/
	}


	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(preinfo.equals("1")){
			finish();
		}else{
			Intent intent = new Intent(this, CTOSMainActivity.class);
			intent.putExtra("active", "info");
			//this.startActivity(intent);
			startActivity(intent);
			finish();
		}
	}

}