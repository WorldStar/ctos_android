package com.justmobile.ctos.infotab;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.library.Post;

public class Info1 extends SherlockFragment implements OnClickListener {
	static Button btSummary;
	static Button btContact, btHelp;
	SharedPreferences settings;
	WebView webview, webview1;
	String status = "";
	PopupWindow pw, pw1;
	int tr = 0;
	Button btDone, btChange, btDone1;
	private ProgressDialog pDialog;
	static String bm = "", en = "";
	String initial = "";
	LinearLayout btnlayout, summarylayout, helplayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		settings = getSherlockActivity().getSharedPreferences("CTOS",
				Context.MODE_PRIVATE);
		try {
			status = settings.getString("Status", null);
		} catch (Exception e) {
			status = "-1";
		}
		View v = inflater.inflate(R.layout.infotab_firstin, container, false);
		// setHasOptionsMenu(true);
		// ActionBar actionBar = getSherlockActivity().getSupportActionBar();
		// actionBar.setBackgroundDrawable(getResources().getDrawable(
		// R.drawable.topbar01));

		btSummary = (Button) v.findViewById(R.id.btSummary);
		btContact = (Button) v.findViewById(R.id.btContact);
		btHelp    = (Button) v.findViewById(R.id.btHelp);
		btnlayout = (LinearLayout)v.findViewById(R.id.btnlayout);
		helplayout = (LinearLayout)v.findViewById(R.id.helplayout);
		summarylayout = (LinearLayout)v.findViewById(R.id.summarylayout);
		btHelp.setOnClickListener(this);
		btSummary.setOnClickListener(this);
		btContact.setOnClickListener(this);
		helplayout.setVisibility(View.GONE);
		summarylayout.setVisibility(View.GONE);
		//summary
		btDone = (Button) v.findViewById(R.id.btDoneSummary);
		btChange = (Button) v.findViewById(R.id.btChangeLanguage);
		btDone.setOnClickListener(cancel_button_click_listener);
		btChange.setOnClickListener(change_button_click_listener);

		webview = (WebView) v.findViewById(R.id.webviewSummary);
		webview.getSettings().setSupportZoom(true);
		
		//webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		//webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webview.getSettings().setBuiltInZoomControls(true);
		//help
		btDone1 = (Button) v.findViewById(R.id.btDoneHelp);
		btDone1.setOnClickListener(cancel_button_click_listener1);
		
		webview1 = (WebView) v.findViewById(R.id.webviewHelp);
		webview1.getSettings().setSupportZoom(true);
		
		//webview1.getSettings().setLoadWithOverviewMode(true);
		webview1.getSettings().setUseWideViewPort(true);
		webview1.getSettings().setBuiltInZoomControls(true);
		//float scale = webview1.getScale();
		//scale = 0.3f * scale;
		webview1.setInitialScale(1);
		webview1.loadUrl("file:///android_asset/help.jpg");
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		if (!status.equals("-1") && !status.equals("")) {
			Log.d("STATUS", status);
			MenuItem login = menu.add(Menu.NONE, 1, 0, "").setIcon(R.drawable.logout);
			login.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			super.onCreateOptionsMenu(menu, inflater);

		} else {
			Log.d("lol", status);
			MenuItem login = menu.add(Menu.NONE, 0, 0, "Login");
			login.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			super.onCreateOptionsMenu(menu, inflater);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			this.startActivity(intent);
			getSherlockActivity().finish();
			break;
		case 1:
			Intent intent1 = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			this.startActivity(intent1);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.commit();
			getSherlockActivity().finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		boolean connection = new ConnectionDetector(getSherlockActivity())
		.isConnectStatus();
		if (connection) {
			if (v.getId() == R.id.btSummary) {
				btnlayout.setVisibility(View.GONE);
				helplayout.setVisibility(View.GONE);
				summarylayout.setVisibility(View.VISIBLE);
				initiatePopupWindow();
			}else if(v.getId() == R.id.btContact){
				//Intent intent = new Intent(getSherlockActivity(), Contact1.class);
				//getSherlockActivity().startActivity(intent);
				//getSherlockActivity().finish();
			
			}else if(v.getId() == R.id.btHelp){
				btnlayout.setVisibility(View.GONE);
				helplayout.setVisibility(View.VISIBLE);
				summarylayout.setVisibility(View.GONE);
				initiatePopupWindow1();
			}
		}else{
			new ConnectionDetector(getSherlockActivity()).Alert("No connection available.");
		}
	}

	private void initiatePopupWindow() {
		try {
			/*LayoutInflater inflater = (LayoutInflater) getSherlockActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View layout = inflater.inflate(
					R.layout.summaryofright,
					(ViewGroup) getSherlockActivity().findViewById(
							R.id.llsummaryofright));

			Drawable background = layout.getBackground();
			background.setAlpha(180);

			pw = new PopupWindow(layout, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT, true);
			pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

			btDone = (Button) layout.findViewById(R.id.btDoneSummary);
			btChange = (Button) layout.findViewById(R.id.btChangeLanguage);
			btDone.setOnClickListener(cancel_button_click_listener);
			btChange.setOnClickListener(change_button_click_listener);

			webview = (WebView) layout.findViewById(R.id.webviewSummary);
			*/
			new sendingData()
					.execute(
							"http://justmobile.com.my/test/ctos/pages/Summary%20of%20Rights%20(English).xml",
							"1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initiatePopupWindow1() {
		try {
			/*LayoutInflater inflater = (LayoutInflater) getSherlockActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View layout = inflater
					.inflate(R.layout.help, (ViewGroup) getSherlockActivity()
							.findViewById(R.id.llhelp));

			Drawable background = layout.getBackground();
			background.setAlpha(180);

			pw1 = new PopupWindow(layout, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT, true);
			pw1.showAtLocation(layout, Gravity.CENTER, 0, 0);

			btDone1 = (Button) layout.findViewById(R.id.btDoneHelp);
			btDone1.setOnClickListener(cancel_button_click_listener1);

			webview1 = (WebView) layout.findViewById(R.id.webviewHelp);
			webview1.getSettings().setSupportZoom(true);
			//webview1.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
			//webview1.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
			webview1.getSettings().setBuiltInZoomControls(true);
			webview1.getSettings().setLoadWithOverviewMode(true);
			webview1.getSettings().setUseWideViewPort(true);
			//webview1.getSettings().setBuiltInZoomControls(true);
			//float scale = 40 * webview1.getScale();
			//webview1.setInitialScale((int) scale);
			webview1.loadUrl("file:///android_asset/help1.html");
			*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private OnClickListener cancel_button_click_listener1 = new OnClickListener() {
		public void onClick(View v) {

			btnlayout.setVisibility(View.VISIBLE);
			helplayout.setVisibility(View.GONE);
			summarylayout.setVisibility(View.GONE);
			//if (pw1.isShowing())
			//	pw1.dismiss();
		}
	};
	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			btnlayout.setVisibility(View.VISIBLE);
			helplayout.setVisibility(View.GONE);
			summarylayout.setVisibility(View.GONE);
			//if (pw.isShowing())
			//	pw.dismiss();
		}
	};
	private OnClickListener change_button_click_listener = new OnClickListener() {
		public void onClick(View v1) {
			if (tr % 2 == 0) {
				if (bm.length() < 1)
					new sendingData()
							.execute(
									"http://justmobile.com.my/test/ctos/pages/Summary%20of%20Rights%20(Malay).xml",
									"0");// bm
				else {
					webview.loadDataWithBaseURL(null, bm, "text/html", "UTF-8",
							null);
					// webview.loadData(bm, "text/html", null);
				}
				Button v = (Button) v1;
				v.setText("EN");
			} else {
				if (en.length() < 1)
					new sendingData()
							.execute(
									"http://justmobile.com.my/test/ctos/pages/Summary%20of%20Rights%20(English).xml",
									"1");
				else {
					webview.loadDataWithBaseURL(null, en, "text/html", "UTF-8",
							null);
					// webview.loadData(en, "text/html", null);
				}
				Button v = (Button) v1;
				v.setText("BM");
			}
			tr++;
		}
	};

	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getSherlockActivity());
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				Post post = new Post();
				String po = post.CallWebService(urls[0], "1", "");
				if (urls[1].equals("1"))
					initial = "1";
				else
					initial = "0";

				return po;
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			String sXML = feed.replaceAll("[\r\n]+", "");
			// String test = sXML.replaceAll("[\r\n]+", "");
			DocumentBuilder db = null;
			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(sXML));

			Document doc = null;
			try {
				doc = db.parse(is);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			NodeList nodes = doc.getElementsByTagName("page");
			Element element = (Element) nodes.item(0);
			NodeList name = element.getElementsByTagName("page_name");
			Element line = (Element) name.item(0);
			if (initial.equals("1"))
				en = line.getTextContent();
			else
				bm = line.getTextContent();
			webview.loadDataWithBaseURL(null, line.getTextContent(),
					"text/html", "UTF-8", null);
			// webview.loadData(getCharacterDataFromElement(line),
			// "text/html",null);
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
}