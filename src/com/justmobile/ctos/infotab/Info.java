package com.justmobile.ctos.infotab;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.TextSize;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.hometab.Home;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.profiletab.RegisterFirstPage;
import com.justmobile.ctos.servicestab.SelfCheck1;
import com.justmobile.ctos.servicestab.Services;
import com.justmobile.ctos.webservice.WebServiceLink;

public class Info extends TabGroupActivity implements OnClickListener {
	static Button btSummary;
	static Button btContact,btHelp, btLocation;
	SharedPreferences settings;
	WebView webview,webview1;
	String status = "";
	PopupWindow pw,pw1;
	int tr = 0;
	Button btDone, btChange,btDone1;
	private ProgressDialog pDialog; 
	static String bm = "", en = "";
	String initial = "";
	LinearLayout btnlayout, summarylayout, helplayout;
	Typeface font1, font2;
	Button translateEn, translateMy;
	LinearLayout clayout;
	String cid = "";
	TextView tvPageTitle;
	String lat = "", lon = "";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.infotab_firstin);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Info");
		tvPageTitle.setTypeface(font1);

		settings = getSharedPreferences("CTOS",
				Context.MODE_PRIVATE);
		Button btlog = (Button) findViewById(R.id.btLog);
		//btlog.setVisibility(View.GONE);
		cid = settings.getString("CID", "");
		if(cid != null && !cid.equals("")){
			 btlog.setText("Logout");
		 }else{
			 btlog.setText("Login");
		 }
		btlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					
					if(cid != null && !cid.equals("")){

						Alert5();
					 }else{
						 Intent intent = new Intent(Info.this,
									CTOSLoginPage.class);
						startActivity(intent);
						finish();
					 }
			}
		});
		clayout = (LinearLayout)findViewById(R.id.cLayout);
		clayout.setVisibility(View.GONE);
		translateEn = (Button) findViewById(R.id.btEn);
		translateEn.setBackgroundResource(R.drawable.agree2);
		translateMy = (Button) findViewById(R.id.btMy);
		
		try {
			status = settings.getString("Status", null);
		} catch (Exception e) {
			status = "-1";
		}

		btSummary = (Button) findViewById(R.id.btSummary);
		btContact = (Button) findViewById(R.id.btContact);
		btHelp    = (Button) findViewById(R.id.btHelp);
		btLocation = (Button) findViewById(R.id.btLocation);
		btnlayout = (LinearLayout)findViewById(R.id.btnlayout);
		helplayout = (LinearLayout)findViewById(R.id.helplayout);
		summarylayout = (LinearLayout)findViewById(R.id.summarylayout);
		btHelp.setOnClickListener(this);
		btSummary.setOnClickListener(this);
		btContact.setOnClickListener(this);
		btLocation.setOnClickListener(this);
		helplayout.setVisibility(View.GONE);
		summarylayout.setVisibility(View.GONE);
		//summary
		btDone = (Button) findViewById(R.id.btDoneSummary);
		//btChange = (Button) findViewById(R.id.btChangeLanguage);
		btDone.setOnClickListener(cancel_button_click_listener);
		//btChange.setOnClickListener(change_button_click_listener);

		webview = (WebView)findViewById(R.id.webviewSummary);
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setBuiltInZoomControls(true);
		//webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		//webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		
		//help
		btDone1 = (Button) findViewById(R.id.btDoneHelp);
		btDone1.setOnClickListener(cancel_button_click_listener1);
		
		webview1 = (WebView) findViewById(R.id.webviewHelp);
		webview1.getSettings().setSupportZoom(true);
		
		//webview1.getSettings().setLoadWithOverviewMode(true);
		webview1.getSettings().setUseWideViewPort(true);
		webview1.getSettings().setBuiltInZoomControls(true);
		//float scale = webview1.getScale();
		//scale = 40f * scale;
		//webview1.setInitialScale((int)scale);
		//webview1.getSettings().setTextSize(TextSize.valueOf("15dp"));
		webview1.loadUrl("file:///android_asset/help.html");
		translateEn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {				
					boolean connection = new ConnectionDetector(Info.this)
					.isConnectStatus();
					if (connection) {
						new sendingData()
						.execute(
								"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/1574bddb75c78a6fd2251d61e2993b5146201319",
								"1");
						translateEn.setBackgroundResource(R.drawable.agree2);
						translateMy.setBackgroundResource(R.drawable.agree1);
					}else{
						new ConnectionDetector(Info.this).Alert("No connection available.");
					}
			}
		});
		translateMy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				

					boolean connection = new ConnectionDetector(Info.this)
					.isConnectStatus();
					if (connection) {
						new sendingData()
						.execute(
								"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/7b52009b64fd0a2a49e6d8a939753077792b0554",
								"0");// bm
						translateEn.setBackgroundResource(R.drawable.agree1);
						translateMy.setBackgroundResource(R.drawable.agree2);
					}else{
						new ConnectionDetector(Info.this).Alert("No connection available.");
					}
			}
		});
		
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(WebServiceLink.summeryflg){
				clayout.setVisibility(View.GONE);
				btnlayout.setVisibility(View.VISIBLE);
				helplayout.setVisibility(View.GONE);
				summarylayout.setVisibility(View.GONE);
				WebServiceLink.summeryflg = false;
			}else{
				onBackProcess();
			}
			return false;
		}
		return super.onKeyUp(keyCode, event);
	}


	public void Alert5() {
		new AlertDialog.Builder(Info.this)
		.setTitle("CTOS")
		.setMessage("Are you sure to log out?")

		.setPositiveButton("YES", new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            // Do nothing but close the dialog

			Intent intent = new Intent(Info.this,
					CTOSMainActivity.class);
			startActivity(intent);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.remove("CID");
			editor.remove("CIDSTATUS");
			editor.commit();
			finish();
            dialog.dismiss();
	    }
	
	    })
	
	    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
	
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing
	            dialog.dismiss();
	        }
	    })
	    .show();
   }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
			if(v.getId() == R.id.btSummary) {
				boolean connection = new ConnectionDetector(Info.this)
				.isConnectStatus();
				if (connection) {
					clayout.setVisibility(View.VISIBLE);
					btnlayout.setVisibility(View.GONE);
					helplayout.setVisibility(View.GONE);
					summarylayout.setVisibility(View.VISIBLE);
					WebServiceLink.summeryflg = true;
					initiatePopupWindow();
				}else{
					new ConnectionDetector(Info.this).Alert("No connection available.");
				}
			}else if(v.getId() == R.id.btLocation){
				/*boolean connection = new ConnectionDetector(Info.this)
				.isConnectStatus();
				if (connection) {
					// TODO Auto-generated method stub
					LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
					Location loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					//locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
					final LocationListener listener = new LocationListener() {

				        public void onLocationChanged(Location location) {
				        }

				        public void onProviderDisabled(String provider) {
				        }

				        public void onProviderEnabled(String provider) {
				        }

				        public void onStatusChanged(String provider, int status, Bundle extras) {
				        }
				    };
				    mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
				    loc = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					
				    
					if (loc != null) {
						lat = String.valueOf(loc.getLatitude());
					    //Log.v("gps", lat.toString());
					    lon = String.valueOf(loc.getLongitude());
					    //Log.v("gps", lat.toString());
					} else {
						Toast.makeText(Info.this,
								"GPS is not available in your devide",
								Toast.LENGTH_SHORT).show();
					}
					final Intent intent = new Intent(
							Intent.ACTION_VIEW,
							Uri.parse("http://maps.google.com/maps?"
									+ "saddr="
									+ lat
									+ ","
									+ lon
									+ "&daddr=Ctos Sdn Bhd Kampung Baru Kuala Lumpur Federal Territory of Kuala Lumpur"));
					// + src_lat + ","+ src_long));
					intent.setClassName("com.google.android.apps.maps",
							"com.google.android.maps.MapsActivity");
					startActivity(intent);
				}else{
					new ConnectionDetector(Info.this).Alert("No connection available.");
				}*/
				Intent intent = new Intent(Info.this, Contact1.class);
				intent.putExtra("info", "1");
				startActivity(intent);
			}else if(v.getId() == R.id.btContact){			
					Intent intent = new Intent(Info.this, Contact.class);
					intent.putExtra("info", "1");
					startActivity(intent);
				
			}else if(v.getId() == R.id.btHelp){

				boolean connection = new ConnectionDetector(Info.this)
				.isConnectStatus();
				if (connection) {
				WebServiceLink.summeryflg = true;
				clayout.setVisibility(View.GONE);
				btnlayout.setVisibility(View.GONE);
				helplayout.setVisibility(View.VISIBLE);
				summarylayout.setVisibility(View.GONE);
				initiatePopupWindow1();
				}else{
					Alert("No internet connection");
				}
			}
	}

	private void initiatePopupWindow() {
		try {
			/*LayoutInflater inflater = (LayoutInflater) getSherlockActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View layout = inflater.inflate(
					R.layout.summaryofright,
					(ViewGroup) getSherlockActivity().findViewById(
							R.id.llsummaryofright));

			Drawable background = layout.getBackground();
			background.setAlpha(180);

			pw = new PopupWindow(layout, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT, true);
			pw.setOutsideTouchable(true);
			pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
					*/
			/*btDone = (Button) layout.findViewById(R.id.btDoneSummary);
			btChange = (Button) layout.findViewById(R.id.btChangeLanguage);
			btDone.setOnClickListener(cancel_button_click_listener);
			btChange.setOnClickListener(change_button_click_listener);

			webview = (WebView) pw.getContentView().findViewById(R.id.webviewSummary);
			webview.getSettings().setSupportZoom(true);
			
			webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
			webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
			*/
			new sendingData()
					.execute(
							"http://www.justmobileinc.com/test/ctos/en/articles/getarticle/1574bddb75c78a6fd2251d61e2993b5146201319",
							"1");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void initiatePopupWindow1() {
		try {
			/*LayoutInflater inflater = (LayoutInflater) getSherlockActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View layout = inflater.inflate(
					R.layout.help,
					(ViewGroup) getSherlockActivity().findViewById(
							R.id.llhelp));

			Drawable background = layout.getBackground();
			background.setAlpha(180);

			pw1 = new PopupWindow(layout, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT, true);
			pw1.showAtLocation(layout, Gravity.CENTER, 0, 0);

			btDone1 = (Button) layout.findViewById(R.id.btDoneHelp);
			btDone1.setOnClickListener(cancel_button_click_listener1);
			
			webview1 = (WebView) layout.findViewById(R.id.webviewHelp);
			webview1.getSettings().setSupportZoom(true);
			//webview1.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
			//webview1.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
			webview1.getSettings().setLoadWithOverviewMode(true);
			webview1.getSettings().setUseWideViewPort(true);
			webview1.getSettings().setBuiltInZoomControls(true);
			//webview1.getSettings().setBuiltInZoomControls(true);
			//float scale = 40 * webview1.getScale();
			//webview1.setInitialScale((int) scale);
			webview1.loadUrl("file:///android_asset/help1.html");
			*/
			//webview1.loadUrl("file:///android_asset/help.jpg");
			//webview1.getSettings().setUseWideViewPort(true);
			//webview1.getSettings().setJavaScriptEnabled(true);
			//webview1.getSettings().setBuiltInZoomControls(true);
			//webview1.getSettings().setLoadWithOverviewMode(true);
			
			//webview1.loadDataWithBaseURL("file:///android_asset/help.html", en, "text/html", "UTF-8", null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private OnClickListener cancel_button_click_listener1 = new OnClickListener() {
		public void onClick(View v) {
			btnlayout.setVisibility(View.VISIBLE);
			helplayout.setVisibility(View.GONE);
			summarylayout.setVisibility(View.GONE);
			//if (pw1.isShowing())
				//pw1.dismiss();
		}
	};
	private OnClickListener cancel_button_click_listener = new OnClickListener() {
		public void onClick(View v) {
			clayout.setVisibility(View.GONE);
			btnlayout.setVisibility(View.VISIBLE);
			helplayout.setVisibility(View.GONE);
			summarylayout.setVisibility(View.GONE);
			//if (pw.isShowing())
			//	pw.dismiss();
		}
	};

	class sendingData extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Info.this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				Post post = new Post();
				String po = post.CallWebService(urls[0], "1", "");
				if (urls[1].equals("1"))
					initial = "1";
				else
					initial = "0";

				return po;
			} catch (Exception e) {
				this.exception = e;
				return this.exception.toString();
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			/*String sXML = feed.replaceAll("[\r\n]+", "");
			// String test = sXML.replaceAll("[\r\n]+", "");
			DocumentBuilder db = null;
			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(sXML));

			Document doc = null;
			try {
				doc = db.parse(is);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			NodeList nodes = doc.getElementsByTagName("page");
			Element element = (Element) nodes.item(0);
			NodeList name = element.getElementsByTagName("page_name");
			Element line = (Element) name.item(0);
			if (initial.equals("1"))
				en = line.getTextContent();
			else
				bm = line.getTextContent();
			//webview.setInitialScale(2);
			
			webview.loadDataWithBaseURL(null, line.getTextContent()+"<br>",
					"text/html", "UTF-8", null);
			// webview.loadData(getCharacterDataFromElement(line),
			// "text/html",null);
			 * */
			try{
				String finalString="";
				for(int i=0;i<feed.length();i++)
				{
					if(feed.charAt(i)!='\\')
							finalString+=feed.charAt(i);
					
				}
				String sXML = "";
				sXML = finalString.replaceAll("rn", "");
				int index = 0;
				if((index = sXML.indexOf("content"))!=-1){
					sXML = sXML.substring(index+10, sXML.length()-2);
				}
				//Spanned span = Html.fromHtml(sXML);
				//sXML = span.toString();
				//sXML = TextUtils.htmlEncode(sXML);
				
				
				
				webview.loadData(sXML, "text/html", "utf-8");	
				
				
			}catch(Exception e){
				ReturnMessage.showAlertDialog(Info.this,
						"Internet is invalid.");
			}
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(Info.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
}