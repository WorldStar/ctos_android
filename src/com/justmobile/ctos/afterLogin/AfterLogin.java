package com.justmobile.ctos.afterLogin;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.R;
import com.justmobile.ctos.hometab.HomeafterLogin;
import com.justmobile.ctos.infotab.Info1;
import com.justmobile.ctos.profiletab.ProfileAfter;
import com.justmobile.ctos.servicestab.Services;

public class AfterLogin extends SherlockFragmentActivity {
	SharedPreferences settings;
	String status;
	ViewPager mViewPager;
	TabsAdapter mTabsAdapter;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		status = settings.getString("Status", null);
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
		Log.d("statusf", status);

		setContentView(mViewPager);
		ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		bar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.topbar01));
		bar.setIcon(R.drawable.ctoslogo);
		
		mTabsAdapter = new TabsAdapter(this, mViewPager);
		

		/*mTabsAdapter.addTab(bar.newTab().setIcon(R.drawable.icon_home1),
				HomeafterLogin.class, null);
		mTabsAdapter.addTab(bar.newTab().setIcon(R.drawable.icon_profile1),
				ProfileAfter.class, null);
		mTabsAdapter.addTab(bar.newTab().setIcon(R.drawable.icon_services1),
				Services.class, null);
		mTabsAdapter.addTab(bar.newTab().setIcon(R.drawable.icon_info1),
				Info1.class, null);*/

	}
	

	public static class TabsAdapter extends FragmentStatePagerAdapter implements
			ActionBar.TabListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final ActionBar mActionBar;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mActionBar = activity.getSupportActionBar();
			mViewPager = pager;
			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
		}

		public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
			TabInfo info = new TabInfo(clss, args);
			tab.setTag(info);
			tab.setTabListener(this);
			mTabs.add(info);
			mActionBar.addTab(tab);
			notifyDataSetChanged();
		}

		
		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);
			return Fragment.instantiate(mContext, info.clss.getName(),
					info.args);
		}

		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
		}

		public void onPageSelected(int position) {
			mActionBar.setSelectedNavigationItem(position);
		}

		public void onPageScrollStateChanged(int state) {
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Object tag = tab.getTag();
			/*if (tab.getPosition() == 0)
				tab.setIcon(R.drawable.icon_home);
			if (tab.getPosition() == 1)
				tab.setIcon(R.drawable.icon_profile);
			if (tab.getPosition() == 2)
				tab.setIcon(R.drawable.icon_services);
			if (tab.getPosition() == 3)
				tab.setIcon(R.drawable.icon_info);
			for (int i = 0; i < mTabs.size(); i++) {
				if (mTabs.get(i) == tag) {
					mViewPager.setCurrentItem(i);
				}
			}*/
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			/*if (tab.getPosition() == 0)
				tab.setIcon(R.drawable.icon_home1);
			if (tab.getPosition() == 1)
				tab.setIcon(R.drawable.icon_profile1);
			if (tab.getPosition() == 2)
				tab.setIcon(R.drawable.icon_services1);
			if (tab.getPosition() == 3)
				tab.setIcon(R.drawable.icon_info1);*/
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "").setIcon(R.drawable.logout).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

	    super.onCreateOptionsMenu(menu);

	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == 0) {
			Alert("Are you sure to Log out?");
			return true;

		} else {
			return super.onOptionsItemSelected(item);
		}
	}
	private Toast toast;
	private long lastBackPressTime = 0;

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		if (this.lastBackPressTime < System.currentTimeMillis() - 3000) {
			this.lastBackPressTime = System.currentTimeMillis();
			toast = Toast.makeText(this, "Press back again to close this app",
					3000);
			toast.show();
		} else {
			if (toast != null) {
				toast.cancel();
			}
			/*Intent intent = new Intent(AfterLogin.this, CTOSLoginPage.class);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.commit();
			this.startActivity(intent);
			finish();*/
			
			
			}
		
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(AfterLogin.this,
								CTOSLoginPage.class);
						startActivity(intent);
						SharedPreferences.Editor editor = settings.edit();
						editor.putString("got", "0");
						editor.commit();
						finish();
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (outState.isEmpty()) {
			outState.putBoolean("bug:fix", true);
		}
	}
	
}
