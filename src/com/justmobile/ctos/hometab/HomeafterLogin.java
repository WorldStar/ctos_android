package com.justmobile.ctos.hometab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.R;

public class HomeafterLogin extends SherlockFragment{
	SharedPreferences settings;
	List<HashMap<String,String>> loginResult = new ArrayList<HashMap<String,String>>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.hometabafter, container, false);
		//setHasOptionsMenu(true);
		settings=getSherlockActivity().getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		try{
		String status=settings.getString("Status", null);
		Log.d("status", status);
		}
		catch(Exception e){
			Log.d("status", "123");
		}
//		MemberWebService wsMember = new MemberWebService(getSherlockActivity());
//		//loginResult = wsMember.AuLogin("000000000000", "abc123");
//		loginResult = wsMember.getLsCountry();
//		Log.d("login result", loginResult.toString());
		
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
      
		MenuItem login = menu.add(Menu.NONE, 0, 0, "").setIcon(R.drawable.logout);
		login.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		super.onCreateOptionsMenu(menu, inflater);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 0:
			Intent intent = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			this.startActivity(intent);
			Log.d("logout", "logout");

			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.commit();
			getSherlockActivity().finish();
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
}