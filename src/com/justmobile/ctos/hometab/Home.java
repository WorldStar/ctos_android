package com.justmobile.ctos.hometab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.CTOSMainActivity;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.profiletab.RegisterFirstPage;
import com.justmobile.ctos.servicestab.Services;

public class Home extends TabGroupActivity{
	Typeface font1,font2;
	boolean connection;
	SharedPreferences settings;
	String CID = "";
	ScrollView  scWhy;//scIntro,
	Button btRegister, btWhy;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hometab);

		 font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		 settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		 CID = settings.getString("CID", "");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Home");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);

		 if(CID != null && !CID.equals("")){
			 btlog.setText("Logout");
		 }else{
			 btlog.setText("Login");
		 }
		btlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					
					if(CID != null && !CID.equals("")){

						Alert5();
					 }else{
						 Intent intent = new Intent(Home.this,
									CTOSLoginPage.class);
						startActivity(intent);
						finish();
					 }
			}
		});
		LinearLayout regLayout = (LinearLayout)findViewById(R.id.regLayout);
		//scIntro = (ScrollView)findViewById(R.id.scIntro);
		scWhy = (ScrollView)findViewById(R.id.scWhy);
		//btWhy = (Button) findViewById(R.id.btWhy);
		scWhy.setVisibility(View.GONE);
		btRegister = (Button) findViewById(R.id.btRegister);
		btRegister.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				connection = new ConnectionDetector(Home.this).isConnectStatus();
				if (connection) {
					Intent intent = new Intent(Home.this,
							RegisterFirstPage.class);
					startActivity(intent);
					finish();
				}
				else{
					Alert("No connection available.");
				}
				//btWhy.setBackgroundResource(R.drawable.cancel1);
				scWhy.setVisibility(View.GONE);
				//scIntro.setVisibility(View.VISIBLE);
				
			}
		});
		
		/*btWhy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btWhy.setBackgroundResource(R.drawable.cancel2);
				scWhy.setVisibility(View.VISIBLE);
				scIntro.setVisibility(View.GONE);
			}
		});*/

		 if(CID != null && !CID.equals("")){
			 regLayout.setVisibility(View.GONE);
		 }else{
			 regLayout.setVisibility(View.VISIBLE);
		 }
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}


	public void Alert5() {
		new AlertDialog.Builder(Home.this)
		.setTitle("CTOS")
		.setMessage("Are you sure to log out?")

		.setPositiveButton("YES", new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            // Do nothing but close the dialog

			Intent intent = new Intent(Home.this,
					CTOSMainActivity.class);
			startActivity(intent);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("got", "0");
			editor.remove("CID");
			editor.remove("CIDSTATUS");
			editor.commit();
			finish();
            dialog.dismiss();
	    }
	
	    })
	
	    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
	
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing
	            dialog.dismiss();
	        }
	    })
	    .show();
   }
}
