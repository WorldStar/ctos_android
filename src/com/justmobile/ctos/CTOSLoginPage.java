package com.justmobile.ctos;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.TabGroupActivity;
import com.justmobile.ctos.afterLogin.AfterLogin;
import com.justmobile.ctos.hometab.Home;
import com.justmobile.ctos.profiletab.ProfileAfter;
import com.justmobile.ctos.profiletab.RegisterFirstPage;
import com.justmobile.ctos.profiletab.RegisterSixthPage;
import com.justmobile.ctos.servicestab.Services;
import com.justmobile.ctos.webservice.MemberWebService;

public class CTOSLoginPage extends Activity implements OnClickListener{
	Typeface font1,font2;
	boolean connection;
	EditText etLoginUsername;
	EditText etLoginPassword;
	String name;
	String password;
	Button btLogin;
	ProgressDialog MyDialog;
	SharedPreferences settings;
	Iterator itr;
	String Status;
	String CID;
	String CidStatus;
	String Key;
	private ProgressDialog pDialog;
	Button btLoginForgotPassword;
	String logocontent = "0";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		 font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Login");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setText("Cancel");
		btlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Intent intent = new Intent(CTOSLoginPage.this,
							CTOSMainActivity.class);
					startActivity(intent);
					finish();
			}
		});
		settings = getSharedPreferences("CTOS", Context.MODE_PRIVATE);
		etLoginUsername = (EditText) findViewById(R.id.etLoginUsername);
		etLoginUsername.setSingleLine();
		//etLoginUsername.setTypeface(font2);
		etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);
		//etLoginPassword.setTypeface(font2);
		Intent intent = getIntent();
		logocontent = intent.getStringExtra("logocontent");

		btLogin = (Button) findViewById(R.id.btLogin);
		btLoginForgotPassword = (Button) findViewById(R.id.btLoginForgotPassword);
		//btLogin.setBackgroundResource(R.drawable.cancel2);
		btLogin.setOnClickListener(this);
		btLoginForgotPassword.setOnClickListener(this);

	}

	class webservicelogin extends AsyncTask<String, String, String> {

		private Exception exception;
		String errorMessage = "Internet service invalid.";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(CTOSLoginPage.this);
			pDialog.setMessage("Verifying...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				MemberWebService Login = new MemberWebService();
				List<HashMap<String, String>> result = Login.AuLogin(name,
						password);
				itr = Login.result.iterator();
				// Looper.prepare();

				if (Login.result.get(0).containsValue("Login successful")) {
					while (itr.hasNext()) {
						HashMap<String, String> map = (HashMap<String, String>) itr
								.next();
						String message = map.get("message");
						Status = map.get("status");
						CID = map.get("cid");
						CidStatus = map.get("cidStatus");
						Key = map.get("key");

						SharedPreferences.Editor editor = settings.edit();
						editor.putString("Status", Status);
						editor.putString("CID", CID);
						editor.putString("CIDSTATUS", CidStatus);
						editor.putString("Key", Key);
						editor.putString("pass", password);
						editor.putString("got", "1");
						editor.commit();
					}

					return "1";

				} else {
					HashMap<String, String> map = (HashMap<String, String>) itr
							.next();
					errorMessage = map.get("errorMessage");
					return "0";
				}

			} catch (Exception e) {
				this.exception = e;
				return "0";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				Success();
			} else {
				Fail(errorMessage);
			}
			btLogin.setBackgroundResource(R.drawable.cancel1);
			btLoginForgotPassword.setBackgroundResource(R.drawable.cancel1);
		}
	}
	public void Alert(String msg) {
		new AlertDialog.Builder(this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btLogin) {
			btLogin.setBackgroundResource(R.drawable.cancel2);
			name = etLoginUsername.getText().toString();
			password = etLoginPassword.getText().toString();
			if(name.length() == 0 || password.length() == 0){
				new ConnectionDetector(this).Alert("Please fill in the details.");
			}else{
				boolean connection = new ConnectionDetector(this)
				.isConnectStatus();
				if (connection) {				
					new webservicelogin().execute();
				}else{
					new ConnectionDetector(this).Alert("No connection available.");
				}
			}
			btLogin.setBackgroundResource(R.drawable.cancel1);
			btLoginForgotPassword.setBackgroundResource(R.drawable.cancel1);
		}else if(v.getId() == R.id.btLoginForgotPassword){
			btLogin.setBackgroundResource(R.drawable.cancel1);
			btLoginForgotPassword.setBackgroundResource(R.drawable.cancel2);
			boolean connection = new ConnectionDetector(this)
			.isConnectStatus();
			if (connection) {
				Intent intent = new Intent(this, ForgotPassword.class);
				this.startActivity(intent);
			}else{
				new ConnectionDetector(this).Alert("No connection available.");
			}
			btLogin.setBackgroundResource(R.drawable.cancel1);
			btLoginForgotPassword.setBackgroundResource(R.drawable.cancel1);
		}
	}
	public void Success() {
		/*Intent i = new Intent();
		i.setClass(CTOSLoginPage.this,
				com.justmobile.ctos.afterLogin.AfterLogin.class);
		CTOSLoginPage.this.startActivity(i);

		this.finish();*/
		Intent intent;
		if(logocontent == null)logocontent = "0";
		if(logocontent.equals("1")){
			  intent = new Intent(CTOSLoginPage.this,CTOSMainActivity.class);
			  intent.putExtra("active", "pro");
			  startActivity(intent);
		}else if(logocontent.equals("2")){
			  intent = new Intent(CTOSLoginPage.this,CTOSMainActivity.class);
			  intent.putExtra("active", "serve");
			  startActivity(intent);
		}else{
		  intent = new Intent(CTOSLoginPage.this,CTOSMainActivity.class);
		  startActivity(intent);
		}
		finish();

	}

	public void Fail(String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msg)
				.setCancelable(false)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, CTOSMainActivity.class);
		this.startActivity(intent);
		this.finish();
	}
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

			Intent intent = new Intent(this, CTOSMainActivity.class);
			this.startActivity(intent);
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}*/
}
