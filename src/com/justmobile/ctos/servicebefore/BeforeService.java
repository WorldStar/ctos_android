package com.justmobile.ctos.servicebefore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.CTOSLoginPage;
import com.justmobile.ctos.ConnectionDetector;
import com.justmobile.ctos.R;
import com.justmobile.ctos.servicestab.CTOSIDEmail1;

public class BeforeService extends SherlockFragment implements OnClickListener {
	Button self;
	Button consent;
	Button email;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.servicebefore, container, false);
		//setHasOptionsMenu(true);

		self = (Button) v.findViewById(R.id.btSelf);
		consent = (Button) v.findViewById(R.id.btCon);
		email = (Button) v.findViewById(R.id.btCTOSID);

		self.setOnClickListener(this);
		consent.setOnClickListener(this);
		email.setOnClickListener(this);
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub

		MenuItem login = menu.add(Menu.NONE, 0, 0, "")
				.setIcon(R.drawable.login);
		login.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub

		int id = item.getItemId();
		Log.d("item ID : ", "onOptionsItemSelected Item ID" + id);
		if (id == 0) {
			Intent intent = new Intent(getSherlockActivity(),
					CTOSLoginPage.class);
			this.startActivity(intent);
			getSherlockActivity().finish();
			return true;

		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ConnectionDetector cd = new ConnectionDetector(getSherlockActivity());
		if(cd.isConnectingToInternet()){
			if (v.getId() == R.id.btSelf) {
				Alert("Please log in to perform self-check");
			}else if(v.getId() == R.id.btCon){
				Alert("Please log in to submit consent");
			}else if(v.getId() == R.id.btCTOSID){
				Intent CTOSIDEmail = new Intent(getSherlockActivity(),CTOSIDEmail1.class);
				startActivity(CTOSIDEmail);
			}else{
				Log.d("tete", "tetetete");
			}
		}else{
			Alert("No internet connection");
		}
	}

	public void Alert(String msg) {
		new AlertDialog.Builder(getActivity()).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getSherlockActivity(),
								CTOSLoginPage.class);
						startActivity(intent);
						getSherlockActivity().finish();
						dialog.dismiss();
					}
				}).show();

	}
}