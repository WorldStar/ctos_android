package com.justmobile.ctos;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.justmobile.ctos.webservice.MemberWebService;


public class ForgotPassword extends Activity implements OnClickListener {
	Dialog viaSecurityDialog, viaAccessCodeDialog, enterAccessCodeDialog;
	TextView tvSecurityQuest, userID1, userID2, userID3;
	EditText ForgotPasswordSecurityAns,newPass,newPassConfirm;
	EditText etID;
	private ProgressDialog pDialog;
	Typeface font1, font2;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgotpassword);
		
		Button btRequestViaSecurity = (Button) findViewById(R.id.btRequestViaSecurity);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		 font2= Typeface.createFromAsset(getAssets(),
					"fonts/CALIBRI.TTF");
		TextView tvPageTitle = (TextView) findViewById(R.id.tvPageTitle);
		tvPageTitle.setText("Forgot Password");
		tvPageTitle.setTypeface(font1);
		Button btlog = (Button) findViewById(R.id.btLog);
		btlog.setVisibility(View.GONE);
		etID = (EditText) findViewById(R.id.etID);
		etID.setSingleLine();
		etID.setTextColor(Color.GRAY);
		etID.addTextChangedListener(qtextWatcher);
		btRequestViaSecurity.setOnClickListener(this);
		
		viaSecurityDialog = new Dialog(this);
		viaSecurityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		viaSecurityDialog
				.setContentView(R.layout.forgotpassword_securitydialog);
		
		viaAccessCodeDialog = new Dialog(this);
		viaAccessCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		viaAccessCodeDialog
				.setContentView(R.layout.forgotpassword_requestaccesscode_dialog);

		enterAccessCodeDialog = new Dialog(this);
		enterAccessCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		enterAccessCodeDialog
				.setContentView(R.layout.forgotpassword_enteraccesscode_dialog);

		LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(
				R.layout.forgotpassword_securitydialog, null);
		viaSecurityDialog.setContentView(ll);

		LinearLayout l1 = (LinearLayout) LayoutInflater.from(this).inflate(
				R.layout.forgotpassword_requestaccesscode_dialog, null);
		viaAccessCodeDialog.setContentView(l1);

		LinearLayout l2 = (LinearLayout) LayoutInflater.from(this).inflate(
				R.layout.forgotpassword_enteraccesscode_dialog, null);
		enterAccessCodeDialog.setContentView(l2);

		tvSecurityQuest = (TextView) viaSecurityDialog
				.findViewById(R.id.tvSecurityQuest);
		ForgotPasswordSecurityAns = (EditText) viaSecurityDialog
				.findViewById(R.id.ForgotPasswordSecurityAns);
		ForgotPasswordSecurityAns.setTextColor(Color.GRAY);
		ForgotPasswordSecurityAns.addTextChangedListener(cftextWatcher);
		ForgotPasswordSecurityAns.setInputType(InputType.TYPE_CLASS_TEXT);
		//ForgotPasswordSecurityAns.setSingleLine();
		newPass = (EditText) viaSecurityDialog
				.findViewById(R.id.newPass);
		newPass.setTextColor(Color.GRAY);
		newPass.addTextChangedListener(cptextWatcher);
		newPass.setInputType(InputType.TYPE_CLASS_TEXT);
		//newPass.setSingleLine();
		newPassConfirm = (EditText) viaSecurityDialog
				.findViewById(R.id.newPassConfirm);
		newPassConfirm.setTextColor(Color.GRAY);
		newPassConfirm.addTextChangedListener(cpctextWatcher);
		newPassConfirm.setInputType(InputType.TYPE_CLASS_TEXT);
		//newPassConfirm.setSingleLine();
		userID1 = (TextView) viaAccessCodeDialog.findViewById(R.id.userID);
		userID2 = (TextView) viaSecurityDialog.findViewById(R.id.userID);
		userID3 = (TextView) enterAccessCodeDialog.findViewById(R.id.userID);
	}

	public String s1 = "";
	public String s2 = "";
	public String s3 = "";
	public String s4 = "";
	public String s5 = "";
	TextWatcher qtextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		etID.setText("Enter User ID here");
        		etID.setTextColor(Color.GRAY);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s5 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s5.length() > tmp.length() && s5.equals("Enter User ID here")){
        		etID.setText("");
        	}
            if(s5.equals("Enter User ID here") && s.length() == (s5.length() + 1)){
            	for(int i = 0; i < s5.length(); i++){
	            	int a = tmp.indexOf(s5.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	etID.setText(tmp);
            	etID.setSelection(1);
            	etID.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cftextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		ForgotPasswordSecurityAns.setText("Security Answer");
        		ForgotPasswordSecurityAns.setTextColor(Color.GRAY);
        		ForgotPasswordSecurityAns.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s1 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s1.length() > tmp.length() && s1.equals("Security Answer")){
        		ForgotPasswordSecurityAns.setText("");
        	}
            if(s1.equals("Security Answer") && s.length() == (s1.length() + 1)){
            	for(int i = 0; i < s1.length(); i++){
	            	int a = tmp.indexOf(s1.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	ForgotPasswordSecurityAns.setText(tmp);
            	ForgotPasswordSecurityAns.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	ForgotPasswordSecurityAns.setSelection(1);
            	ForgotPasswordSecurityAns.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cptextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		newPass.setText("New Password");
        		newPass.setTextColor(Color.GRAY);
        		newPass.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s2 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s2.length() > tmp.length() && s2.equals("New Password")){
        		newPass.setText("");
        	}
            if(s2.equals("New Password") && s.length() == (s2.length() + 1)){
            	for(int i = 0; i < s2.length(); i++){
	            	int a = tmp.indexOf(s2.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	newPass.setText(tmp);
            	newPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	newPass.setSelection(1);
            	newPass.setTextColor(Color.BLACK);            	
            }
        }  
    };
	TextWatcher cpctextWatcher = new TextWatcher() {          
        @Override  
        public void onTextChanged(CharSequence s, int start, int before, int count) {  
            // TODO Auto-generated method stub  
        	
			//if (!s.toString().matches("[a-zA-Z ]+")) {
				//s = new String("");
			//}
        	if(s.length() == 0){        		
        		newPassConfirm.setText("Confirm Password");
        		newPassConfirm.setTextColor(Color.GRAY);
        		newPassConfirm.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }            
        @Override  
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub          	
        	s3 = s.toString();
        }            
        @Override  
        public void afterTextChanged(Editable s) {  
            // TODO Auto-generated method stub  
        	String tmp = s.toString();
        	if(s3.length() > tmp.length() && s3.equals("Confirm Password")){
        		newPassConfirm.setText("");
        	}
            if(s3.equals("Confirm Password") && s.length() == (s3.length() + 1)){
            	for(int i = 0; i < s3.length(); i++){
	            	int a = tmp.indexOf(s3.substring(i, i+1));
	            	tmp = tmp.substring(0,a) + tmp.substring(a+1, tmp.length());
            	}
            	newPassConfirm.setText(tmp);
            	newPassConfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            	newPassConfirm.setSelection(1);
            	newPassConfirm.setTextColor(Color.BLACK);            	
            }
        }  
    };

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String string = etID.getText().toString();
		if (etID.length() < 4) {
			Alert("Please fill the CTOS ID");
		} else {
			if (v.getId() == R.id.btRequestViaSecurity) {
				if(string.equals("Enter your ID here")) string = "";
				if(string.length() != 0){
				  fillId(string);
				  new RetreiveFeedTask().execute(string);
				}else{
				  Alert("Please fill in the details.");
				  etID.setFocusable(true);
				}
				
			/*case R.id.btRequestAccessCode:
				viaAccessCodeDialog.show();
				break;
			case R.id.btEnterAccessCode:
				enterAccessCodeDialog.show();
				break;*/

			}
		}
	}

	public void cancelation(final View view) {
		if (viaSecurityDialog.isShowing())
			viaSecurityDialog.dismiss();
		if (viaAccessCodeDialog.isShowing())
			viaAccessCodeDialog.dismiss();
		if (enterAccessCodeDialog.isShowing())
			enterAccessCodeDialog.dismiss();
	}

	public void submitSecurityQuest(View v) {
		int fps = ForgotPasswordSecurityAns.length();
		int np = newPass.length();
		int npc = newPassConfirm.length();
		String tmp = newPass.getText().toString();
		

		Pattern p = Pattern.compile("[a-zA-z0-9]*"); 
		if (!p.matcher(tmp).matches()){			
    		Alert("Password should be alpha numeric.");
    		return;
    	}

		boolean f = false, g = false, com = false;
		for(int i = 0; i < newPass.length(); i++){
			String a = newPass.getText().toString().substring(i, i+1);
			try{
				int su = Integer.parseInt(a);
				f = true;
				if(g == false){
					com = true; break;
				}
			}catch(Exception e){
				g = true;
				if(f == true){
					com = true; break;
				}
			}
		}
		if(f == false || g == false){
			com = true;
		}

		if(com == true){
			boolean com2 = false;
			f = false;g = false;
			for(int i = 0; i < newPass.length(); i++){
				String a = newPass.getText().toString().substring(i, i+1);
				try{
					int su = Integer.parseInt(a);
					f = true;
					if(g == true){
						com2 = true; break;
					}
				}catch(Exception e){
					g = true;
					if(f == false){
						com2 = true; break;
					}
				}
			}
			if(f == false || g == false){
				com2 = true;
			}
			if(com2 == false){
				com = false;
			}
		}
		if(com == true){
			Alert("Password should be alpha numeric. Example: abc1234");
			return;
		}
		String Sfps = ForgotPasswordSecurityAns.getText().toString();
		String Snp = newPass.getText().toString();
		String Snpc = newPassConfirm.getText().toString();
		String id = etID.getText().toString();
		
		if (fps <1)ForgotPasswordSecurityAns.setHintTextColor(Color.RED);
		if (np <1)newPass.setHintTextColor(Color.RED);
		if (npc <1)newPassConfirm.setHintTextColor(Color.RED);
		
		if(fps>0&&np>0&&npc>0){
			if(Snp.equals(Snpc))
			{
				new submitSecurity().execute(id,Sfps,Snp,Snpc);
			}
			else
			{
				Alert("Confirmation password doesn't match");
			}
		}
	}

	public void Alert(String msg) {
		new AlertDialog.Builder(ForgotPassword.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).show();
	}
	public void Alert1(String msg) {
		new AlertDialog.Builder(ForgotPassword.this).setTitle("CTOS")
				.setMessage(msg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						finish();
					}
				}).show();
	}
	public void fillId(String id) {
		userID1.setText("ID: " + id);
		userID2.setText("ID: " + id);
		userID3.setText("ID: " + id);
	}

	class RetreiveFeedTask extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ForgotPassword.this);
			pDialog.setMessage("Verify CID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest
						.getQuestionSec(urls[0]);

				if (temp.get(0).get("status").equals("0"))
					return temp.get(0).get("message");
				else {
					String text = temp.get(0).get("customSecurityQuestion");
					tvSecurityQuest.setText("Security Question: " + text);
					return temp.get(0).get("status");
				}
			} catch (Exception e) {
				this.exception = e;
				return "Internet is slowly. Try again.";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				viaSecurityDialog.show();
			} else {
				Alert(feed);
			}
		}
	}
	class submitSecurity extends AsyncTask<String, String, String> {

		private Exception exception;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ForgotPassword.this);
			pDialog.setMessage("Verify CID. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... urls) {
			try {
				MemberWebService quest = new MemberWebService();
				List<HashMap<String, String>> temp = quest
						.submitQuestionSec(urls[0],urls[1],urls[2],urls[3]);
				
				if (temp.get(0).get("status").equals("0"))
					return temp.get(0).get("message");
				else {
					return temp.get(0).get("status");
				}
			} catch (Exception e) {
				this.exception = e;
				return "Security Answer is invalid.";
			}
		}

		protected void onPostExecute(String feed) {
			pDialog.dismiss();
			if (feed.equals("1")) {
				if (viaSecurityDialog.isShowing())
					viaSecurityDialog.dismiss();
				
				Alert1("Successful updated");
				
			} else {
				Alert(feed);
			}
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

			Intent intent = new Intent(this, CTOSLoginPage.class);
			this.startActivity(intent);
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}*/
}