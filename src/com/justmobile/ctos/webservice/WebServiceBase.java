package com.justmobile.ctos.webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import com.justmobile.ctos.library.Envelope;
import com.justmobile.ctos.library.Post;
import com.justmobile.ctos.library.PullXML;
import com.justmobile.ctos.library.logFile;

import android.util.Log;

public class WebServiceBase{
	public List<HashMap<String,String>> result = new ArrayList<HashMap<String,String>>();
	
	public Envelope startEnvelope(String sFunctionName){
		
		Envelope envelope = new Envelope();
		envelope.setFunctionName(sFunctionName);
		envelope.setNameSpace(WebServiceLink.getNameSpace());
		return envelope;		
	}
	
	public String post(String sEnvelope, String sSoapAction) {
		Log.d("sEnvelope", sEnvelope);
		//new logFile().write("senddata", sEnvelope);
		Post post = new Post();
		String sXML = post.CallWebService(WebServiceLink.getURL(), sSoapAction,
				sEnvelope);
		//new logFile().write("receivedata", sXML);
		sXML = sXML.replaceAll("[\r\n]", "");
		return sXML;
	}
	
	public List<HashMap<String, String>> parse(String sXML) {
		try {
			result = (PullXML.main(sXML));
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}
}