package com.justmobile.ctos.webservice;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.justmobile.ctos.library.Envelope;

public class MemberWebService extends WebServiceBase {
	final String SOAP_URL = WebServiceLink.getSoapActionURL();
	Context context;

	public MemberWebService() {
	}

	public MemberWebService(Context context) {
		this.context = context;
	}

	// AuLogin function
	public List<HashMap<String, String>> AuLogin(String Username,
			String Password) {
		String FUNCTION_NAME = "AuLogin";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("AuLogin SOAP ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("username", Username);
		envelope.setBody("password", Password);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// PmCreate function
	public List<HashMap<String, String>> PmCac(String nic, String passport,
			 String country, String mobileNumberPrefix, String mobileNumberSuffix
			) {

		String FUNCTION_NAME = "PmCac";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME + "Request";
		Log.d("PmCreate Soap action", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		// settag mykadinfo
		if(country.equals("MY")){
			envelope.setBody("cid", nic);
		}else{
			envelope.setBody("cid", passport);
		}
		envelope.setBody("mobileNumberPrefix", mobileNumberPrefix);
		envelope.setBody("mobileNumberSuffix", mobileNumberSuffix);
		
		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// PmCreate function
	public List<HashMap<String, String>> PmCreate(String name, String password,
			String confirmPassword, String nic, String ic, String passport,
			String gender, String address1, String address2, String address3,
			String postcode, String city, String state, String country,
			String citizenship, String nationality, String corrAddress1,
			String corrAddress2, String corrAddress3, String corrPostcode,
			String corrCity, String corrState, String corrCountry,
			String email, String mobileNumberPrefix, String mobileNumberSuffix,
			String standardSQ, String customSQ, String securityAnswer,
			String frontICBase64, String backICBase64, String cac) {

		String FUNCTION_NAME = "PmCreate";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME + "Request";
		Log.d("PmCreate Soap action", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		// settag mykadinfo
		envelope.setBody("cac", cac);
		envelope.setStartTag("mykadInfo");
		envelope.setBody("nationality", citizenship);
		envelope.setBody("address1", address1);
		envelope.setBody("address2", address2);
		envelope.setBody("address3", address3);
		envelope.setBody("city", city);
		envelope.setBody("postcode", postcode);
		envelope.setBody("state", state);
		envelope.setBody("country", country);
		envelope.setBody("gender", gender);
		envelope.setBody("name", name);
		envelope.setBody("nic", nic);
		envelope.setBody("ic", ic);
		envelope.setBody("passport", passport);
		envelope.setBody("password", password);
		envelope.setBody("confirmPassword", confirmPassword);
		envelope.setEndTag("mykadInfo");
		// envelope.setBody("citizenship", citizenship);
		envelope.setStartTag("correpondenceInfo");
		envelope.setBody("address1", corrAddress1);
		envelope.setBody("address2", corrAddress2);
		envelope.setBody("address3", corrAddress3);
		envelope.setBody("city", corrCity);
		envelope.setBody("postcode", corrPostcode);
		envelope.setBody("state", corrState);
		envelope.setBody("country", corrCountry);
		envelope.setBody("email", email);
		envelope.setBody("mobileNumberPrefix", mobileNumberPrefix);
		envelope.setBody("mobileNumberSuffix", mobileNumberSuffix);
		envelope.setEndTag("correpondenceInfo");

		envelope.setStartTag("securityInfo");
		envelope.setBody("standardSecurityQuestion", standardSQ);
		envelope.setBody("customSecurityQuestion", customSQ);
		envelope.setBody("securityAnswer", securityAnswer);
		envelope.setEndTag("securityInfo");

		envelope.setBody("myKadImageFront", frontICBase64);
		envelope.setBody("myKadImageBack", backICBase64);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// LsCountry function - get all the countries
	public List<HashMap<String, String>> getLsCountry() {
		String FUNCTION_NAME = "LsCountry";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML.length()+"");
		parse(sXML);
		return result;

	}

	// LsCountry function - get all the countries
	public List<HashMap<String, String>> getCheckMobile(String prefix, String postfix) {
		String FUNCTION_NAME = "FnCheckMobileExist";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("mobilePrefix", prefix);
		envelope.setBody("mobileSuffix", postfix);
		
		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML.length()+"");
		parse(sXML);
		return result;

	}
	// LsCountry function - get all the countries
	public List<HashMap<String, String>> getLsState() {
		String FUNCTION_NAME = "LsState";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML.length()+"");
		parse(sXML);
		return result;

	}

	// LsMobilePrefix function - get all the mobile prefix
	public List<HashMap<String, String>> getLsMobilePrefix() {
		String FUNCTION_NAME = "LsMobilePrefix";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// LsStdScrQuestion function - get all the countries
	public List<HashMap<String, String>> getLsStdScrQuestion() {
		String FUNCTION_NAME = "LsStdScrQuestion";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	public List<HashMap<String, String>> getLsNationality() {
		String FUNCTION_NAME = "LsNationality";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	public List<HashMap<String, String>> getGender() {
		String FUNCTION_NAME = "LsGender";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create PmScrQuestion
	public List<HashMap<String, String>> getQuestionSec(String id) {
		String FUNCTION_NAME = "PmScrQuestion";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create PmScrQuestion
	public List<HashMap<String, String>> submitQuestionSec(String id,
			String ans, String pw, String pwc) {
		String FUNCTION_NAME = "PmScrAnswer";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);
		envelope.setBody("answer", ans);
		envelope.setBody("password", pw);
		envelope.setBody("confirmPassword", pwc);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create PmView
	public List<HashMap<String, String>> PmView(String id, String key) {
		String FUNCTION_NAME = "PmView";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);
		envelope.setBody("key", key);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create FnSelfcheck
	public List<HashMap<String, String>> FnSelfcheck(String id, String key) {
		String FUNCTION_NAME = "FnSelfcheck";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);
		envelope.setBody("key", key);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create FnSelfcheck
	public List<HashMap<String, String>> FnSelfcheckReport(String id,
			String key, String reId, String reNo) {
		String FUNCTION_NAME = "FnSelfcheckReport";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME + "Request";
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);
		envelope.setBody("key", key);
		envelope.setBody("reportId", reId);
		envelope.setBody("reportNo", reNo);

		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}

	// create PmUpdate
	public List<HashMap<String, String>> PmUpdate(String id, String key, String cac,
			String add1, String add2, String add3, String city, String country,
			String email, String mnp, String mns, String pcode, String state) {
		String FUNCTION_NAME = "PmUpdate";
		String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
		Log.d("SOAP_ACTION", SOAP_ACTION);

		Envelope envelope = startEnvelope(FUNCTION_NAME);
		envelope.setBody("cid", id);
		envelope.setBody("key", key);
		envelope.setBody("cac", cac);
		envelope.setStartTag("correspondence");
			envelope.setBody("address1", add1);
			envelope.setBody("address2", add2);
			envelope.setBody("address3", add3);
			envelope.setBody("city", city);
			envelope.setBody("country", country);
			envelope.setBody("email", email);
			envelope.setBody("mobileNumberPrefix", mnp);
			envelope.setBody("mobileNumberSuffix", mns);
			envelope.setBody("postcode", pcode);
			envelope.setBody("state", state);
		envelope.setEndTag("correspondence");
		
		String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
		Log.d("sXML", sXML);
		parse(sXML);
		return result;
	}
	// create PmChgPswd
		public List<HashMap<String, String>> PmChgPswd(String id,
				String key, String reId, String reNo) {
			String FUNCTION_NAME = "PmChgPswd";
			String SOAP_ACTION = SOAP_URL + FUNCTION_NAME;
			Log.d("SOAP_ACTION", SOAP_ACTION);

			Envelope envelope = startEnvelope(FUNCTION_NAME);
			envelope.setBody("cid", id);
			envelope.setBody("key", key);
			envelope.setBody("newPassword", reId);
			envelope.setBody("newPasswordConfirm", reNo);

			String sXML = post(envelope.getEnvelope(), SOAP_ACTION);
			Log.d("sXML", sXML);
			parse(sXML);
			return result;
		}
}