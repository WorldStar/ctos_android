package com.justmobile.ctos;

import java.util.List;

import android.widget.Button;

public class TopTab {
	
	static public void setButtonView(List<Button> buttonList, int bView){
		for (int x = 0; x < buttonList.size(); x++){
			if(x == bView){
				buttonList.get(x).setBackgroundResource(R.drawable.gloss_button_press);
			} else {
				buttonList.get(x).setBackgroundResource(R.drawable.gloss_button_unpress);
			}
		}
	}
}
