package com.justmobile.ctos;

import com.justmobile.ctos.hometab.*;
import com.justmobile.ctos.infotab.Info;
import com.justmobile.ctos.library.ReturnMessage;
import com.justmobile.ctos.profiletab.Profile;
import com.justmobile.ctos.profiletab.ProfileAfter;
import com.justmobile.ctos.servicebefore.*;
import com.justmobile.ctos.servicestab.Services;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;


public class CTOSMainActivity extends TabActivity implements OnTabChangeListener{
	/** Called when the activity is first created. */
	public TabHost tabHost;
	public ImageView ivPrev;
	public ImageView ivNext;
	Typeface font1,font2;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		font1= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRIB.TTF");
		font2= Typeface.createFromAsset(getAssets(),
				"fonts/CALIBRI.TTF");
	
		tabHost = getTabHost();
		tabHost.setOnTabChangedListener(this);
		tabHost.addTab(tabHost.newTabSpec("tab1")
				.setIndicator("", getResources().getDrawable(R.drawable.tab1))
				.setContent(new Intent(this, Home.class)));
		tabHost.addTab(tabHost.newTabSpec("tab2")
				.setIndicator("", getResources().getDrawable(R.drawable.tab2))
				.setContent(new Intent(this, ProfileAfter.class)));
		tabHost.addTab(tabHost.newTabSpec("tab3")
				.setIndicator("",getResources().getDrawable(R.drawable.tab3))//.tabicon_notifications
				.setContent(new Intent(this, Services.class)));
		tabHost.addTab(tabHost.newTabSpec("tab4")
						.setIndicator("",getResources().getDrawable(R.drawable.tab4))//.tabicon_notifications
						.setContent(new Intent(this, Info.class)));
		

		tabHost.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (getTabHost().getCurrentTabTag().equals("tab2")) { 
					SharedPreferences settings = getSharedPreferences("CTOS",
							Context.MODE_PRIVATE);
					String CID = "";
					try {
						CID = settings.getString("CID", "");
					} catch (Exception e) {
						Log.d("Key", "123");
					}
					if(CID != null && !CID.equals("")){			
					}else{
						ReturnMessage.showAlertDialog(CTOSMainActivity.this, "Please log in to view/edit your profile.");
						Intent intent = new Intent(CTOSMainActivity.this,
								CTOSLoginPage.class);
						startActivity(intent);
						//finish();
					}
				}
			}
		});
		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)  
		{   
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getBaseContext().getResources().getColor(R.color.unselectedgray));  
		}  

		tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getBaseContext().getResources().getColor(R.color.selectedgray));  

		Intent intent = getIntent();

		String emailmark = intent.getStringExtra("active");
		if (intent.getBooleanExtra("ShowNotification", false)){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("email")){
			tabHost.setCurrentTab(2);
		}else if(emailmark !=null && emailmark.equals("info")){
			tabHost.setCurrentTab(3);
		}else if(emailmark !=null && emailmark.equals("profile")){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("pro")){
			tabHost.setCurrentTab(1);
		}else if(emailmark !=null && emailmark.equals("serve")){
			tabHost.setCurrentTab(2);
		}else {
			tabHost.setCurrentTab(0);
		}
		
	}
	public void onTabChanged(String tabId) {
		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)  
		{  
//			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#8A4117"));  
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getBaseContext().getResources().getColor(R.color.unselectedgray));
			
		}  

//		tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#C35817"));  
		tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(getBaseContext().getResources().getColor(R.color.selectedgray));  
	
		InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
        
	}
	/*if(tabId == 1){
		
	}*/
}